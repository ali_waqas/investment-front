import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ProfileService } from '@app/portal/profile/profile.service';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import 'jquery';
import { LocalStorageService } from '@app/core';
import { Registration } from '@app/portal/profile/profile.model';
import { ApplicationEvent } from '@app/shared/services/alert-modal.service';
import { Alert } from '@app/shared/models/alert.model';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent extends AbstractBaseComponent implements OnInit {
  signupForm: FormGroup;
  isLoading = false;
  public packageId = 1;

  constructor(
    public router: Router,
    private formBuilder: FormBuilder,
    private localStorageService: LocalStorageService,
    protected profileService: ProfileService) {
    super(profileService);
  }

  ngOnInit() {
    this.packageId = this.localStorageService.getItem('package-id');
    this.initBinding();
    this.createForm();
  }

  public async signup() {
    this.isLoading = true;
    try {
      const registration: Registration = this.signupForm.value;
      registration.package_id = this.packageId;
      const registrationResponse = await this.profileService.registerAsync(registration);
      this.router.navigateByUrl('/login?p=true&email='+registration.email+'&tr='+registrationResponse.profile.id, { state: registrationResponse });
      this.localStorageService.setItem('temp_registration', JSON.stringify(registrationResponse));
      this.hasErrors = false;
    } catch (error) {
      this.httpError = error;
      this.hasErrors = true;
    }
    this.isLoading = false;
  }

  public payNow() {
    console.log('Pay now');
    // [routerLink]="['/login']"
  }

  private createForm() {
    this.signupForm = this.formBuilder.group({
      first_name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      password_confirmation: ['', Validators.required],
      terms: ['', Validators.required],
      referral: ['', Validators.required]
    });
  }

  private initBinding() {
    (<any>$)('.toggle-password').click(function() {
      $(this).toggleClass('fa-eye fa-eye-slash');
      const input = (<any>$)((<any>$)(this).attr('toggle'));
      if (input.attr('type') === 'password') {
        input.attr('type', 'text');
      } else {
        input.attr('type', 'password');
      }
    });
  }
}
