import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService, LocalStorageService } from '@app/core';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { ProfileService } from '@app/portal/profile/profile.service';
import { SettingsService } from '@app/shared/services/settings-service';
import {
  PerfectMoneyRequest,
  Product,
  CryptoDepositRequest,
  DAIDepositRequest
} from '@app/portal/deposits/deposit.model';
import { DepositsService } from '@app/portal/deposits/deposits.service';
import { ApplicationEvent } from '@app/shared/services/alert-modal.service';
import { Alert } from '@app/shared/models/alert.model';
import { Profile, Registration } from '@app/portal/profile/profile.model';
import { param } from 'jquery';

declare const $: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent extends AbstractBaseComponent implements OnInit {
  loginForm: FormGroup;
  resetPasswordForm: FormGroup;
  public perfectMoneyRequest: PerfectMoneyRequest;
  public paymentNeeded = false;
  public accountActivated = false;
  public cryptoDepositForm: FormGroup;
  public DAIDepositForm: FormGroup;
  public requestInProgress = false;
  public showTetherForm = false;
  public showDAIForm = false;
  public loginFormData: { id?: number; email: string; password: string } = null;
  public tempEmail = '';
  isLoading = false;
  constructor(
    public router: Router,
    private formBuilder: FormBuilder,
    profileService: ProfileService,
    private depositService: DepositsService,
    private authenticationService: AuthenticationService
  ) {
    super(profileService);
  }

  ngOnInit() {
    this.createForm();
    const params = this.router.parseUrl(this.router.url).queryParams;
    this.accountActivated = !!params.session;

    if (typeof params.p !== 'undefined') {
      this.tempEmail = params.email;
      if (typeof this.tempEmail !== 'undefined') {
        this.loginFormData = {
          email: this.tempEmail,
          password: '',
          id: params.tr
        };
        (<any>$)('#payment-required').modal();
      }
    }

    this.initBinding();
  }

  async login() {
    this.hasErrors = false;
    this.isLoading = true;
    this.loginFormData = this.loginForm.value;

    this.authenticationService.login(this.loginFormData).subscribe(
      async (credentials: Authentication.Credentials) => {
        const client = await this.profileService.getClientProfileAsync();
        if (client.profile.is_signup_fee_paid === 'Yes') {
              $('body').removeClass('nav-function-top');
              this.router.navigate(['/portal/dashboard'], {
                replaceUrl: true
              });
        } else {
          const params = this.router.parseUrl(this.router.url).queryParams;
          const amount = !!params.test ? 0.01 : 10.0;
          this.paymentNeeded = true;
          this.perfectMoneyRequest = {
            client_id: client.profile.id,
            payee_name: `${client.profile.first_name +
              ' ' +
              client.profile.last_name}`,
            payment_units: 'USD',
            product: Product.SUBSCRIPTION_FEE_1,
            is_subscription_fee: 'Yes',
            payee_account: '',
            payment_amount: amount,
            payment_id: '',
            payment_url_method: 'POST'
          };
          (<any>$)('#payment-required').modal();
        }
      },
      (error: any) => {
        if(error.error.hasOwnProperty('otp_required')) {
            $("#2fa-modal-md").modal();
        } else if (error.hasOwnProperty('error')) {
            this.hasErrors = true;
            this.httpError = error;
            this.isLoading = false;
          if (error.error.error.includes('pending')) {
            this.loginFormData.id = error.error.tmp_reg.id;
            this.loginFormData = {
              email: error.error.tmp_reg.email,
              password: '',
              id: error.error.tmp_reg.id
            };
            console.log(this.loginFormData);
            (<any>$)('#payment-required').modal();
          }
        }
      }
    );
  }

  public async initPerfectMoneyRequest() {
    try {
      const tempRegistration: any = await this.profileService.fetchTempraryRegistrationAsync(
        this.loginFormData.id
      );
      this.perfectMoneyRequest = {
        client_id: tempRegistration.id,
        payee_name: `${tempRegistration.first_name +
          ' ' +
          tempRegistration.last_name}`,
        payment_units: 'USD',
        product: Product.SUBSCRIPTION_FEE_1,
        is_subscription_fee: 'Yes',
        payee_account: '',
        payment_amount: 10.0,
        payment_id: '',
        payment_url_method: 'POST',
        suggested_memo: 'Payment for: ' + tempRegistration.email
      };

      const request: any = await this.depositService.createOpenPerfectMoneyRequest(
        this.perfectMoneyRequest
      );
      this.perfectMoneyRequest = request.perfect_money_request;
      (<any>$('#form_payment_id')).val(this.perfectMoneyRequest.payment_id);
      (<any>$('#form_payment_amount')).val(
        this.perfectMoneyRequest.payment_amount
      );
      (<any>$('#payment_url')).val(this.perfectMoneyRequest.payment_url);
      (<any>$('#no_payment_url')).val(this.perfectMoneyRequest.nopayment_url);
      (<any>$('#memo')).val('Payment for: ' + tempRegistration.email);
      (<any>$('#perfectmoney_step_1')).submit();
    } catch (error) {
      this.httpError = error;
      this.hasErrors = true;
      alert(error);
    }
  }

  resetPassword() {
    // TODO: Implement Reset Password
  }

  private createForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', Validators.required]
    });
    this.resetPasswordForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }

  public createCryptoDepositForm() {
    this.cryptoDepositForm = this.formBuilder.group({
      transaction_id: ['', [Validators.required]],
      transaction_date: ['', [Validators.required]],
      amount: [10, [Validators.required]],
      email: [
        this.loginFormData.email,
        [Validators.required, Validators.email]
      ],
      status: ['PENDING', [Validators.required]]
    });
    this.showTetherForm = true;
  }

   public createDAIDepositForm() {
    this.DAIDepositForm = this.formBuilder.group({
      transaction_id: ['', [Validators.required]],
      transaction_date: ['', [Validators.required]],
      amount: [10, [Validators.required]],
      email: [
        this.loginFormData.email,
        [Validators.required, Validators.email]
      ],
      status: ['PENDING', [Validators.required]]
    });
    this.showDAIForm = true;
  }

  public async createCryptoDepositRequest() {
    try {
      this.requestInProgress = true;
      const cryptoDepositRequest: CryptoDepositRequest = this.cryptoDepositForm
        .value;
      const request: any = await this.depositService.createCryptoDepositRequest(
        cryptoDepositRequest
      );
      this.cryptoDepositForm.reset();
      this.alert = {
        title: 'Deposit Request Created',
        sub_title: 'Transaction Saved',
        body:
          'We have successfully received your request for subscription fee, your account will be updated once the funds are received.',
        ok_text: 'OK',
        close_text: 'CLOSE'
      };
      (<any>$)('#payment-required').modal('hide');
      (<any>$)('#alert-modal-md').modal();
      this.router.navigateByUrl('login');
    } catch (error) {
      this.httpError = error;
      this.hasErrors = true;
    }
  }

  public async createDAIDepositRequest() {
    try {
      this.requestInProgress = true;
      const daiDepositRequest: DAIDepositRequest = this.DAIDepositForm
        .value;
      const request: any = await this.depositService.createDAIDepositRequest(
        daiDepositRequest
      );
      this.DAIDepositForm.reset();
      this.alert = {
        title: 'Deposit Request Created',
        sub_title: 'Transaction Saved',
        body:
          'We have successfully received your request for subscription fee, your account will be updated once the funds are received.',
        ok_text: 'OK',
        close_text: 'CLOSE'
      };
      (<any>$)('#payment-required').modal('hide');
      (<any>$)('#alert-modal-md').modal();
      this.router.navigateByUrl('login');
    } catch (error) {
      this.httpError = error;
      this.hasErrors = true;
    }
  }

  private initBinding() {
    (<any>$)('.toggle-password').click(function() {
      $(this).toggleClass('fa-eye fa-eye-slash');
      const input = (<any>$)((<any>$)(this).attr('toggle'));
      if (input.attr('type') === 'password') {
        input.attr('type', 'text');
      } else {
        input.attr('type', 'password');
      }
    });
  }
}
