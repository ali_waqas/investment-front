import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProfileService } from '@app/portal/profile/profile.service';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { Alert } from '@app/shared/models/alert.model';
import { ApplicationEvent } from '@app/shared/services/alert-modal.service';
import { ActivatedRoute, Params } from '@angular/router';
import { ResetPasswordRequest } from './reset-password.model';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})

export class ResetPasswordComponent extends AbstractBaseComponent implements OnInit {

    resetPasswordForm: FormGroup;
    private token: string;
    constructor(
        private formBuilder: FormBuilder,
        profileService: ProfileService,
        private router: ActivatedRoute,
        private applicationEvent: ApplicationEvent) {
      super(profileService);

    }

    ngOnInit() {
        this.router.params.forEach((params: Params) => {
            this.token = params.token;
        });
        this.createForm();
    }

    private createForm() {
        this.resetPasswordForm = this.formBuilder.group({
            password: ['', [Validators.required]],
            password_confirmation: ['', [Validators.required]]
        });
    }

    public async resetPassword() {
        try {
            const resetPasswordRequest: ResetPasswordRequest = this.resetPasswordForm.value;
            resetPasswordRequest.token = this.token;
            const resp = await this.profileService.resetPassword(resetPasswordRequest);
            const alert: Alert = {title: 'Password Recovery',
                                  sub_title: 'Request completed',
                                  body: 'Your password has been changed successfully.',
                                  ok_text: 'OK',
                                  close_text: 'CLOSE'};
            this.applicationEvent.fireAlert(alert);
        } catch (error) {
            console.log(error);
            this.hasErrors = true;
            this.httpError = error;
        }
    }
}
