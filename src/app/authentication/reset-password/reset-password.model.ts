export interface ResetPasswordRequest {
    token: string;
    password: string;
    confirm_password: string;
}
