import { Component, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-maintenance',
  templateUrl: './maintenance.component.html',
  styleUrls: ['maintenance.component.css']
})
export class MaintenanceComponent implements AfterViewInit {
  ngAfterViewInit() {
    $(function() {
      $('.preloader').fadeOut();
    });
  }
}
