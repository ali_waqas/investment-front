import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProfileService } from '@app/portal/profile/profile.service';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { Alert } from '@app/shared/models/alert.model';
import { ApplicationEvent } from '@app/shared/services/alert-modal.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})

export class ForgotPasswordComponent extends AbstractBaseComponent implements OnInit {

    resetPasswordForm: FormGroup;
    constructor(
        private formBuilder: FormBuilder,
        profileService: ProfileService,
        private applicationEvent: ApplicationEvent) {
      super(profileService);

    }

    ngOnInit() {
        this.createForm();
    }

    private createForm() {
        this.resetPasswordForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]]
        });
    }

    public async requestPasswordResetEmail() {
        try {
            const resp = await this.profileService.generatePasswordResetEmail(this.resetPasswordForm.value.email);
            const alert: Alert = {title: 'Password Recovery',
                                  sub_title: 'Request completed',
                                  body: 'Your password reset request has been received, if there is an account associated with the email you provide we will send you an email with instruction.',
                                  ok_text: 'OK',
                                  close_text: 'CLOSE'};
            this.applicationEvent.fireAlert(alert);
        } catch (error) {
            this.hasErrors = true;
            this.httpError = error;
        }
    }
}
