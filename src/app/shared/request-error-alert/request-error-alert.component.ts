import { Component, OnInit, Input } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-request-error-alert',
  templateUrl: './request-error-alert.component.html',
  styleUrls: ['./request-error-alert.component.css']
})
export class RequestErrorAlertComponent implements OnInit {
  public errorCode = 0;
  @Input() httpError: HttpErrorResponse;
  constructor() {}
  ngOnInit() {
    if (this.httpError) {
      this.errorCode = this.httpError.status || 0;
    }
  }
}
