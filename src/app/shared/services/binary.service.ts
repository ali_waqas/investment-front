import { Injectable } from '@angular/core';
import { BaseAPIClass } from '@app/core/class/baseAPI.class';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { LocalStorageService } from '@app/core';
import { AuthenticationEvent } from '@app/core/authentication/authentication.model';
import { ProfileService } from '@app/portal/profile/profile.service';
import { BinaryDeposit } from '@app/binary/deposits/deposit.model';
import { BinaryWithdrawal } from '@app/binary/withdrawals/withdrawal.model';
import { BinaryProfile } from '@app/binary/profile/profile.model';
import { ApplicationEvent } from './alert-modal.service';
import { BinaryTrade } from '@app/binary/trades/trade.model';

@Injectable({providedIn: 'root'})
export class BinaryService extends BaseAPIClass {
  private readonly URL_BINARY_TOKEN: string = 'client/binary_token';
  private readonly S: string = 'converly.iokeygen';
  private readonly BASE_URL: string = 'https://bo.converly.io/';
  private readonly STORAGE_KEY_BINARY_TOKEN: string = 'BinaryToken';
  private readonly STORAGE_KEY_BINARY_PROFILE: string = 'BinaryProfile';
  private readonly STORAGE_KEY_BINARY_PROFILE_TYPE: string = 'BinaryProfileType';
  private readonly URL_GET_TOKEN: string = 'get_token';
  private readonly URL_PROFILE: string = 'users/';
  private readonly URL_TRADES: string = 'trades/';
  private readonly URL_DEPOSITS: string = 'deposits/';
  private readonly URL_WITHDRAWALS: string = 'withdraws/';
  private readonly URL_BINARY_ACCOUNT: string = 'client/binary_account';
  private readonly URL_MODIFIERS: string = 'get/modifiers/';
  private readonly DEMO_DEPOSIT_AMOUNT: number = 10000;


  constructor(
    protected httpClient: HttpClient,
    protected authenticationService: AuthenticationService,
    private localStorageService: LocalStorageService,
    private applicationEvent: ApplicationEvent,
    private profileService: ProfileService ) {
    super(httpClient, authenticationService);
    this.authenticationSubscription = this.authenticationService.onAuthenticationChange.subscribe(
        this.handleAuthenticationChanged.bind(this)
    );
  }

  public async getSha256Async(data: any) {
    const newSHA: any = await this.postAsync(this.URL_BINARY_TOKEN, data).toPromise();
    return newSHA.token;
  }

  public async getAuthenticationTokenAsync(demo: boolean = false, refresh: boolean = false) {
    let binaryToken = null;
    if (!refresh) {
        binaryToken  = this.getBinaryTokenFromStorage();
    }
    if (binaryToken == null) {
        const client = await this.profileService.getProfileAsync();
        const SHA512 = await this.getSha256Async({email: client.profile.email});
        const response: any = await this.getAsync(this.URL(this.URL_GET_TOKEN),
                                    {
                                      email: client.profile.email,
                                      hash: SHA512,
                                      demo
                                    }).toPromise();
        if (response.status === 'ok') {
            this.localStorageService.setItem(this.STORAGE_KEY_BINARY_TOKEN, response.token);
            this.localStorageService.setItem(this.STORAGE_KEY_BINARY_PROFILE_TYPE, (demo) ? 'Demo' : 'Live');
            binaryToken = response.token;
        } else {
            binaryToken = null;
        }
    }
    return binaryToken;
  }

  public isDemoAccount() {
    return this.localStorageService.getItem(this.STORAGE_KEY_BINARY_PROFILE_TYPE) === 'Demo';
  }

  public getBinaryTokenFromStorage() {
    return this.localStorageService.getItem(this.STORAGE_KEY_BINARY_TOKEN);
  }

  public async fetchProfileAsync(refresh = false) {
    let profile: BinaryProfile[] = null; // Problem with the API. Sends an array.
    try {
        profile = this.getProfileFromLocalStorage();
        if (profile === null || refresh) {
            profile = await this.fetchAsync<BinaryProfile[]>(this.URL(this.URL_PROFILE));
            profile[0] = this.fixProfileData(profile[0]);
            this.localStorageService.setItem(this.STORAGE_KEY_BINARY_PROFILE, JSON.stringify(profile));
        }
      } catch (error) {
        if (error.status === 403) { // Authentication Token is expired
            await this.authenticationService.logout().toPromise();
        }
      }
      if (profile && profile.length > 0) {
          return profile[0];
      } else {
          return null;
      }
  }

    public fixProfileData(profile: BinaryProfile) {
        profile.summary.total_withdraw_amount = profile.summary.total_withdraw_amount || 0;
        profile.summary.total_wins = profile.summary.total_wins || 0;
        return profile;
    }

  public async fetchTradesAsync() {
    return await this.fetchAsync(this.URL(this.URL_TRADES));
  }

  public async fetchModifiersAsync() {
    return await this.fetchAsync(this.URL(this.URL_MODIFIERS));
  }

  public async fetchDepositsAsync() {
    return await this.fetchAsync(this.URL(this.URL_DEPOSITS));
  }

  public async fetchWithdrawalsAsync() {
    return await this.fetchAsync(this.URL(this.URL_WITHDRAWALS));
  }

  public async createTradeAsync(trade: BinaryTrade) {
    return await this.createAsync(this.URL(this.URL_TRADES), trade);
  }

  public async createDepositAsync(deposit: BinaryDeposit) {
      return await this.createAsync(this.URL(this.URL_DEPOSITS), deposit);
  }

  public async createWithdrawalAsync(withdrawal: BinaryWithdrawal) {
    return await this.createAsync(this.URL(this.URL_WITHDRAWALS), withdrawal);
  }

  public async createAccountAsync(account: BinaryProfile) {
    try {
        account.demo = false;
        const profile = await this.postAsync(this.URL(this.URL_PROFILE), account, {}, true).toPromise();
        this.getAuthenticationTokenAsync(false, true); // This will save the token locally for the next requests
        return profile;
    } catch (error) {
        this.applicationEvent.fireAlert({
            title: 'Account not created',
            sub_title: 'Unable to create binary account at this time, please try again later',
            body: '',
            close_text: 'CANCEL',
            ok_text: 'OK'
        });
    }
  }

  public async createDemoAccountAsync(account: BinaryProfile) {
    account.demo = true;
    const profile = await this.postAsync(this.URL(this.URL_PROFILE), account, {}, true).toPromise();
    this.getAuthenticationTokenAsync(true, true); // This will save the token locally for the next requests
    const deposit: BinaryDeposit = { amount: this.DEMO_DEPOSIT_AMOUNT, reference: 'Demo Depost Ref# ',  date: new Date() };
    this.createDepositAsync(deposit);
    return profile;
  }



  private async fetchAsync<T>(url: string) {
    return await this.getAsync<T>(url , {}, await this.gethHeadersAsync()).toPromise();
  }

  private async createAsync<T>(url: string, data: any) {
      return await this.postAsync<T>(url, data, await this.gethHeadersAsync(), true).toPromise();
  }

  private URL(PATH: string) {
      return this.BASE_URL + PATH;
  }

  private async gethHeadersAsync(ignoreAuth: boolean = false) {
    let token = null;
    token = await this.getAuthenticationTokenAsync();
    const headers: { [key: string]: string } = {
      Authorization: token ? this.getAuthorizationHeader(token) : ''
    };
    return headers;
  }

  private removeBinaryDataFromLocalStorage() {
      this.localStorageService.clearItem(this.STORAGE_KEY_BINARY_TOKEN);
      this.localStorageService.clearItem(this.STORAGE_KEY_BINARY_PROFILE);
      this.localStorageService.clearItem(this.STORAGE_KEY_BINARY_PROFILE_TYPE);
  }

  private async handleAuthenticationChanged(event: AuthenticationEvent) {
    switch (event.status) {
        case 'logged-in':
        break;
        case 'logged-out':
            this.removeBinaryDataFromLocalStorage();
        break;
    }
  }

  private getProfileFromLocalStorage() {
    return JSON.parse(this.localStorageService.getItem(this.STORAGE_KEY_BINARY_PROFILE));
  }


}
