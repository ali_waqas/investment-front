export interface GenericEvent {
  name: string;
  payload?: any;
}
