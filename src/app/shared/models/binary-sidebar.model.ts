export const DashboardItems = [
    {
        id: 11,
        title: 'Dashboard',
        link: '/binary/dashboard',
        icon: 'fal fa-shield-alt'
      },
    //   { id: 12, title: 'Legion Platform', link: '/portal/dashboard', icon: 'fal fa-window' },
      {
        id: 14,
        title: 'Deposits',
        link: '/binary/deposit',
        icon: 'fal fa-dollar-sign'
      },
      {
        id: 15,
        title: 'Withdrawals',
        link: '/binary/withdraw',
        icon: 'fal fa-credit-card-front'
      },
      {
        id: 16,
        title: 'Trades',
        link: '/binary/trades',
        icon: 'fal fa-exchange-alt'
      }
];
