interface KeyValueData {
    [key: string]: string;
}

export interface AnalysisReport {
    analysis: DepositByDays;
}

export interface DepositByDays {
  deposit_by_days: KeyValueData[];
}
