export interface Setting {
  name: string;
  value: string;
  mode: string;
  id: number;
}
