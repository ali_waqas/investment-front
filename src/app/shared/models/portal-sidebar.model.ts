export enum NavItemType {
  Menu,
  NavTitle
}

export const DashboardItems = [
  {
    id: 0,
    title: 'Dashboard',
    link: '/portal/dashboard',
    icon: 'fal fa-shield-alt',
    hasBadge: false,
    type: NavItemType.Menu
  },
  {
    id: 1,
    title: 'My Profile',
    link: '/portal/profile',
    icon: 'fal fa-user',
    hasBadge: false,
    type: NavItemType.Menu
  },
  {
    id: 2,
    title: 'Genealogy',
    link: '/portal/network',
    icon: 'fal fa-sitemap',
    hasBadge: false,
    type: NavItemType.Menu

  },
  {
    id: 3,
    title: 'My Transactions',
    link: '/portal/transactions',
    icon: 'fal fa-exchange-alt',
    hasBadge: false,
    type: NavItemType.Menu

  },
  {
    id: 4,
    title: 'My Investment',
    link: '/portal/investments',
    icon: 'fal fa-shield-alt',
    hasBadge: false,
    type: NavItemType.Menu

  },
  {
    id: 5,
    title: 'Withdrawals',
    link: '/portal/withdrawals',
    icon: 'fal fa-credit-card-front',
    hasBadge: false,
    type: NavItemType.Menu

  },
  {
    id: 6,
    title: 'Rewards',
    link: '/portal/awards',
    icon: 'fal fa-trophy-alt',
    hasBadge: false,
    type: NavItemType.Menu
  },
  {
    id: 7,
    title: 'Support',
    link: '/portal/support',
    icon: 'ni ni-support',
    hasBadge: false,
    type: NavItemType.Menu

  },
  {
    id: 8,
    title: 'Messages',
    link: '/portal/tickets',
    icon: 'fal fa-envelope-open',
    hasBadge: true,
    type: NavItemType.Menu

  },
  {
    id: 10,
    title: 'FAQ',
    link: '/portal/faqs',
    icon: 'ni ni-question',
    hasBadge: false,
    type: NavItemType.Menu

  },
  {
    id: 401,
    title: 'Privacy Policy',
    link: '#',
    icon: 'fal fa-file-pdf',
    hasBadge: false,
    type: NavItemType.Menu

  },
  {
    id: 402,
    title: 'Terms & Conditions',
    link: '#',
    icon: 'fal fa-file-pdf',
    hasBadge: false,
    type: NavItemType.Menu

  }

];
