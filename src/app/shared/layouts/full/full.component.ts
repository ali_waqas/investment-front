import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { ProfileService } from '@app/portal/profile/profile.service';
import { ApplicationEvent } from '@app/shared/services/alert-modal.service';
import { Alert } from '@app/shared/models/alert.model';
import { AuthenticationService } from '@app/core';
import { AuthenticationEvent } from '@app/core/authentication/authentication.model';
declare var $: any;
@Component({
  selector: 'app-full-layout',
  templateUrl: './full.component.html',
  styleUrls: ['./full.component.scss']
})
export class FullComponent extends AbstractBaseComponent implements OnInit {
  constructor(
    private router: Router,
    protected profileService: ProfileService,
    private applicationEvent: ApplicationEvent,
    private authenticationService: AuthenticationService) {

    super(profileService);
    this.subscribe(
      this.applicationEvent.onAlert.subscribe(this.handleModalAlert.bind(this))
    );
    this.subscribe(
      this.authenticationService.onAuthenticationChange.subscribe(
        this.handleAuthenticationChange.bind(this)
      )
    );
  }

  ngOnInit() {}

  public handleModalAlert(alert: Alert) {
    this.alert = alert;
    (<any>$('#alert-modal-md')).modal();
  }

  public handleAuthenticationChange(event: AuthenticationEvent) {
    switch (event.status) {
      case 'logged-in':
        break;
      case 'logged-out':
        this.router.navigateByUrl('/login');
        break;
    }
  }

    public getSidebarClass() {
        const packageId = this.profileService.getPackageId();
        switch(packageId) {
            case 1:
                return "page-sidebar bronze";
            case 2:
                return "page-sidebar silver";
            case 3:
                return "page-sidebar gold";
            case 4:
                return "page-sidebar diamond";
        }
        return "page-sidebar guest";
    }

    public getPackageClass() {
        const packageId = this.profileService.getPackageId();
        if(this.profileService.getTotalDeposits() == 0 ) {
            return  "guest";
        } else {
            switch(packageId) {
                case 1:
                    return "bronze";
                case 2:
                    return "silver";
                case 3:
                    return "gold";
                case 4:
                    return "diamond";
            }
        }
    }
}
