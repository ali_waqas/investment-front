import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BlankComponent } from '@app/shared/layouts/blank/blank.component';
import { FullComponent } from '@app/shared/layouts/full/full.component';
import { SpinnerComponent } from '@app/shared/spinner.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ButtonLoaderComponent } from '@app/shared/button-loader';
import { FormErrorWrapperComponent } from '@app/shared/form-error-wrapper/form-error-wrapper.component';
import { FooterComponent } from './components/footer/footer.component';
import { PortalFooterComponent } from '@app/portal/footer/footer.component';
import { SidebarComponent } from '@app/portal/sidebar/sidebar.component';
import { DashboardComponent } from '@app/dashboard';
import { DepositsComponent } from '@app/portal/deposits/deposits.component';
import { HistoryComponent } from '@app/portal/history/history.component';
import { HeaderComponent } from '@app/portal/header/header.component';
import { ProfileComponent } from '@app/portal/profile/profile.component';
import { TransactionsComponent } from '@app/portal/transactions/transactions.component';
import { ReferralsWidgetComponent } from '@app/dashboard/dashboard/referrals-widget/referrals-widget.component';
import { ProfitsWidgetComponent } from '@app/dashboard/dashboard/profits-widget/profits-widget.component';
import { DashboardItemComponent } from '@app/dashboard/dashboard/dashboard-item/dashboard-item.component';
import { RequestErrorAlertComponent } from './request-error-alert/request-error-alert.component';
import { SubHeaderComponent } from '@app/portal/sub-header/sub-header.component';
import { WithdrawalsComponent } from '@app/portal/withdrawals/withdrawals.component';
import { InvestmentsComponent } from '@app/portal/investments/investments.component';
import { PerfectMoneyComponent } from '@app/portal/perfect-money/perfect-money.component';
import { InvoiceComponent } from '@app/portal/invoice/invoice.component';
import { BinaryDashboardComponent } from '@app/binary/dashboard/dashboard.component';
import { BinaryComponent } from '@app/binary/binary.component';
import { BinarySubHeaderComponent } from '@app/binary/sub-header/sub-header.component';
import { BinaryDepositsComponent } from '@app/binary/deposits/deposits.component';
import { BinaryWithdrawalsComponent } from '@app/binary/withdrawals/withdrawals.component';
import { BinaryTradesComponent } from '@app/binary/trades/trades.component';
import { BinaryDepositComponent } from '@app/binary/deposit/deposit.component';
import { BinaryWithdrawalComponent } from '@app/binary/withdrawal/withdrawal.component';
import { BinaryTradesWidgetComponent } from '@app/binary/dashboard/trades-widget/trades-widget.component';
import { TradeDialogueComponent } from '@app/binary/trade-dialogue/trade-dialogue.component';
import { IntroComponent } from '@app/frontend/intro/intro.component';
import { AboutComponent } from '@app/frontend/about/about.component';
import { NewsComponent } from '@app/frontend/news/news.component';
import { WebinarsComponent } from '@app/frontend/webinars/webinars.component';
import { WebinarsComponent as WebinarsPortal } from '@app/portal/webinars/webinars.component';
import { ContactComponent } from '@app/frontend/contact/contact.component';
import { FoundationComponent } from '@app/frontend/foundation/foundation.component';
import { FrontEndComponent } from '@app/frontend/frontend.component';
import { NotFoundComponent } from '@app/authentication/404';
import { SignupComponent } from '@app/authentication/signup';
import { LoginComponent } from '@app/authentication/login';
import { SubsriptionPaymentComponent } from '@app/frontend/subsription-payment/subsription-payment.component';
import { BinaryLoginComponent } from '@app/binary/login/login.component';
import { SupportComponent } from '@app/portal/support/support.component';
import { AwardsComponent } from '@app/portal/awards/awards.component';
import { WithdrawComponent } from '@app/portal/withdraw/withdraw.component';
import { ForgotPasswordComponent } from '@app/authentication/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from '@app/authentication/reset-password/reset-password.component';
import { NetworkComponent } from '@app/portal/network/network.component';
import { ClientsComponent } from '@app/portal/clients/clients.component';
import { LoadingComponent } from './components/footer/loading/loading.component';
import { MediaComponent } from '@app/portal/media/media.component';
import { InnerNewsComponent } from '@app/portal/inner-news/inner-news.component';
import { HomepageComponent } from '@app/portal/homepage/homepage.component';
import { PendingRegistrationsComponent } from '@app/portal/pending-registrations/pending-registrations.component';
import { TwoFAComponent } from '@app/frontend/two-fa/two-fa.component';
import { ClientDashboardItemsComponent } from '@app/dashboard/dashboard/client-dashboard-items/client-dashboard-items.component';
import { DepositsByDaysChartComponent } from '@app/dashboard/dashboard/deposits-by-days/deposits-by-days-chart.component';
import { TopClientDepositsChartComponent } from '@app/dashboard/dashboard/top-client-deposits-chart/top-client-deposits-chart.component';
import { DepositWidgetComponent } from '@app/dashboard/dashboard/deposit-widget/deposit-widget.component';
import { WithdrawalRequestsWidgetComponent } from '@app/dashboard/dashboard/withdrawal-requests-widget/withdrawal-requests-widget.component';
import { ClientSummaryDetailComponent } from '@app/portal/client-summary-detail/client-summary-detail.component';
import { ClientDashboardComponent } from '@app/portal/client-dashboard/client-dashboard.component';
import { FaqsComponent } from '@app/portal/faqs/faqs.component';
import { FrontendFaqsComponent } from '@app/frontend/faqs/frontend-faqs.component';
import { PackagesComponent } from '@app/portal/packages/packages.component';
import { KYCComponent } from '@app/portal/kyc/kyc.component';
import { CommissionsComponent } from '@app/portal/commissions/commission.component';
import { AcademySignUpComponent } from '@app/portal/academy-signup/academy-signup.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    NgbModule,
    ReactiveFormsModule
  ],
  declarations: [
    HomepageComponent,
    InnerNewsComponent,
    LoadingComponent,
    SpinnerComponent,
    BlankComponent,
    FullComponent,
    ButtonLoaderComponent,
    FormErrorWrapperComponent,
    HeaderComponent,
    FooterComponent,
    PortalFooterComponent,
    SidebarComponent,
    DashboardComponent,
    DepositsComponent,
    HistoryComponent,
    ProfileComponent,
    TransactionsComponent,
    ReferralsWidgetComponent,
    ProfitsWidgetComponent,
    DashboardItemComponent,
    RequestErrorAlertComponent,
    SubHeaderComponent,
    WithdrawalsComponent,
    InvestmentsComponent,
    PerfectMoneyComponent,
    InvoiceComponent,
    IntroComponent,
    AboutComponent,
    NewsComponent,
    FoundationComponent,
    ContactComponent,
    FrontEndComponent,
    SubsriptionPaymentComponent,
    WebinarsComponent,
    SupportComponent,
    AwardsComponent,
    WithdrawComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    NetworkComponent,
    WebinarsPortal,
    TwoFAComponent,
    FrontendFaqsComponent,
    FaqsComponent,

    // Binary
    BinaryComponent,
    BinarySubHeaderComponent,
    BinaryDashboardComponent,
    BinaryDepositsComponent,
    BinaryWithdrawalsComponent,
    BinaryTradesComponent,
    BinaryDepositComponent,
    BinaryWithdrawalComponent,
    BinaryTradesWidgetComponent,
    TradeDialogueComponent,
    NotFoundComponent,
    SignupComponent,
    LoginComponent,
    BinaryLoginComponent,
    ClientsComponent,
    MediaComponent,
    PendingRegistrationsComponent,
    ClientDashboardItemsComponent,
    DepositsByDaysChartComponent,
    TopClientDepositsChartComponent,
    DepositWidgetComponent,
    WithdrawalRequestsWidgetComponent,
    ClientSummaryDetailComponent,
    ClientDashboardComponent,
    PackagesComponent,
    KYCComponent,
    CommissionsComponent,
    AcademySignUpComponent

  ],
  exports: [
    HomepageComponent,
    InnerNewsComponent,
    SpinnerComponent,
    BlankComponent,
    FullComponent,
    ButtonLoaderComponent,
    FormErrorWrapperComponent,
    HeaderComponent,
    FooterComponent,
    PortalFooterComponent,
    SidebarComponent,
    DashboardComponent,
    DepositsComponent,
    HistoryComponent,
    ProfileComponent,
    TransactionsComponent,
    ReferralsWidgetComponent,
    ProfitsWidgetComponent,
    DashboardItemComponent,
    RequestErrorAlertComponent,
    SubHeaderComponent,
    WithdrawalsComponent,
    InvestmentsComponent,
    PerfectMoneyComponent,
    InvoiceComponent,
    IntroComponent,
    AboutComponent,
    NewsComponent,
    FoundationComponent,
    WebinarsComponent,
    ContactComponent,
    FrontEndComponent,
    // Binary
    BinaryComponent,
    BinarySubHeaderComponent,
    BinaryDashboardComponent,
    BinaryDepositsComponent,
    BinaryWithdrawalsComponent,
    BinaryTradesComponent,
    BinaryDepositComponent,
    BinaryWithdrawalComponent,
    BinaryTradesWidgetComponent,
    TradeDialogueComponent,
    BinaryLoginComponent,
    ClientsComponent,
    PendingRegistrationsComponent,
    TwoFAComponent,
    PackagesComponent,
    KYCComponent,
    CommissionsComponent
  ],
  entryComponents: [],
  providers: []
})
export class SharedModule {}
