import { Component, Input, OnInit } from '@angular/core';
import { TopClientDeposit } from '../dashboard.model';

declare var $:any;
declare var color:any;

@Component({
  selector: 'top-client-deposits-chart',
  templateUrl: './top-client-deposits-chart.component.html',
  styleUrls: ['./top-client-deposits-chart.component.css']
})
export class TopClientDepositsChartComponent implements OnInit {

    @Input() public topClientDeposits: TopClientDeposit[] = [];

    constructor() {
    }

    ngOnInit() {
        let dataSetPie: any = [];
        const colors = [
                        color.primary._500,
                        color.info._500,
                        color.warning._500,
                        color.danger._500,
                        color.success._500
                    ];
        let index = 0;
        this.topClientDeposits.slice(0, 5).forEach(topClientDeposits=> {
            dataSetPie.push(        {
                label: topClientDeposits.first_name + '  ' + topClientDeposits.last_name,
                data: topClientDeposits.number_of_deposits,
                color: colors[index]
            });
            index++;
        });

        $.plot($("#flotPie"), dataSetPie, {
                series:{
                        pie: {
                            innerRadius: 0.3,
                            show: true
                        }
                },
                legend: {
                    show: false
                }
        });
  }

  private labelFormatter(label: string, series: any)
  {
      return "<div class='fs-xs text-center p-1 text-white'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
  }
}
