import { Component, OnInit } from '@angular/core';
import { ProfileService } from '@app/portal/profile/profile.service';
import { BinaryIncome } from '@app/portal/profile/profile.model';
import { ApplicationEvent } from '@app/shared/services/alert-modal.service';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';

@Component({
  selector: 'app-dashboard-profits-widget',
  templateUrl: './profits-widget.component.html',
  styleUrls: ['./profits-widget.component.css']
})
export class ProfitsWidgetComponent extends AbstractBaseComponent
  implements OnInit {
  public binaryIncomes: BinaryIncome[];
  genericEvent: any;

  constructor(protected profileService: ProfileService) {
    super(profileService);
    this.dataTableName = 'tbl-binary-incomes';
  }

  ngOnInit() {
    this.fetchDataAsync();
  }

  private async fetchDataAsync() {
    this.loadingStart();
    const response: any = await this.profileService.getBinaryIncome();
    this.binaryIncomes = response.binary_income;
    this.setPages(this.binaryIncomes.length);
    this.addTableOption('responsive', true);
    this.addTableOption('pageLength', 5);
    this.addDataTableButtons();
    this.addTableOption('bFilter', false);
    this.initDataTable();
    this.loadingFinished();
  }
}
