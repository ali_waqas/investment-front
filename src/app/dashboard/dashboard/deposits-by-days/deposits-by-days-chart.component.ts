import { Component, OnInit } from '@angular/core';
import { ReportsService } from '@app/frontend/reports/reports.service';
import { AnalysisReport } from '@app/shared/models/report.model';

declare var $:any;
declare var color:any;

@Component({
  selector: 'app-deposits-by-days-chart',
  templateUrl: './deposits-by-days-chart.component.html',
  styleUrls: ['./deposits-by-days-chart.component.css']
})
export class DepositsByDaysChartComponent implements OnInit {

    private report: AnalysisReport;
    constructor(private reportsService: ReportsService) {

    }

    async ngOnInit() {
        this.fetchData();
    }

    private async fetchData() {
       this.report =  await this.reportsService.fetchDepositsByDays();
       this.buildChart();
    }

    private buildChart() {
        let dataSet1: any = [];
        let ticks: any = [];
        let yaxisTicks: any = [];
        let topDepositValue = 0;
        let minDepositValue = 0;
        let index = 0;
        this.report.analysis.deposit_by_days.forEach(entry=> {
            if(Number(entry.total) >= topDepositValue) {
                topDepositValue = Number(entry.total);
            }

            if(Number(entry.total) <= minDepositValue) {
                minDepositValue = Number(entry.total);
            } else if(minDepositValue == 0){
                minDepositValue = Number(entry.total);
            }
            dataSet1.push([index+1, Number(entry.total)]);
            ticks.push([index, this.formatDate(entry.date)]);
            index++;
        });
        yaxisTicks.push([minDepositValue, '$'+minDepositValue]);
        yaxisTicks.push([topDepositValue, '$'+topDepositValue]);
        $.plot('#flotBar1', [
            {
                data: dataSet1,
                bars:
                {
                    show: true,
                    lineWidth: 0,
                    fillColor: '#28a745',
                    barWidth: .3,
                    align: 'right'
                }
            }],
            {
                grid:
                {
                    borderWidth: 0,
                },
                yaxis:
                {
                    min: minDepositValue,
                    max: topDepositValue,
                    tickColor: '#F0F0F0',
                    ticks: yaxisTicks,
                    font:
                    {
                        color: '#000',
                        size: 10
                    }
                },
                xaxis:
                {
                    mode: 'categories',
                    tickColor: '#F0F0F0',
                    ticks: ticks,
                    font:
                    {
                        color: '#000',
                        size: 9
                    }
                }
            });
    }

    private randomIntFromInterval(min: number, max: number) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    private formatDate(dateString: string) {
        const d = new Date(dateString);
        return ('0' + d.getDate()).slice(-2)  + "-" + ('0' + (d.getMonth()+1)).slice(-2);
    }

}
