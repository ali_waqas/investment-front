import { Component, OnInit } from '@angular/core';
import { ProfileService } from '@app/portal/profile/profile.service';
import { Client } from '@app/portal/profile/profile.model';
import { DashboardItem } from './dashboard.model';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { ApplicationEvent } from '@app/shared/services/alert-modal.service';
import { CurrencyPipe } from '@angular/common';
import { LocalStorageService } from '@app/core';

@Component({
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent extends AbstractBaseComponent
  implements OnInit {
  public client: Client;
  public dashboardItems: DashboardItem[] = [];
  private readonly ANNOUNCEMENT_ID: string = 'announcement-6';
  private readonly ANNOUNCEMENT_NFT_ID: string = 'announcement-nft';
  private readonly ANNOUNCEMENT_THIRD_ID: string = 'announcement-third';
  private readonly ANNOUNCEMENT_FOUR_ID: string = 'announcement-four';
  private readonly ANNOUNCEMENT_5_ID: string = 'announcement-05';
  private readonly ANNOUNCEMENT_6_ID: string = 'announcement-06';
  private readonly ANNOUNCEMENT_7_ID: string = 'announcement-07';

  constructor(
    protected profileService: ProfileService,
    private localStorageService: LocalStorageService,
    private applicationEvent: ApplicationEvent,
    private currency: CurrencyPipe) {
    super(profileService);
  }

  protected async fetchClientProfile() {
    if (this.profileService.authenticated()) {
      this.client = await this.profileService.getProfileAsync();
    }
  }

  async ngOnInit() {
    this.pageTitle = 'Dashboard';
    this.pageIcon = 'fal fa-chart-area';
    this.ready = true;


    await this.fetchClientProfile();
    this.prepareDashboardItems();
    if (this.errorSubscription == null) {
      this.errorSubscription = this.onError.subscribe(
        this.handleErrors.bind(this)
      );
    }

    if (this.applicationEventSubscription == null) {
      this.applicationEventSubscription = this.applicationEvent.onProfileNeedsRefresh.subscribe(
        this.handleProfileRefresh.bind(this)
      );
    }

    this.showModal("#announcement-8");
    this.showModal("#announcement-7");
    this.showModal("#announcement-9");
    this.showModal("#announcement-11");
  }

  private prepareDashboardItems() {

    const award_amount = this.currency.transform(
      Number(this.client.profile.award_amount).toString()
    );

    const reward_amount = this.currency.transform(
      Number(this.client.profile.reward_amount).toString()
    );

    const daily_roi = this.currency.transform(
      Number(this.client.profile.daily_roi).toString()
    );
    const referral_income = this.currency.transform(
      Number(this.client.profile.overview.referral_income).toString()
    );
    const left_referrals = Number(
      this.client.profile.overview.left_referrals
    ).toString();
    const right_referrals = Number(
      this.client.profile.overview.right_referrals
    ).toString();
    const roi_wallet = this.currency.transform(
      Number(this.client.profile.overview.roi_wallet).toString()
    );
    const total_withdrawals = this.currency.transform(
      Number(this.client.profile.overview.total_withdrawals).toString()
    );
    const total_deposits = this.currency.transform(
      Number(this.client.profile.overview.total_deposits).toString()
    );

    this.dashboardItems = [
      {
        name: 'Rewarded',
        value: reward_amount,
        background_class: 'bg-primary-600 bg-1',
        icon: this.getPackageFolder() + '/7.png'
      },
      {
        name: 'Awarded',
        value: award_amount,
        background_class: 'bg-fusion-600 bg-6',
        icon: this.getPackageFolder() + '/7.png'
      },
      {
        name: 'Commissions',
        value: referral_income,
        background_class: 'bg-danger-600 bg-2',
        icon: this.getPackageFolder() + '/2.png'
      },
      {
        name: 'Withdrawals',
        value: total_withdrawals,
        background_class: 'bg-warning-400 bg-3',
        icon: this.getPackageFolder() + '/3.png'
      },
      {
        name: 'Left Referrals',
        value: left_referrals,
        background_class: 'bg-info-200 bg-4',
        icon: this.getPackageFolder() + '/4.png'
      },
      {
        name: 'Right Referrals',
        value: right_referrals,
        background_class: 'bg-success-400 bg-5',
        icon: this.getPackageFolder() + '/5.png'
      }
    ];
  }

  public handleErrors(error: any) {
    this.httpError = error;
    this.hasErrors = true;
  }

  private async handleProfileRefresh(event: any) {
    this.client = await this.profileService.getProfileAsync(true);
  }

  private getPackageFolder() {
    const packageId = this.profileService.getPackageId();
    let packageName = 'guest';

    if (this.client.profile.total_deposits === 0) {
      packageName = 'guest';
    } else {
      switch (packageId) {
        case 1:
          packageName = 'bronze';
          break;
        case 2:
          packageName = 'guest';
          break;
        case 3:
          packageName = 'default';
          break;
        case 4:
          packageName = 'diamond';
          break;
      }
    }
    return 'assets/images/dashboard-icons/' + packageName;
  }

  public closeModal(id: string) {
    (<any>$(id)).modal('hide');
    this.localStorageService.setItem(id, true);
  }

  public showModal(id: string) {
    const modalShownAlready = this.localStorageService.getItem(id);

    if (modalShownAlready == null) {
      (<any>$(id)).modal();
    }
  }

}
