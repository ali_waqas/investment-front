import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Peer, ReferralIncome } from '@app/portal/profile/profile.model';
import { ReferralsService } from '@app/portal/referrals/referrals.service';
import { Referral } from '@app/portal/referrals/referral.model';
import { ApplicationEvent } from '@app/shared/services/alert-modal.service';
import { ProfileService } from '@app/portal/profile/profile.service';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';

@Component({
  selector: 'app-dashboard-referrals-widget',
  templateUrl: './referrals-widget.component.html',
  styleUrls: ['./referrals-widget.component.css']
})
export class ReferralsWidgetComponent extends AbstractBaseComponent
  implements OnInit {
  @Output() errorsEmitter = new EventEmitter();
  public referral: Referral;
  public referralIncome: ReferralIncome[];

  genericEvent: any;
  dataTableName: string;

  constructor(
    private referralService: ReferralsService,
    protected profileService: ProfileService
  ) {
    super(profileService);
    this.dataTableName = 'tbl-referrals';
  }

  @Input() network: Peer[];

  ngOnInit() {
    this.fetchDataAsync();
  }

  private async fetchDataAsync() {
    this.loadingStart();
    const response: any = await this.profileService.getReferralIncome();
    this.referralIncome = response.referral_income;
    this.setPages(this.referralIncome.length);
    this.addTableOption('responsive', true);
    this.addTableOption('pageLength', 5);
    this.addDataTableButtons();
    this.addTableOption('bFilter', false);
    this.initDataTable();
    this.loadingFinished();
  }

  public async createReferral(direction: string) {
    try {
      this.referral = await this.referralService.createReferralAsync(
        direction,
        'Normal'
      );
      (<any>$('#new_referral')).modal();
      setTimeout(function() {
        const copyTextarea: any = document.getElementById('new_referral_text');
        copyTextarea.select();
      }, 500);
    } catch (error) {
      console.log(error);
      this.errorsEmitter.emit(error);
    }
  }

  public copyText() {
    const copyTextarea: any = document.getElementById('new_referral_text');
    copyTextarea.select();
    const result = document.execCommand('copy');
    alert('Your referral has been copied');
  }

  handleGenericEvent(event: any) {
    switch (event.whatHappened) {
      case 'scripts-loaded':
        (<any>$)('#tbl-referrals').dataTable({
          scrollY: '270px',
          scrollCollapse: true,
          paging: false,
          bFilter: false,
          bInfo: false
        });
        break;
    }
  }
}
