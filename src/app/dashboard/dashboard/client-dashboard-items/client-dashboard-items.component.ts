import { Component, OnInit, Input } from '@angular/core';
import { DashboardItem } from '../dashboard.model';

@Component({
  selector: 'app-client-dashboard-items',
  templateUrl: './client-dashboard-items.component.html',
  styleUrls: ['./client-dashboard-items.component.css']
})
export class ClientDashboardItemsComponent implements OnInit {

  constructor() { }
  @Input() dashboardItem: DashboardItem;
  ngOnInit() {
  }

}
