import { Injectable } from '@angular/core';
import { BaseAPIClass } from '@app/core/class/baseAPI.class';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { DashboardResponse } from './dashboard.model';

@Injectable({ providedIn: 'root' })
export class DashboardService extends BaseAPIClass {

    private readonly URL_FETCH_ADMIN_DATA: string = 'dashboard/admin';

    private dashboardData: DashboardResponse = null;
    constructor(
    protected httpClient: HttpClient,
    protected authenticationService: AuthenticationService) {
        super(httpClient, authenticationService);
    }

    public async fetchAdminDataAsync() {
        return this.getAsync(this.URL_FETCH_ADMIN_DATA, {}, {}).toPromise();
    }

    public async getAnalysisData() {
        if(this.dashboardData === null) {
            this.dashboardData = await this.fetchAdminDataAsync();
            return this.dashboardData;
        } else {
            return this.dashboardData;
        }
    }

}
