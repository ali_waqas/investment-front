export interface DashboardItem {
  name: string;
  value: string;
  background_class: string;
  icon: string;
}

export interface DashboardResponse {
  analysis?: Analysis;
  message?: string;
}

export interface Analysis {
  total_clients: number;
  total_deposits: number;
  total_withdrawals: number;
  top_client_deposits:TopClientDeposit;
}

export interface TopClientDeposit{
  client_id: number;
  number_of_deposits: number;
  amount: number;
  first_name: string;
  last_name: string;
}
