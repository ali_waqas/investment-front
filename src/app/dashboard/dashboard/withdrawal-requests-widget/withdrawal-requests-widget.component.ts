import { Component, OnInit } from '@angular/core';
import { AbstractWidgetComponent } from '@app/core/class/abstract.widget.omponent';
import { Withdrawal, WithdrawalRequestsData, WithdrawalRequestsFilter, WithdrawalRequestsResponse } from '@app/portal/withdrawals/withdrawal.model';
import { WithdrawalsService } from '@app/portal/withdrawals/withdrawals.service';

@Component({
  selector: 'app-withdrawal-requests-widget',
  templateUrl: './withdrawal-requests-widget.component.html',
  styleUrls: ['./withdrawal-requests-widget.component.css']
})
export class WithdrawalRequestsWidgetComponent extends AbstractWidgetComponent implements OnInit {

  public withdrawalRequests: WithdrawalRequestsData[] = [];
  private withdrawalRequestsFilter: WithdrawalRequestsFilter = { };
  ignoreBackdropClick: false;


  constructor(
    private withdrawalService: WithdrawalsService,
    ) {
    super();
    this.entityName = 'withdrawal-requests';
    this.dataTableName = 'app-withdrawal-requests-widgets-table';
  }

  ngOnInit() {
    this.fetchDataAsync();
  }

  private fetchWithdrawalRequests() {
    return this.withdrawalService.fetchWithdrawalRequestsAsync(this.withdrawalRequestsFilter);
  }

  private async fetchDataAsync() {
    this.loadingStart();
    const withdrawalRequestsResponse: WithdrawalRequestsResponse = await this.fetchWithdrawalRequests();
    this.withdrawalRequests = withdrawalRequestsResponse.withdrawal_requests.data;
    this.setPages(withdrawalRequestsResponse.withdrawal_requests.last_page);
    this.loadingFinished();
  }


  public typeClass(withdrawal: Withdrawal) {
    switch (withdrawal.status) {
        case 'PENDING':
            return 'badge badge-warning';
        case 'FAILED':
        case 'CANCELLED':
            return 'badge badge-danger';
        case 'COMPLETED':
            return 'badge badge-success';
    }
  }

  public async gotoPage(page: number) {
    this.withdrawalRequestsFilter.page = page;
    this.updateWithdrawalPagerLinks(page);
    this.fetchDataAsync();
  }


  protected updateWithdrawalPagerLinks(page: number) {
    $('.page-item-withdrawal').removeClass('active');
    $('.page-item-withdrawal:eq('+(page-1)+')').addClass('active');
}


}
