import { Component, OnInit, Input } from '@angular/core';
import { DashboardItem } from '../dashboard.model';

@Component({
  selector: '[]app-dashboard-item]',
  templateUrl: './dashboard-item.component.html',
  styleUrls: ['./dashboard-item.component.scss']
})
export class DashboardItemComponent implements OnInit {
  constructor() {}
  @Input() dashboardItem: DashboardItem;
  ngOnInit() {}
}
