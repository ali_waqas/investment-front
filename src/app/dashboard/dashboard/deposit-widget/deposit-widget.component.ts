import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Deposit, DepositData, DepositsFilter, DepositsResponse } from '@app/portal/deposits/deposit.model';
import { DepositsService } from '@app/portal/deposits/deposits.service';
import { AbstractWidgetComponent } from '@app/core/class/abstract.widget.omponent';

@Component({
  selector: 'app-deposit-widget',
  templateUrl: './deposit-widget.component.html',
  styleUrls: ['./deposit-widget.component.css']
})
export class DepositWidgetComponent extends AbstractWidgetComponent implements OnInit {

  public deposits: DepositData[] = [];
  private depositsFilter: DepositsFilter  = {};
  @Output() emitData = new EventEmitter();

  constructor(
    private depositService: DepositsService,
    ) {
    super();
    this.entityName = 'deposits';
    this.dataTableName = 'app-deposits-widgets-table';
  }

  ngOnInit() {
    this.fetchDataAsync();
  }

  private fetchDeposits() {
    return this.depositService.fetchDepositsAsync(this.depositsFilter);
  }

  private async fetchDataAsync() {
    this.loadingStart();
    const depositsResponse: DepositsResponse = await this.fetchDeposits();
    this.deposits = depositsResponse.deposits.data;
    this.setPages(depositsResponse.deposits.last_page);
    this.loadingFinished();
  }

  public async gotoPage(page: number) {
    this.depositsFilter.page = page;
    this.updatePagerLinks(page);
    this.fetchDataAsync();
  }
}
