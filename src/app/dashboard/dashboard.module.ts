import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DashboardRoutes } from '@app/dashboard/dashboard.routing';
import { ClientDashboardItemsComponent } from './dashboard/client-dashboard-items/client-dashboard-items.component';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    NgbModule,
    RouterModule.forChild(DashboardRoutes)
  ],
  declarations: [
  ClientDashboardItemsComponent]
})
export class DashboardModule {}
