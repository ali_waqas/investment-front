import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '@app/core';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { ProfileService } from '@app/portal/profile/profile.service';
import { SettingsService } from '@app/shared/services/settings-service';
import { PerfectMoneyRequest, Product } from '@app/portal/deposits/deposit.model';
import { DepositsService } from '@app/portal/deposits/deposits.service';

@Component({
  selector: 'app-binary-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class BinaryLoginComponent extends AbstractBaseComponent implements OnInit {
  loginForm: FormGroup;
  resetPasswordForm: FormGroup;
  public perfectMoneyRequest: PerfectMoneyRequest;
  public paymentNeeded = false;
  public accountActivated = false;

  isLoading = false;
  constructor(
    public router: Router,
    private formBuilder: FormBuilder,
    profileService: ProfileService,
    private settingsService: SettingsService,
    private depositService: DepositsService,
    private authenticationService: AuthenticationService) {
      super(profileService);
  }

  ngOnInit() {
    this.createForm();
    const params = this.router.parseUrl(this.router.url).queryParams;
    this.accountActivated = !!params.session;
  }

  async login() {
    this.isLoading = true;

  }

  public async createPerfectMoneyRequest() {
    try {
      console.log(this.perfectMoneyRequest);
      const failedUrl = await this.settingsService.get('perfect_money_failed_api_url');
      const successUrl = await this.settingsService.get('perfect_money_success_api_url');
      const request: any = await this.depositService.createPerfectMoneyRequest(this.perfectMoneyRequest);
      this.perfectMoneyRequest = request.perfect_money_request;
      (<any>$('#form_payment_id')).val(this.perfectMoneyRequest.payment_id);
      (<any>$('#form_payment_amount')).val(this.perfectMoneyRequest.payment_amount);
      (<any>$('#payment_url')).val(successUrl.value);
      (<any>$('#no_payment_url')).val(failedUrl.value);
      (<any>$('#perfectmoney_step_1')).submit();
    } catch (error) {
      this.httpError = error;
      this.hasErrors = true;
    }
  }

  resetPassword() {
    // TODO: Implement Reset Password
  }

  private createForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', Validators.required]
    });
    this.resetPasswordForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
  }
}
