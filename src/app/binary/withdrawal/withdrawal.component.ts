import { Component, OnInit } from '@angular/core';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProfileService } from '@app/portal/profile/profile.service';
import { WithdrawalsService } from '@app/portal/withdrawals/withdrawals.service';
import { Withdrawal, WithdrawalWireTransfer, WithdrawalCrypto } from '@app/portal/withdrawals/withdrawal.model';
import { BinaryWithdrawal } from '../withdrawals/withdrawal.model';
import { BinaryService } from '@app/shared/services/binary.service';


@Component({
  selector: 'app-binary-withdraw',
  templateUrl: './withdrawal.component.html',
  styleUrls: ['./withdrawal.component.css']
})
export class BinaryWithdrawalComponent extends AbstractBaseComponent implements OnInit {
  public withdrawalForm: FormGroup;
  public withdrawalWithWireTransferForm: FormGroup;
  public wireTransferForm: FormGroup;
  public withdrawalWithCrypto: FormGroup;

  constructor(
    protected profileService: ProfileService,
    protected withdrawalsService: WithdrawalsService,
    private formBuilder: FormBuilder,
    private binaryService: BinaryService) {
    super(profileService);
  }

  async ngOnInit() {
    this.pageTitle = 'Withdrawals';
    this.pageIcon = 'fal fa-credit-card-front';
    this.client = await this.profileService.getProfileAsync();
    this.createWithdrawalForm();
    this.createCryptoWithdrawalForm();

    this.ready = true;
  }

  public async withdrawNowAsync() {
    try {
      const withdrawalsRequest: BinaryWithdrawal = this.withdrawalForm.value;
      const withdrawal = await this.binaryService.createWithdrawalAsync(withdrawalsRequest);
      this.client = await this.profileService.getProfileAsync(true);
      alert('Withdrawal has been saved successfully.');
    } catch (error) {
      this.httpError = error;
      this.hasErrors = true;
    }
  }

  public createWithdrawalForm() {
    this.withdrawalForm = this.formBuilder.group({
      amount: ['', [Validators.required]],
      reference: ['', [Validators.required]]
    });
  }

  public createCryptoWithdrawalForm() {
    this.withdrawalWithCrypto = this.formBuilder.group({
      first_name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      wallet_address: ['', [Validators.required]],
      amount: ['', [Validators.required]],
    });
  }

  public async withdrawWithCryptoAsync() {
    try {
        const cryptoWithdrawalRequest: WithdrawalCrypto = this.withdrawalWithCrypto.value;
        const withdrawal = await this.withdrawalsService.createWitdrawalWithCryptoAsync(cryptoWithdrawalRequest);
        alert('Withdrawal has been saved successfully.');
      } catch (error) {
        this.httpError = error;
        this.hasErrors = true;
      }
  }
}
