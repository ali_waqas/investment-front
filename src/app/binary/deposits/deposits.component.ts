import { Component, OnInit } from '@angular/core';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { BinaryDeposit } from './deposit.model';
import { BinaryService } from '@app/shared/services/binary.service';
import { ProfileService } from '@app/portal/profile/profile.service';
import { SettingsService } from '@app/shared/services/settings-service';
import { PerfectMoneyRequest } from '@app/portal/deposits/deposit.model';
import { FormGroup } from '@angular/forms';
import { DepositsService } from '@app/portal/deposits/deposits.service';

@Component({
  selector: 'app-binary-deposits',
  templateUrl: './deposits.component.html',
  styleUrls: ['./deposits.component.scss']
})
export class BinaryDepositsComponent extends AbstractBaseComponent implements OnInit {

  public deposits: BinaryDeposit[];
  public perfectMoneyForm: FormGroup;
  constructor(
              profileService: ProfileService,
              private binaryService: BinaryService) {
    super(profileService);
   }

  async ngOnInit() {
    this.pageIcon = '';
    this.pageTitle = 'Deposits';
    const deposits: any = await this.binaryService.fetchDepositsAsync();
    this.deposits = deposits;
  }
}
