export interface BinaryDeposit {
    amount: number;
    reference: string;
    date: Date;
}
