import { Component, OnInit } from '@angular/core';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { FormBuilder, FormGroup, Validators, ValidationErrors } from '@angular/forms';
import { ProfileService } from '@app/portal/profile/profile.service';
import { BinaryService } from '@app/shared/services/binary.service';
import { PerfectMoneyRequest } from '@app/portal/deposits/deposit.model';
import { BinaryTrade } from '../trades/trade.model';
import { ApplicationEvent } from '@app/shared/services/alert-modal.service';
import { TradeFeed } from '../dashboard/dashboard.model';

@Component({
  selector: 'app-binary-trade-dialogue',
  templateUrl: './trade-dialogue.component.html',
  styleUrls: ['./trade-dialogue.component.css']
})
export class TradeDialogueComponent extends AbstractBaseComponent implements OnInit {
  public tradeForm: FormGroup;
  public perfectMoneyRequest: PerfectMoneyRequest;
  genericEvent: any;
  public tradeFeed: TradeFeed;
  trade: BinaryTrade;
  constructor(
    protected profileService: ProfileService,
    protected binaryService: BinaryService,
    private applicationEvent: ApplicationEvent,
    private formBuilder: FormBuilder) {
    super(profileService);
    if (this.genericEvent == null) {
        this.genericEvent = this.applicationEvent.onGenericEvent.subscribe(this.handleGenericEvent.bind(this));
    }
  }

    async ngOnInit() {
        this.createTradeForm();
    }

    public async createTrade(position: string) {
        try {
            this.trade = this.tradeForm.value;
            this.trade.position = position;
            this.trade.asset = this.tradeFeed.name;
            this.trade.callback_url = 'http://server.optionslegion.com/binary/trade/closed';
            const newTrade = await this.binaryService.createTradeAsync(this.trade);
            this.applicationEvent.fireGenericEvent({whatHappened: 'trade-opened', newTrade});
        } catch (error) {
            this.httpError = error;
            this.hasErrors = true;
        }
    }

    public createTradeForm() {
        this.tradeForm = this.formBuilder.group({
            position: ['', []],
            trade_amount: ['', [Validators.required]],
            trade_type: [null, [Validators.required]],
            asset: ['', []],
            callback_url: ['', []],
        });
    }

    getFormValidationErrors() {
        Object.keys(this.tradeForm.controls).forEach(key => {
        const controlErrors: ValidationErrors = this.tradeForm.get(key).errors;
        if (controlErrors != null) {
            Object.keys(controlErrors).forEach(keyError => {
                console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
            });
        }
        });
    }

    handleGenericEvent(event: any) {
        switch (event.whatHappened) {
            case 'open-trade-modal':
                this.tradeFeed = event.tradeFeed;
                this.ready = true;
            break;
        }
    }
}