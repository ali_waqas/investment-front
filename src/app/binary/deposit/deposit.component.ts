import { Component, OnInit } from '@angular/core';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { FormBuilder, FormGroup, Validators, ValidationErrors } from '@angular/forms';
import { SettingsService } from '@app/shared/services/settings-service';
import { ProfileService } from '@app/portal/profile/profile.service';
import { BinaryService } from '@app/shared/services/binary.service';
import { PerfectMoneyRequest, Product } from '@app/portal/deposits/deposit.model';
import { DepositsService } from '@app/portal/deposits/deposits.service';
import { LocalStorageService } from '@app/core';

@Component({
  selector: 'app-binary-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.css']
})
export class BinaryDepositComponent extends AbstractBaseComponent implements OnInit {
  public depositForm: FormGroup;
  public perfectMoneyForm: FormGroup;
  public wireTransferForm: FormGroup;
  public perfectMoneyRequest: PerfectMoneyRequest;
  constructor(
    protected profileService: ProfileService,
    protected binaryService: BinaryService,
    private formBuilder: FormBuilder,
    private depositService: DepositsService,
    private settingsService: SettingsService) {
    super(profileService);
  }

  async ngOnInit() {
    this.pageTitle = 'Deposits';
    this.pageIcon = 'fal fa-dollar-sign';
    this.client = await this.profileService.getProfileAsync();
    this.createDepositForm();
    this.createWireTransferForm();
    this.createPerfectMoneyForm();
    this.ready = true;
  }

  public async depositNowAsync() {
    // try {
    //   const depositRequest: Deposit = this.depositForm.value;
    //   depositRequest.method_id = '1';
    //   const deposit = await this.depositService.createDepositAsync(depositRequest);
    //   this.client = await this.profileService.getProfileAsync(true);
    //   alert('Deposit has been saved successfully.');
    // } catch (error) {
    //   this.httpError = error;
    //   this.hasErrors = true;
    // }
  }

  public async createPerfectMoneyRequest() {
    try {
      const failedUrl = await this.settingsService.get('perfect_money_failed_api_url');
      const successUrl = await this.settingsService.get('perfect_money_success_api_url');
      const perfectMoneyRequest: PerfectMoneyRequest = this.perfectMoneyForm.value;
      perfectMoneyRequest.payment_units = 'USD';
      perfectMoneyRequest.token = this.binaryService.getBinaryTokenFromStorage();
      perfectMoneyRequest.product = Product.BINARY_TRADE;
      perfectMoneyRequest.is_subscription_fee = 'No';
      const request: any = await this.depositService.createPerfectMoneyRequest(perfectMoneyRequest);
      this.perfectMoneyRequest = request.perfect_money_request;
      (<any>$('#form_payment_id')).val(this.perfectMoneyRequest.payment_id);
      (<any>$('#form_payment_amount')).val(this.perfectMoneyRequest.payment_amount);
      (<any>$('#payment_url')).val(successUrl.value);
      (<any>$('#no_payment_url')).val(failedUrl.value);
      (<any>$('#perfectmoney_step_1')).submit();
    } catch (error) {
      this.httpError = error;
      this.hasErrors = true;
    }
  }

//   public async saveWireDetailsAsync() {
//     try {
//       const wireTransferRequest: WireTransfer = this.wireTransferForm.value;
//       console.log(wireTransferRequest);
//       const wireTransfer = await this.depositService.createWireTransferAsync(wireTransferRequest);
//       this.client = await this.profileService.getProfileAsync(true);
//       alert('Wire Details have been saved successfully. Your account will be credited once the transaction is confirmed.');
//     } catch (error) {
//       this.httpError = error;
//       this.hasErrors = true;
//     }
//   }

  public createDepositForm() {
    this.depositForm = this.formBuilder.group({
      first_name: [this.client.profile.first_name, [Validators.required]],
      last_name: [this.client.profile.last_name, [Validators.required]],
      email: [this.client.profile.email, [Validators.required]],
      amount: ['', [Validators.required]],
      card_number: ['', [Validators.required]],
      expiry: ['', [Validators.required]],
    });
  }

  public async createPerfectMoneyForm() {
     this.perfectMoneyForm = this.formBuilder.group({
      first_name: [this.client.profile.first_name, [Validators.required]],
      last_name: [this.client.profile.last_name, [Validators.required]],
      email: [this.client.profile.email, [Validators.required]],
      payment_amount: ['', [Validators.required]],
    });
  }

  public createWireTransferForm() {
    this.wireTransferForm = this.formBuilder.group({
      bank: ['', [Validators.required]],
      country: ['', [Validators.required]],
      city: ['', [Validators.required]],
      wire_date: ['', [Validators.required]],
      amount: ['', [Validators.required]],
    });
  }

  getFormValidationErrors() {
    Object.keys(this.depositForm.controls).forEach(key => {

    const controlErrors: ValidationErrors = this.depositForm.get(key).errors;
    if (controlErrors != null) {
          Object.keys(controlErrors).forEach(keyError => {
            console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
          });
        }
      });
    }
}
