export interface BinaryTrade {
    position: string;
    profit?: string;
    closeing_time?: Date;
    revenue?: any;
    closing_price?: string;
    is_profitable?: any;
    open_time?: Date;
    trade_id?: string;
    is_closed?: boolean;
    trade_amount: number;
    opening_price?: string;
    trade_type?: string;
    asset: string;
    callback_url?: string;
}
