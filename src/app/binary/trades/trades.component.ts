import { Component, OnInit } from '@angular/core';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { BinaryService } from '@app/shared/services/binary.service';
import { ProfileService } from '@app/portal/profile/profile.service';
import { BinaryTrade } from './trade.model';

@Component({
  selector: 'app-binary-trades',
  templateUrl: './trades.component.html',
  styleUrls: ['./trades.component.scss']
})
export class BinaryTradesComponent extends AbstractBaseComponent implements OnInit {

  public trades: BinaryTrade[];
  constructor(profileService: ProfileService, private binaryService: BinaryService) {
    super(profileService);
   }

  async ngOnInit() {
    this.pageIcon = '';
    this.pageTitle = 'Trades';
    const trades: any = await this.binaryService.fetchTradesAsync();
    this.trades = trades;
  }

  public tradeProfitClass(trade: BinaryTrade) {
    switch (trade.is_profitable) {
        case true:
            return 'text-success';
        break;
        case false:
            return 'text-danger';
        break;
        case null:
            return 'text-default';
        break;
    }
  }
}
