import { BinaryDeposit } from '../deposits/deposit.model';
import { BinaryTrade } from '../trades/trade.model';
import { BinaryWithdrawal } from '../withdrawals/withdrawal.model';

export interface BinaryProfile {
    email: string;
    demo?: boolean;
    summary?: Summary;
    balance?: number;
    deposits?: BinaryDeposit[];
    trades?: BinaryTrade[];
    withdraws?: BinaryWithdrawal[];
}


export interface Summary {
    total_deposit_amount: number;
    total_withdraw_amount: number;
    total_deposits: number;
    total_withdraws: number;
    total_losses: number;
    total_wins: number;
    total_trades: number;
    total_trade_amount: number;
    total_open_trades: number;
    total_open_trade_amount?: number;
}
