import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { Client, ShortProfile } from '@app/portal/profile/profile.model';
import { ProfileService } from '@app/portal/profile/profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent extends AbstractBaseComponent implements OnInit {
  public profileForm: FormGroup;
  public client: Client;
  constructor(
    protected profileService: ProfileService,
    private formBuilder: FormBuilder
  ) {
    super(profileService);
  }

  async ngOnInit() {
    this.pageTitle = 'Profile';
    this.pageIcon = 'fal fa-chart-area';
    await this.getProfile();
  }

  private async getProfile() {
    this.client = await this.profileService.getProfileAsync();
    this.createProfileForm();
    this.ready = true;
  }

  public async updateProfile() {
    const shortProfile: ShortProfile = this.profileForm.value;
    try {
      const updatedProfile = await this.profileService.updateProfileAsync(shortProfile);
    } catch (error) {
      this.httpError = error;
      this.hasErrors = true;
    }
  }

  private createProfileForm() {
    this.profileForm = this.formBuilder.group({
      first_name: [this.client.profile.first_name, [Validators.required]],
      last_name: [this.client.profile.last_name, [Validators.required]],
      email: [this.client.profile.email, [Validators.required]],
      mobile: [this.client.profile.mobile, [Validators.required]],
      phone: [this.client.profile.phone],
      city: [this.client.profile.city],
      address: [this.client.profile.address],
      country: [this.client.profile.country]
    });
  }
}
