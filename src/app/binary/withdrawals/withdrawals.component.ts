import { Component, OnInit } from '@angular/core';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { BinaryService } from '@app/shared/services/binary.service';
import { ProfileService } from '@app/portal/profile/profile.service';
import { BinaryWithdrawal } from './withdrawal.model';

@Component({
  selector: 'app-binary-withdrawals',
  templateUrl: './withdrawals.component.html',
  styleUrls: ['./withdrawals.component.scss']
})
export class BinaryWithdrawalsComponent extends AbstractBaseComponent implements OnInit {

  public withdrawals: BinaryWithdrawal[];
  constructor(profileService: ProfileService, private binaryService: BinaryService) {
    super(profileService);
   }

  async ngOnInit() {
    this.pageIcon = '';
    this.pageTitle = 'Withdrawals';
    const withdrawals: any = await this.binaryService.fetchWithdrawalsAsync();
    this.withdrawals = withdrawals;
  }
}
