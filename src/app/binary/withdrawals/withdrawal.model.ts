
export interface BinaryWithdrawal {
    amount: number;
    reference: string;
    date?: Date;
}
