import { Component, OnInit, Input } from '@angular/core';
import { BinaryProfile } from '../profile/profile.model';
import { BinaryService } from '@app/shared/services/binary.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-binary-sub-header',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.css']
})

export class BinarySubHeaderComponent implements OnInit {
  constructor(
        private binaryService: BinaryService,
        private router: Router) {}
    public profile: BinaryProfile;
    public isDemo = false;
    @Input() title?: string;
    @Input() icon?: string;

    async ngOnInit() {
        this.profile = await this.binaryService.fetchProfileAsync(true);
        this.isDemo = await this.binaryService.isDemoAccount();
    }

    public async switchToDemoAccount() {
      try {
        const binaryProfile = <BinaryProfile> await this.binaryService.createDemoAccountAsync({email: this.profile.email});
      } catch (error) {
        await this.binaryService.getAuthenticationTokenAsync(true, true);
        this.refresh();
      }
    }

    public async switchToRealAccount() {
        try {
            await this.binaryService.getAuthenticationTokenAsync(false, true);
            this.refresh();
          } catch (error) {
              console.log(error);
        }
    }

    refresh(): void {
        window.location.reload();
    }
}
