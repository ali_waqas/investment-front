import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProfileService } from '@app/portal/profile/profile.service';
import { Client } from '@app/portal/profile/profile.model';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { ApplicationEvent } from '@app/shared/services/alert-modal.service';
import { DashboardItem } from '@app/dashboard/dashboard/dashboard.model';
import { BinaryService } from '@app/shared/services/binary.service';
import { TradeFeed, Asset } from './dashboard.model';
import { BinaryProfile } from '../profile/profile.model';

declare const TradingView: any;
declare const $: any;

@Component({
  selector: 'app-binary-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class BinaryDashboardComponent extends AbstractBaseComponent
implements OnInit, OnDestroy {
    private readonly ASSET_URL = 'https://s.tradingview.com/widgetembed/?frameElementId=tradingview_e9357&amp;symbol=__SYMBOL__&amp;interval=1&amp;symboledit=1&amp;saveimage=1&amp;toolbarbg=f1f3f6&amp;details=1&amp;studies=%5B%5D&amp;theme=dark&amp;style=1&amp;timezone=Etc%2FUTC&amp;withdateranges=1&amp;studies_overrides=%7B%7D&amp;overrides=%7B%7D&amp;enabled_features=%5B%5D&amp;disabled_features=%5B%5D&amp;locale=en&amp;utm_source=localhost&amp;utm_medium=widget&amp;utm_campaign=chart&amp;utm_term=__SYMBOL__';
    private readonly WS_ENDPOINT: string = 'wss://bo.converly.io/ws/';
    public client: Client;
    public dashboardItems: DashboardItem[] = [];
    private genericEvent: any;
    private flotChartsAvailable: false;
    public binaryProfile: BinaryProfile;
    public feed: TradeFeed[] = [];
    public assets_1: Asset[] = [];
    public assets_2: Asset[] = [];

    constructor(protected profileService: ProfileService,
        private applicationEvent: ApplicationEvent,
        private binaryService: BinaryService) {
        super(profileService);
        if (this.genericEvent == null) {
            this.genericEvent = this.applicationEvent.onGenericEvent.subscribe(this.handleGenericEvent.bind(this));
        }
    }

    ngOnInit() {
        $('body').addClass('nav-function-hidden');
        this.pageTitle = 'Dashboard';
        this.pageIcon = 'fal fa-chart-area';
        this.intializeAsync();
        if (this.errorSubscription == null) {
            this.errorSubscription = this.onError.subscribe(this.handleErrors.bind(this));
        }

        if (this.applicationEventSubscription == null) {
            this.applicationEventSubscription = this.applicationEvent.onProfileNeedsRefresh.subscribe(this.handleProfileRefresh.bind(this));
        }
    }
    
    private async intializeAsync() {
        this.openTradeDataRead();
        this.binaryProfile = await this.binaryService.fetchProfileAsync() as BinaryProfile;
        const assetsData = await this.binaryService.fetchModifiersAsync();
        let i = 0;
        for (const [key, value] of Object.entries(assetsData)) {
            if ( i % 2 === 0 ) {
                this.assets_1.push({name: key, cofficient: value});
            } else {
                this.assets_2.push({name: key, cofficient: value});
            }
            i++;
        }
        this.ready = true;
    }

    public handleErrors(error: any) {
        this.httpError = error;
        this.hasErrors = true;
    }

    private async handleProfileRefresh(event: any) {
        this.client = await this.profileService.getProfileAsync(true);
    }

    private openTradeDataRead() {
        const clazz = this;
        const socket = new WebSocket(this.WS_ENDPOINT);
        socket.onopen = function (e) { };
        socket.onmessage = function (event) {
            const data = JSON.parse(event.data);
            const feed: TradeFeed[] = [];
            Object.keys(data).forEach(key => {
            if (key !== 'time_stamp') {
                const id = key.replace('/', '-');
                const price = (data[key] === 'OFF') ? 0.00 : data[key].toFixed(6);
                feed.push({
                id,
                price,
                name: key,
                available: (price > 0)
                });
            }
            });
            clazz.feed = feed;
        };
        socket.onclose = function (event) {
            if (event.wasClean) {
            alert(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
            } else {
            alert('[close] Connection died');
            }
        };
        socket.onerror = function (error) {};
    }

    handleGenericEvent(event: any) {
        switch (event.whatHappened) {
            case 'trade-opened':
            this.closeTradeWindow();
            break;
        }
    }

    public tradeNow(tradeFeed: TradeFeed) {
        this.applicationEvent.fireGenericEvent({
            whatHappened: 'open-trade-modal',
            tradeFeed
        });
        ( < any > $('#open-trade-modal-md')).modal();
    }

    private closeTradeWindow() {
        ( < any > $('#open-trade-modal-md')).modal('hide');
    }

    public openAssets() {
        ( < any > $('#assets-modal')).modal();
    }

    public closeAssets() {
        ( < any > $('#assets-modal')).modal('hide');
    }

    ngOnDestroy() {
        $('body').removeClass('nav-function-hidden');
    }

    public loadAsset(asset: Asset) {
        const feed = this.feed.find(feed_ => asset.name === feed_.name);
        if (feed) {
            this.closeAssets();
            this.tradeNow(feed);
        }
    }
}
