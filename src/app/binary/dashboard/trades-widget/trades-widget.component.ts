import { Component, OnInit } from '@angular/core';
import { ProfileService } from '@app/portal/profile/profile.service';
import { BinaryIncome } from '@app/portal/profile/profile.model';
import { BinaryService } from '@app/shared/services/binary.service';
import { BinaryTrade } from '@app/binary/trades/trade.model';
import { ApplicationEvent } from '@app/shared/services/alert-modal.service';

@Component({
  selector: 'app-binary-dashboard-trades-widget',
  templateUrl: './trades-widget.component.html',
  styleUrls: ['./trades-widget.component.css']
})
export class BinaryTradesWidgetComponent implements OnInit {
  public trades: BinaryTrade[];
  genericEvent: any;
  constructor(
      private binaryService: BinaryService,
      private applicationEvent: ApplicationEvent) {
        if (this.genericEvent == null) {
            this.genericEvent = this.applicationEvent.onGenericEvent.subscribe(this.handleGenericEvent.bind(this));
        }
  }

  async ngOnInit() {
    await this.refreshTrades();
  }

  async refreshTrades() {
    const response: any = await this.binaryService.fetchTradesAsync();
    this.trades = response;
  }
  public tradeProfitClass(trade: BinaryTrade) {
    switch (trade.is_profitable) {
        case true:
            return 'text-success';
        break;
        case false:
            return 'text-danger';
        break;
        case null:
            return 'text-default';
        break;
    }
  }

  async handleGenericEvent(event: any) {
    switch (event.whatHappened) {
        case 'trade-opened':
            await this.refreshTrades();
        break;
    }
  }
}
