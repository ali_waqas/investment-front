export interface TradeFeed {
    id: string;
    name: string;
    price: number;
    available: boolean;
}


export interface Asset {
    name: string;
    cofficient: string;
}
