import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule, FullComponent } from '@app/shared';
import { BinaryDashboardComponent } from './dashboard/dashboard.component';
import { BinaryDepositsComponent } from './deposits/deposits.component';
import { BinaryWithdrawalsComponent } from './withdrawals/withdrawals.component';
import { BinaryTradesComponent } from './trades/trades.component';
import { BinaryDepositComponent } from './deposit/deposit.component';
import { BinaryWithdrawalComponent } from './withdrawal/withdrawal.component';
import { BinaryLoginComponent } from './login/login.component';


@NgModule({
  declarations: [
  ],
  imports: [
    SharedModule,
    RouterModule.forChild([
        {
            path: 'binary',
            redirectTo: 'binary/dashboard',
            pathMatch: 'full'
        },
        {
          path: 'dashboard',
          component: BinaryDashboardComponent
        },
        {
          path: 'deposits',
          component: BinaryDepositsComponent
        },
        {
            path: 'deposit',
            component: BinaryDepositComponent
        },
        {
            path: 'withdrawals',
            component: BinaryWithdrawalsComponent
        },
        {
            path: 'withdraw',
            component: BinaryWithdrawalComponent
        },
        {
            path: 'trades',
            component: BinaryTradesComponent
        }
    ])
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [],
  providers: []
})
export class BinaryModule {}
