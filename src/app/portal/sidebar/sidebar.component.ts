import { GenericEvent } from './../../shared/models/generic-event.model';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Location } from '@angular/common';
import { ProfileService } from '../profile/profile.service';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { DashboardItems as PortalDashboardItems, NavItemType } from '@app/shared/models/portal-sidebar.model';
import { DashboardItems as BinaryDashboardItems } from '@app/shared/models/binary-sidebar.model';
import { Router } from '@angular/router';
import { Product } from '../deposits/deposit.model';
import { Alert } from '@app/shared/models/alert.model';
import { ApplicationEvent } from '@app/shared/services/alert-modal.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent extends AbstractBaseComponent implements OnInit {
  public NavbarItems: NavbarItem[] = [];
  public messageCount: MessageCount;
  public ready = false;
  @Output() public emitData = new EventEmitter();
  public NavItemType: NavItemType;
  constructor(
            protected profileService: ProfileService,
            private location: Location,
            private applicationEvent: ApplicationEvent,
            private router: Router) {
    super(profileService);
  }

  async ngOnInit() {
      this.subscribe(this.applicationEvent.onGenericEvent.subscribe(this.handelEvent.bind(this)));
      this.client = await this.profileService.getProfileAsync();
      this.loadSideBarItems();
      this.getUnreadMessageNumber()
  }

  public async getUnreadMessageNumber() {
    try {
      console.log('Call Unread message number,');
      const messageUnreadCountResponse: MessageCount = await this.profileService.postMarkAsRead();
      this.messageCount = messageUnreadCountResponse;
      this.applicationEvent.fireGenericEvent({name: 'unread-message-count-ready', payload: this.messageCount});
      this.ready = true;
    } catch (error) {
      const alert: Alert = {
        title: 'Error',
        sub_title: '',
        body: error,
        ok_text: 'OK',
        close_text: 'CLOSE'
      };
      this.applicationEvent.fireAlert(alert);
    }
  }

  loadSideBarItems() {
    if (this.location.path().includes('binary')) {
        this.platform = Product.BINARY_TRADE;
        this.NavbarItems = BinaryDashboardItems;
    } else {
        this.platform = Product.INVESTMENT_PLATFORM;
        this.NavbarItems = PortalDashboardItems;
        if (this.isAdmin()) {
            this.NavbarItems.push({
                id: 41,
                title: 'Administrators',
                link: 'javascript:;',
                icon: 'fal fa-user',
                type: NavItemType.Menu,
                subMenu: [
                  {
                    id: 42,
                    title: 'Clients',
                    link: '/portal/clients',
                    type: NavItemType.Menu,
                    icon: 'fal fa-users'
                  },
                  {
                    id: 43,
                    title: 'Pending Registrations',
                    link: '/portal/pending-registrations',
                    type: NavItemType.Menu,
                    icon: 'fal fa-user',
                  },
                  {
                    id: 44,
                    title: 'Packages',
                    link: '/portal/packages',
                    type: NavItemType.Menu,
                    icon: 'fal fa-cubes',
                  },
                  {
                    id: 45,
                    title: 'KYC',
                    link: '/portal/kyc',
                    type: NavItemType.Menu,
                    icon: 'fal fa-file-pdf',
                  },
                  {
                    id: 46,
                    title: 'Commissions',
                    link: '/portal/commissions',
                    type: NavItemType.Menu,
                    icon: 'fal fa-user',
                  },
              ]
            });
        }
    }
    this.buildNavigationMenu();
  }

  private buildNavigationMenu() {
    const navSpeed = 500;
    const 	navClosedSign =  'fal fa-angle-down';
    const   navOpenedSign = 'fal fa-angle-up';
    const navInitalized = 'js-nav-built';
    setTimeout(function() {
        (<any>$)('#js-nav-menu').navigation({
            accordion : true,
            speed : navSpeed,
            closedSign : '<em class="' + navClosedSign + '"></em>',
            openedSign : '<em class="' + navOpenedSign + '"></em>',
            initClass: navInitalized
        });
    }, 400);
}

  public get navType(): typeof NavItemType {
    return NavItemType;
  }

  public gotoPage(navbarItem: NavbarItem) {
    if (navbarItem.link !== 'javascript:;') {
        if(navbarItem.id == 401) {
          window.open("https://server.optionslegion.com/downloads/PRIVACY_POLICY_Options_Legion.pdf");
        } else if(navbarItem.id == 402) {
          window.open("https://server.optionslegion.com/downloads/Terms_and_Conditions_Options_Legion.pdf");
        } else {
          this.router.navigateByUrl(navbarItem.link);
        }
    }
  }

  public handelEvent(event: GenericEvent) {
    switch (event.name) {
      case 'message-status-update':
        this.getUnreadMessageNumber();
        break;
    }
  }
}

export interface NavbarItem {
  id: number;
  title: string;
  link: string;
  icon: string;
  subMenu?: NavbarItem[];
  type?: NavItemType;
}

export interface MessageCount{
  count: number;
}
