export interface Ticket {
  id: number;
  parent_id: number;
  client_id: number;
  subject: string;
  priority: string;
  email: string;
  note: string;
  status: string;
  replied: string;
  created_at: string;
  updated_at: string;
}
