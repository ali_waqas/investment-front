import { GenericEvent } from './../../shared/models/generic-event.model';
import {
  Component,
  EventEmitter,
  OnInit,
  Output
} from '@angular/core';
import {
  AbstractBaseComponent
} from '@app/core/class/abstract.base.omponent';
import {
  ProfileService
} from '../profile/profile.service';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  Ticket
} from './ticket.model';
import {
  TicketsService
} from './tickets.service';
import {
  ApplicationEvent
} from '@app/shared/services/alert-modal.service';
import {
  Message,
  MessageAsReadOrUnread,
  MessageData,
  Messages,
  MessagesFilter,
  recievedMessages,
  sentMessages
} from './message.model';
import {
  ClientResponse,
} from '../profile/profile.model';
import { Alert } from '@app/shared/models/alert.model';
import { MessageCount } from '../sidebar/sidebar.component';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.css']
})
export class TicketsComponent extends AbstractBaseComponent implements OnInit {
  public is_Admin: boolean;
  public ticketForm: FormGroup;
  public newMessageForm: FormGroup;
  public replyMessageForm: FormGroup;
  public tickets: Ticket[];
  public isLoading = false;
  public hideReply = false;
  public ticket: Ticket;
  public messages: Messages;
  public message: Message | recievedMessages | sentMessages;
  public clientResponse: ClientResponse;
  public allMessages: Message[] = [];
  public sentMessages: sentMessages[] = [];
  public recievedMessages: recievedMessages[] = [];
  public messageCount: MessageCount;
  public messagesFilterForm: FormGroup;
  public messagesFilter: MessagesFilter = {};
  public showBtn: boolean;
  @Output() public emitData = new EventEmitter();
  constructor(
    protected profileService: ProfileService,
    private ticketsService: TicketsService,
    private applicationEvent: ApplicationEvent,
    private formBuilder: FormBuilder
  ) {
    super(profileService);
  }

  async ngOnInit() {
    this.subscribe(this.applicationEvent.onGenericEvent.subscribe(this.handleEvent.bind(this)));
    this.pageTitle = 'Messages';
    this.pageIcon = 'fal fa-envelope-open';
    this.createNewMessageForm();
    this.createReplyMessageForm();
    await this.listMessages();
    this.checkisAdmin();
    this.firstTimeGetMessageCount();
    this.initForm();
    this.ready = true;
  }

  public checkisAdmin() {
    if (this.messages.loggedInUser.type == 'Administrator') {
      this.is_Admin = true;
    } else {
      this.is_Admin = false;
    }
  }

  public async createNewMessageAsync() {
    this.isLoading = true;
    const message: Message = this.newMessageForm.value;
    try {
      await this.ticketsService.createNewMessageAsync(message);
      this.applicationEvent.fireAlert({
        title: 'Message',
        sub_title: 'Message Sent',
        body: 'Your message has been sent successfully',
        ok_text: 'OK',
        close_text: 'CLOSE'
      });
      this.listMessages();
      this.newMessageForm.reset();
    } catch (error) {
      this.httpError = error;
      this.hasErrors = true;
    }
    this.isLoading = false;
  }


  public async replyMessage() {
    this.isLoading = true;
    const message: Message = this.replyMessageForm.value;
    try {
      this.hasErrors = false;
      this.isLoading = true;
      await this.ticketsService.createReplyMessageAsync(message);
      ( < any > $('#view-reply-message-modal')).modal('hide');
      this.applicationEvent.fireAlert({
        title: 'Message',
        sub_title: 'Message Reply Success',
        body: 'Reply has been sent successfully',
        ok_text: 'OK',
        close_text: 'CLOSE'
      });
      this.listMessages();
      this.replyMessageForm.reset();
    } catch (error) {
      this.isLoading = false;
      this.httpError = error;
      this.hasErrors = true;
    }
  }

  public createReplyMessageForm() {
    this.replyMessageForm = this.formBuilder.group({
      id: ['', [Validators.required]],
      subject: ['', [Validators.required]],
      body: ['', [Validators.required]],
    });
  }

  private async listMessages() {
    try {
      const messagesResponse = await this.ticketsService.getMessagesAsync(this.messagesFilter);
      this.messages = messagesResponse.messages;
    } catch (error) {
      this.httpError = error;
      this.hasErrors = true;
    }
  }

  public createNewMessageForm() {
    this.newMessageForm = this.formBuilder.group({
      subject: ['', [Validators.required]],
      body: ['', [Validators.required]],
    });
  }

  public openMessage(message: Message | recievedMessages | sentMessages, value = true) {
    this.hideReplyButtonOnSentMessage(message);
    this.message = message;
    ( < any > $('#view-message-modal')).modal();
    if(value){
      this.checked(null, message.id);
    }
  }

  public openReplyMessage(receivedmessage: recievedMessages) {
    this.message = receivedmessage;
    this.replyMessageForm.controls['id'].setValue(receivedmessage.id);
    this.replyMessageForm.controls['subject'].setValue("Reply: "+receivedmessage.subject);
    ( < any > $('#view-message-modal')).modal('hide');
    ( < any > $('#view-reply-message-modal')).modal();
  }

  private hideReplyButtonOnSentMessage(message: sentMessages) {
    if (message.hasOwnProperty('receiver')) {
      this.hideReply = false;
    } else {
      this.hideReply = true;
    }
  }

  public async checked(event: any, id_: number) {
    try  {
      if(event) {
        if (event.target.checked) {
          await this.ticketsService.postMarkAsRead(id_);
          this.listMessages();
        } else {
          await this.ticketsService.postMarkAsUnread(id_);
          this.listMessages();
        }
      } else {
          await this.ticketsService.postMarkAsRead(id_);
          this.listMessages();
      }
        this.applicationEvent.fireGenericEvent({name: 'message-status-update'});

    } catch (error) {
      const alert: Alert = {
        title: 'Error',
        sub_title: '',
        body: error,
        ok_text: 'OK',
        close_text: 'CLOSE'
      };
      this.applicationEvent.fireAlert(alert);
    }
  }

  private async firstTimeGetMessageCount() {
    try {
      const messageUnreadCountResponse: MessageCount = await this.profileService.postMarkAsRead();
      this.messageCount = messageUnreadCountResponse;
      this.ready = true;
    } catch (error) {
      const alert: Alert = {
        title: 'Error',
        sub_title: '',
        body: error,
        ok_text: 'OK',
        close_text: 'CLOSE'
      };
      this.applicationEvent.fireAlert(alert);
    }
  }

  public setMessageCount(value_: MessageCount) {
    this.messageCount = value_;
    this.ready =true
  }

  public handleEvent(event: GenericEvent) {
    switch (event.name) {
      case 'unread-message-count-ready':
        this.setMessageCount(event.payload);
      break;
    }
  }

  public showButton() {
    this.showBtn = true;
  }

  public hideButton() {
    this.showBtn = false;
  }

  public async markAllAsRead() {
    try  {
        await this.ticketsService.postMarkAllAsRead();
        this.listMessages();
        this.applicationEvent.fireGenericEvent({name: 'message-status-update'});

    } catch (error) {
      const alert: Alert = {
        title: 'Error',
        sub_title: '',
        body: error,
        ok_text: 'OK',
        close_text: 'CLOSE'
      };
      this.applicationEvent.fireAlert(alert);
    }
  }

  private initForm() {
    this.messagesFilterForm = this.formBuilder.group({
      client_id: ['', []],
      client_name: ['', []],
      message_status: ['', []],
    });
  }

  public applyFilers() {
    this.messagesFilter = this.messagesFilterForm.value;
    console.log("Yah hun main",this.messagesFilter);
    this.listMessages();
  }

  public clearFilters() {
    this.initFilters();
    this.listMessages();
  }

  public initFilters() {
    this.messagesFilter = { page: 1, per_page: 100 };
  }
}
