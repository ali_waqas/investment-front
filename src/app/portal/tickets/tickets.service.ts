import { Injectable } from '@angular/core';
import { BaseAPIClass } from '@app/core/class/baseAPI.class';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { Ticket } from './ticket.model';
import { Message, MessageAsReadOrUnread, MessageData, MessagesFilter } from './message.model';

@Injectable({ providedIn: 'root' })
export class TicketsService extends BaseAPIClass {
  private readonly URL_CREATE_TICKET: string = 'ticket';
  private readonly URL_LIST_TICKETS: string = 'tickets';
  private readonly URL_LIST_MESSAGES: string = 'all_messages';
  private readonly URL_SEND_NEW_MESSAGE: string = 'send_message';
  private readonly URL_SEND_REPLY_MESSAGE: string = 'reply_message';
  private readonly URL_READ_MESSAGE: string = 'mark_message_as_read/';
  private readonly URL_UNREAD_MESSAGE: string = 'mark_message_as_unread/';
  private readonly URL_MARK_ALL_AS_READ_MESSAGE: string = 'mark_all_messages_as_read';

  constructor(
    protected httpClient: HttpClient,
    protected authenticationService: AuthenticationService
  ) {
    super(httpClient, authenticationService);
  }

  public async createTicketAsync(ticket: Ticket) {
    const ticket_ = await this.postAsync<Ticket>(
      this.URL_CREATE_TICKET,
      ticket
    ).toPromise();
    return ticket_;
  }

  public async createNewMessageAsync(message: Message) {
    return await this.postAsync<Message>(
      this.URL_SEND_NEW_MESSAGE,
      message
    ).toPromise();
  }

  public async createReplyMessageAsync(message: Message) {
    return await this.postAsync<Message>(
      this.URL_SEND_REPLY_MESSAGE,
      message
    ).toPromise();
  }

  public async getTicketsAsync() {
    const tickets = await this.postAsync<Ticket[]>(
      this.URL_LIST_TICKETS
    ).toPromise();
    return tickets;
  }

  public async getMessagesAsync(filters: MessagesFilter) {
    const messages = await this.postAsync<MessageData>(
      this.URL_LIST_MESSAGES,
      filters,
      {},
      false
    ).toPromise();
    return messages;
  }

  public async postMarkAsRead(messageId_: number) {
    return await this.postAsync<MessageAsReadOrUnread>(this.URL_READ_MESSAGE + messageId_, {}, {}, false).toPromise();
  }

  public async postMarkAsUnread(messageId_: number) {
    return await this.postAsync<MessageAsReadOrUnread>(this.URL_UNREAD_MESSAGE + messageId_, {}, {}, false).toPromise();
  }

  public async postMarkAllAsRead() {
    return await this.postAsync<MessageAsReadOrUnread>(this.URL_MARK_ALL_AS_READ_MESSAGE, {}, {}, false).toPromise();
  }
}
