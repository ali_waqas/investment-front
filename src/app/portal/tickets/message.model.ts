import { Profile } from '../profile/profile.model';

export interface MessageData {
  messages: Messages;
}

export interface Messages {
  allMessages: Message[];
  sentMessages: sentMessages[];
  recievedMessages: recievedMessages[];
  loggedInUser: Profile;
}

export interface Message {
  id: number;
  sender_id?: number;
  receiver_id?: number;
  read_at?: string;
  reply_to?: boolean;
  deleted_from_sender?: boolean;
  deleted_from_receiver?: boolean;
  subject?: string;
  body?: string;
  created_at?: string;
  updated_at?: string;
  receiver?: Profile;
  message_status?: String;
}

export interface sentMessages {
  id?: number;
  sender_id?: number;
  receiver_id?: number;
  read_at?: string;
  reply_to?: boolean;
  deleted_from_sender?: boolean;
  deleted_from_receiver?: boolean;
  subject?: string;
  body?: string;
  created_at?: string;
  updated_at?: string;
  receiver?: Profile;
}

export interface recievedMessages {
  id?: number;
  sender_id?: number;
  receiver_id?: number;
  read_at?: string;
  reply_to?: boolean;
  deleted_from_sender?: boolean;
  deleted_from_receiver?: boolean;
  subject?: string;
  body?: string;
  created_at?: string;
  updated_at?: string;
  sender?: Profile;
}

export interface repliedMessages {
  id?: number;
  sender_id?: number;
  receiver_id?: number;
  read_at?: string;
  reply_to?: boolean;
  deleted_from_sender?: boolean;
  deleted_from_receiver?: boolean;
  subject?: string;
  body?: string;
  created_at?: string;
  updated_at?: string;
  sender?: Profile;
}

export interface MessageAsReadOrUnread {
  message: string;
}

export interface MessagesFilter {
  client_id?: number;
  client_name?: string;
  message_status?: string;
  page?: number;
  per_page?: number;
}
