import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { Analysis, DashboardItem, DashboardResponse } from '@app/dashboard/dashboard/dashboard.model';
import { DashboardService } from '@app/dashboard/dashboard/dashboard.service';
import { Client, ClientEntity } from '../profile/profile.model';
import { ProfileService } from '../profile/profile.service';

@Component({
  selector: 'app-client-dashboard',
  templateUrl: './client-dashboard.component.html',
  styleUrls: ['./client-dashboard.component.css']
})
export class ClientDashboardComponent extends AbstractBaseComponent implements OnInit {
  public clients: ClientEntity[] = [];
  public client: Client;
  public profileForm: FormGroup;
  public profileReady = false;
  public depositFormReady = false;
  public depositForm: FormGroup;
  public activeProfile: ClientEntity;
  public analysis: Analysis;
  public analysisReady = false;
  public dashboardItems: DashboardItem[] = [];
  constructor(
    protected profileService: ProfileService,
    private dashboardService: DashboardService,
    ) {
    super(profileService);
    this.dataTableName = 'dt-clients';
    this.pageTitle = 'Manage Clients';
    this.pageIcon = 'fal fa-users';
}

  async ngOnInit() {

      await this.fetchClientDashboardData();
      this.prepareDashboardItems();
  }


  protected async fetchClientDashboardData() {
    this.client = await this.profileService.getProfileAsync();
  }


  private async prepareDashboardItems() {
    const dashboardResponse: DashboardResponse = await this.dashboardService.getAnalysisData();
    this.analysis = dashboardResponse.analysis;
    this.analysisReady = true;

    const totalClients = Number(this.analysis.total_clients).toString();
    const totalDeposits = Number(this.analysis.total_deposits).toString();
    const totalWithdrawals = Number(this.analysis.total_withdrawals).toString();
    this.dashboardItems = [
      {
        name: 'Total Clients',
        value: totalClients,
        background_class: 'bg-success-400',
        icon: 'fa-users',
      },
      {
        name: 'Total Deposits',
        value: totalDeposits,
        background_class: 'bg-danger-500',
        icon: 'fal fa-credit-card',
      },
      {
        name: 'Total Withdrawals',
        value: totalWithdrawals,
        background_class: 'bg-primary-300',
        icon: 'fal fa-credit-card',
      }
    ];

    console.log(this.dashboardItems);

  }

}
