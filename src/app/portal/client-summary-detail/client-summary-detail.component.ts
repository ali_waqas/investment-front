import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AbstractWidgetComponent } from '@app/core/class/abstract.widget.omponent';
import { ClientsService } from '../clients/clients.service';
import { Client, ClientEntity, ClientKYCDocuments,Document, ClientsFilter, ClientSummary,ClientSummaryResponse, ShortProfile, TypeOfAuthentication, SendEmail } from '../profile/profile.model';
import { Referral } from '../referrals/referral.model';
import { Withdrawal } from '../withdrawals/withdrawal.model';
import { ProfileService } from '../profile/profile.service';
import { Alert } from '@app/shared/models/alert.model';
import { ApplicationEvent } from '@app/shared/services/alert-modal.service';
import { GenericEvent } from '@app/shared/models/generic-event.model';
import { ManualDeposit } from '../deposits/deposit.model';
import { DynamicScriptLoaderService } from '@app/shared/services/dynamic-script-loader.service';
declare var $: any;

@Component({
  selector: 'app-client-summary-detail',
  templateUrl: './client-summary-detail.component.html',
  styleUrls: ['./client-summary-detail.component.css']
})
export class ClientSummaryDetailComponent extends AbstractWidgetComponent implements OnInit {
  @Output() public emmitData = new EventEmitter();

  public clientSummary: ClientSummaryResponse;
  private clientId: number;
  public clientForm: FormGroup;
  public client: Client;
  public profileReady = false;
  public depositFormReady = false;
  public sendEmailFormReady = false;
  public alreadyDisabled = false;
  public depositForm: FormGroup;
  public sendEmailForm: FormGroup;
  public KYCDocuments: Document[] = [];
  public alreadyGoogleDisabled = false;
  public activeProfile: ClientEntity;
  private clientsFilter: ClientsFilter  = {};


  constructor(
    private route: ActivatedRoute,
    private clientsService: ClientsService,
    private applicationEvent: ApplicationEvent,
    private dynamicScriptLoaderService: DynamicScriptLoaderService,
    private formBuilder: FormBuilder,
    ) {
  super();
  }

  async ngOnInit() {
        this.route.params.subscribe((params: Params) => {
            this.clientId = params.id;
            this.fetchDataAsync();
        });
        await this.getProfile();
        this.ready = true;
    }

    ngAfterViewInit() {
      const scriptsToLoad: string[] = [
        'summernote'
      ];
      this.dynamicScriptLoaderService.load(scriptsToLoad).then((scripts) => {
        this.applicationEvent.fireGenericEvent({ name: 'summernote-loaded' });
      });
    }

    private getSummerNoteText() {
    return (<any>$)('.summernote').summernote('code');
  }


    private fetchSummary() {
        return this.clientsService.fetchClientSummary(this.clientId);
    }

    private async fetchDataAsync() {
        this.loadingStart();
        this.clientSummary = await this.fetchSummary();
        this.setPages(this.clientSummary.summary.referrals.last_page);
        this.loadingFinished();
    }

    public typeClass(referral: Referral) {
            switch (referral.status) {
              case 'Pending':
                  return 'badge badge-warning';
              case 'Terminated':
              case 'Expired':
              case 'Cancelled':
                  return 'badge badge-danger';
              case 'Joined':
              case 'COMPLETED':
                  return 'badge badge-success';
          }
    }

    public typeWithdrawalClass(withdrawal: Withdrawal) {
      switch (withdrawal.status) {
          case 'PENDING':
              return 'badge badge-warning';
          case 'FAILED':
          case 'CANCELLED':
              return 'badge badge-danger';
          case 'COMPLETED':
              return 'badge badge-success';
      }
    }


    public async gotoPage(page: number) {
      this.clientsFilter.page = page;
      this.updatePagerLinks(page);
      this.fetchDataAsync();
    }

    private async getProfile() {
      this.clientSummary = await this.fetchSummary();
      this.createProfileForm();
    }

    private createProfileForm() {
      this.clientForm = this.formBuilder.group({
        id: [this.clientSummary.summary.profile.id, [Validators.required]],
        first_name: [this.clientSummary.summary.profile.first_name, [Validators.required]],
        last_name: [this.clientSummary.summary.profile.last_name, [Validators.required]],
        email: [this.clientSummary.summary.profile.email, [Validators.required]],
        mobile: [this.clientSummary.summary.profile.mobile, [Validators.required]],
        phone: [this.clientSummary.summary.profile.phone],
        city: [this.clientSummary.summary.profile.city],
        address: [this.clientSummary.summary.profile.address],
        country: [this.clientSummary.summary.profile.country]
      });
    }


    public async markAsFeePaid(client: ClientEntity) {
      try {
          client.is_signup_fee_paid = 'Yes';
          const response: any = await this.clientsService.saveClientProfileAsync(client);
          const alert: Alert = {
              title: 'Account Activated',
              sub_title: 'Profile changes saved',
              body: 'Client profile has been updated successfuly.',
              ok_text: 'OK',
              close_text: 'CLOSE'};
          this.applicationEvent.fireAlert(alert);
      } catch (error) {
          console.log(error);
      }
  }

  public async resetPassword(client: ClientEntity) {
    try {
        client.password = this.makeid(6);
        const response: any = await this.clientsService.saveClientProfileAsync(client);
        const alert: Alert = {
            title: 'Password Reset Successfuly',
            sub_title: 'Profile changes saved',
            body: 'Client password has been changed, new Password is: <b>' + client.password + '</b>',
            ok_text: 'OK',
            close_text: 'CLOSE'
        };
        this.applicationEvent.fireAlert(alert);
    } catch (error) {
        console.log(error);
    }
  }


  public openSendEmailForm(client: ClientEntity) {
    this.activeProfile = client;
    (<any>$)('.dropdown-menu').hide();
    this.createSendEmailForm(client);
    (<any>$)('#client-send-email-modal').modal();
    const t = this;
    setTimeout(function(){
        t.initSummerNote();
    }, 1000);
    this.sendEmailFormReady = true;
  }

  private makeid(length: number) {
    let result           = '';
    const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  }


  public openDisable2FA(client: ClientEntity) {
    this.activeProfile = client;
    (<any>$)('#disable-2fa-modal').modal();
    this.profileReady = true;
    if (this.client.profile.type_of_authentication.toString() == TypeOfAuthentication.google_authenticator.toString()) {

    }
  }


  public async disableEmail2FA(client: ClientEntity) {
    try {
        const disableResponse: any = await this.clientsService.disableEmail2FAAsync(client.id);
        console.log(disableResponse.message);
        if (disableResponse.message == 'Email 2fa is already disabled') {
              this.alreadyDisabled = true;
          this.alreadyGoogleDisabled = false;

        }
        else{
             const alert: Alert = {
          title: 'Two Factor Authentication Disabled',
          sub_title: 'Email 2FA',
          body: 'Email Two Factor Authentication has been Disabled',
          ok_text: 'OK',
          close_text: 'CLOSE'
          };
        this.applicationEvent.fireAlert(alert);
          (<any>$)("#disable-2fa-modal").modal('hide');
        }

    } catch (error) {
        console.log(error);
    }
  }

  public openDepositForm(client: ClientEntity) {
    this.activeProfile = client;
    this.createDepositForm(client);
    (<any>$)('#client-deposit-modal').modal();
    this.depositFormReady = true;
}


public createDepositForm(client: ClientEntity) {
  this.depositForm = this.formBuilder.group({
      client_id: [client.id, [Validators.required]],
      first_name: [client.first_name, [Validators.required]],
      last_name: [client.last_name, [Validators.required]],
      email: [client.email, [Validators.required]],
      amount: ['', [Validators.required]],
      method_id: ['', [Validators.required]],
      details: ['', [Validators.required]],
      type: ['INVESTMENT', [Validators.required]]
  });
}


  public createSendEmailForm(client: ClientEntity) {
    this.sendEmailForm = this.formBuilder.group({
        to: [client.email, [Validators.required]],
        subject: ['', [Validators.required]],
        cc: ['', []],
        bcc: ['', []],
        email_body: ['', []],
    });
  }


public async saveDeposit() {
  try {

      const manualDeposit: ManualDeposit = this.depositForm.value;
      const response: any = await this.clientsService.createManualDeposit(manualDeposit);
      const alert: Alert = {
          title: 'Deposit Saved',
          sub_title: 'New deposit has been created',
          body: 'Client account has been deposited with $' + manualDeposit.amount,
          ok_text: 'OK',
          close_text: 'CLOSE'
      };
      this.applicationEvent.fireAlert(alert);
  } catch (error) {
      console.log(error);
  }
}


  public async sendEmail() {
    try {

      const email_body = this.getSummerNoteText();
      this.sendEmailForm.controls['email_body'].setValue(email_body);
        const sendEmail: SendEmail = this.sendEmailForm.value;
        const response: any = await this.clientsService.sendEmail(sendEmail);
        const alert: Alert = {
            title: 'Email Sent',
            sub_title: 'Email has been sent successfully',
            body: 'Email has been sent successfully',
            ok_text: 'OK',
            close_text: 'CLOSE'
        };
        this.applicationEvent.fireAlert(alert);
    } catch (error) {
        console.log(error);
    }
  }


public async viewKYCDocuments(client: ClientEntity) {
  try {
      this.activeProfile = client;
      const clientKYC: ClientKYCDocuments = await this.clientsService.fetchClientKYCDocumentsAsync(client.id);
      this.KYCDocuments = clientKYC.documents;
      (<any>$)('#client-kyc-modal').modal();
  } catch (error) {
      console.log(error);
  }
  }


  private initSummerNote() {
    (<any>$)('.summernote').summernote(
      {
        height: 200,
        tabsize: 2,
        placeholder: 'Type here...',
        dialogsFade: true,
        toolbar: [
          ['style', ['style']],
          ['font', ['strikethrough', 'superscript', 'subscript']],
          ['font', ['bold', 'italic', 'underline', 'clear']],
          ['fontsize', ['fontsize']],
          ['fontname', ['fontname']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']],
          ['height', ['height']],
          ['table', ['table']],
          ['insert', ['link', 'picture', 'video']],
          ['view', ['fullscreen', 'codeview', 'help']]
        ]
      });
  }

  public async changeKYCStatus(client: ClientEntity, status: string) {
    try {
        const kycApprovalResponse: any = await this.clientsService.approveKYCAsync(client.id, status);
        const alert: Alert = {
            title: 'KYC Updated',
            sub_title: 'Request Completed',
            body: kycApprovalResponse.message,
            ok_text: 'OK',
            close_text: 'CLOSE'};
        this.applicationEvent.fireAlert(alert);
    } catch (error) {
        console.log(error);
    }
  }

  public async disableGoogle2FA(client: ClientEntity) {
    try {
        const disableResponse: any = await this.clientsService.disableGoogle2FAAsync(client.id);
        if (disableResponse.message == 'Google 2fa is already disabled') {
          this.alreadyGoogleDisabled = true;
          this.alreadyDisabled = false;
        } else {
          const alert: Alert = {
            title: 'Two Factor Authentication Disabled',
            sub_title: 'Google 2FA',
            body: 'Google Two Factor Authentication has been Disabled',
            ok_text: 'OK',
            close_text: 'CLOSE'
          };
          this.applicationEvent.fireAlert(alert);
          (<any>$)("#disable-2fa-modal").modal('hide');
        }

    } catch (error) {
        console.log(error);
    }
   }



    public async updateProfile() {
        const shortProfile: ShortProfile = this.clientForm.value;
        try {
          const updatedProfile = await this.clientsService.updateProfileAsync(
            shortProfile
          );
          const alert: Alert = {
            title: 'Profile changes saved',
            sub_title: 'Request completed',
            body: 'Your profile has been updated successfuly.',
            ok_text: 'OK',
            close_text: 'CLOSE'
          };
          this.applicationEvent.fireAlert(alert);
        } catch (error) {
          this.hasErrors = true;
        }
    }
}
