import { Component, OnInit } from '@angular/core';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { ProfileService } from '../profile/profile.service';

@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.css']
})
export class SupportComponent extends AbstractBaseComponent implements OnInit {
    constructor(
      protected profileService: ProfileService) {
      super(profileService);
    }
    async ngOnInit() {
      this.pageTitle = 'Support';
      this.pageIcon = 'ni ni-support';
  }

}
