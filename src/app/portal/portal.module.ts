import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from '@app/dashboard';
import { DepositsComponent } from './deposits/deposits.component';
import { PortalComponent } from './portal/portal.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SharedModule } from '@app/shared';
import { ProfileComponent } from './profile/profile.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { WithdrawalsComponent } from './withdrawals/withdrawals.component';
import { NetworkComponent } from './network/network.component';
import { InvestmentsComponent } from './investments/investments.component';
import { PerfectMoneyComponent } from './perfect-money/perfect-money.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { WithdrawComponent } from './withdraw/withdraw.component';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { WebinarsComponent } from './webinars/webinars.component';
import { SupportComponent } from './support/support.component';
import { AwardsComponent } from './awards/awards.component';
import { ClientsComponent } from './clients/clients.component';
import { HomepageComponent } from './homepage/homepage.component';
import { AwardComponent } from './award/award.component';
import { PendingRegistrationsComponent } from './pending-registrations/pending-registrations.component';
import { ClientSummaryDetailComponent } from './client-summary-detail/client-summary-detail.component';
import { ClientDashboardComponent } from './client-dashboard/client-dashboard.component';
import { TicketsModule } from './tickets/tickets.module';
import { FaqsComponent } from './faqs/faqs.component';
import { PackagesComponent } from './packages/packages.component';
import { KYCComponent } from './kyc/kyc.component';
import { CommissionsComponent } from './commissions/commission.component';
import { AcademySignUpComponent } from './academy-signup/academy-signup.component';

@NgModule({
  declarations: [PortalComponent, AwardComponent],
  imports: [
    SharedModule,
    CommonModule,
    RouterModule.forChild([
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'homepage',
        component: HomepageComponent
      },
      {
        path: 'profile',
        component: ProfileComponent
      },
      {
        path: 'add_funds',
        component: DepositsComponent
      },
      {
        path: 'withdrawals',
        component: WithdrawalsComponent
      },
      {
        path: 'withdraw',
        component: WithdrawComponent
      },
      {
        path: 'transactions',
        component: TransactionsComponent
      },
      {
        path: 'tickets',
        loadChildren: () => TicketsModule,
      },
      {
        path: 'investments',
        component: InvestmentsComponent
      },
      {
        path: 'perfect_money_successful',
        component: PerfectMoneyComponent
      },
      {
        path: 'perfect_money_failed',
        component: PerfectMoneyComponent
      },
      {
        path: 'invoice',
        component: InvoiceComponent
      },
      {
        path: 'faqs',
        component: FaqsComponent
      },
      {
        path: 'network',
        component: NetworkComponent
      },
      {
        path: 'webinars',
        component: WebinarsComponent
      },
      {
        path: 'support',
        component: SupportComponent
      },
      {
        path: 'awards',
        component: AwardsComponent
      },
      {
        path: 'award',
        component: AwardComponent
      },
      {
        path: 'clients',
        component: ClientsComponent
      },
      {
        path: 'client-dashboard',
        component: ClientDashboardComponent
      },
      {
        path: 'client-dashboard/summary/:id',
        component: ClientSummaryDetailComponent
      },
      {
        path: 'pending-registrations',
        component: PendingRegistrationsComponent
      },
      {
        path: 'packages',
        component: PackagesComponent
      },
      {
        path: 'kyc',
        component: KYCComponent
      },
      {
        path: 'commissions',
        component: CommissionsComponent
      },
      {
        path: 'academy-signup',
        component: AcademySignUpComponent
      },
    ])
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [],
  providers: [CurrencyPipe]
})
export class PortalModule {}
