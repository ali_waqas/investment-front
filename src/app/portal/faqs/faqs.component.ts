import { Component, OnInit } from '@angular/core';
import { faqs } from '@app/frontend/faqs/faq.model';
import { FAQ } from './faq.model';
@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.component.html',
  styleUrls: ['./faqs.component.css']
})
export class FaqsComponent implements OnInit {
  public pageTitle = 'FAQ';
  public pageIcon = 'ni ni-question';
  public faqs: FAQ[] = [];
  constructor() { }
  ngOnInit() {
    this.faqs = faqs;
  }
}
