import { Component, OnInit, Input } from '@angular/core';
import { Profile } from '../profile/profile.model';
import { ProfileService } from '../profile/profile.service';

@Component({
  selector: 'app-sub-header',
  templateUrl: './sub-header.component.html',
  styleUrls: ['./sub-header.component.scss']
})
export class SubHeaderComponent implements OnInit {
    constructor(private profileService: ProfileService) {}
    public profile: Profile;
    @Input() title?: string;
    @Input() icon?: string;
    @Input() showIncomes = false;
    public leftIncome: number = 0;
    public rightIncome: number = 0;

    async ngOnInit() {
    const response = await this.profileService.getProfileAsync();
    this.profile = response.profile;
    if(this.profile.incomes != null) {
        this.leftIncome =
            this.profile.incomes.left_balance == null
            ? 0
            : this.profile.incomes.left_balance + Number(this.profile.incomes.left_balanced_income);
        this.rightIncome =
            this.profile.incomes.right_balance == null
            ? 0
            : this.profile.incomes.right_balance + Number(this.profile.incomes.right_balanced_income);
    }
    }
}
