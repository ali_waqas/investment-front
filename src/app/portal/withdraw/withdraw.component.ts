import { Component, OnInit } from '@angular/core';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Withdrawal, WithdrawalWireTransfer, WithdrawalPerfectMoney, WithdrawalCrypto, WithdrawalDAI } from '../withdrawals/withdrawal.model';
import { ProfileService } from '../profile/profile.service';
import { WithdrawalsService } from '../withdrawals/withdrawals.service';
declare const $: any;
@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.scss']
})
export class WithdrawComponent extends AbstractBaseComponent implements OnInit {
    private readonly WALLET_MAX_AMOUNT = 2000;
    public withdrawalForm: FormGroup;
    public withdrawalWithWireTransferForm: FormGroup;
    public withdrawalWithPerfectMoney: FormGroup;
    public withdrawalWithPerfectMoneyEmailCodeForm: FormGroup;
    public withdrawalWithCrypto: FormGroup;
    public withdrawalWithDAI: FormGroup;
    public wireTransferForm: FormGroup;
    public emailCode = false;
    public showVerify = false;
    public showVerifyMessage = false;
    public emailCodeCrypto = false;
    public emailCodeDAI = false;
    public showVerifyCrypto = false;
    public showVerifyDAI = false;
    public emailCodeWireTransfer = false;
    public showVerifyWireTransfer = false;
    public wallets: {type: string, description: string}[] = [];
    public submitted:boolean;
    public reinvestAmount: number = 0;
    public reinvestmentWallet: string = null;
    constructor(
      protected profileService: ProfileService,
      protected withdrawalsService: WithdrawalsService,
      private formBuilder: FormBuilder) {
      super(profileService);
    }

    async ngOnInit() {
      this.pageTitle = 'Withdraw';
      this.pageIcon = 'fal fa-credit-card-front';
      this.createWithdrawalForm();
      this.createWithdrawalWithWireTransferForm();
      this.createPerfectMoneyWithdrawalForm();
      this.createCryptoWithdrawalForm();
      this.createDAIWithdrawalForm();
      await this.fetchClientDashboardData();
      this.wallets = [];
      this.wallets.push({type: 'Binary', description: `Binary Wallet ($${this.client.profile.binary_wallet})`});
      this.wallets.push({type: 'ROI', description: `ROI Wallet ($${this.client.profile.roi_wallet})`});
      this.ready = true;
    }

    protected async fetchClientDashboardData() {
      this.client = await this.profileService.getProfileAsync();
      this.ready = true;
    }


    public async withdrawNowAsync() {
      try {
        const withdrawalsRequest: Withdrawal = this.withdrawalForm.value;
        const withdrawal = await this.withdrawalsService.createWithdrawalAsync(withdrawalsRequest);
        this.client = await this.profileService.getProfileAsync(true);
        alert('Withdrawal has been saved successfully.');
      } catch (error) {
        this.httpError = error;
        this.hasErrors = true;
      }
    }

    public async saveWitdrawalWithWireTransferAsync() {
      try {
        this.submitted = true;
        $("#validateWireTransfer").attr('disabled', 'disabled');
        const withdrawalsRequest: WithdrawalWireTransfer = this.withdrawalWithWireTransferForm.value;
        const withdrawal = await this.withdrawalsService.createWitdrawalWithWireTransferAsync(withdrawalsRequest);
        $("#validateWireTransfer").removeAttr('disabled');
        this.submitted = false;
        this.emailCodeWireTransfer = false;
        this.showVerifyMessage = false;
        alert('Withdrawal has been saved successfully.');
      } catch (error) {
        $("#validateWireTransfer").removeAttr('disabled');
        this.submitted = false;
        this.httpError = error;
        console.log('ERROR IS:');
        console.log(this.httpError.error.error);
        if (this.httpError.error.error == 'Please verify the OTP Code') {
          this.emailCodeWireTransfer = true;
          this.showVerifyMessage = true;
          this.showVerifyWireTransfer = false;
        }
        else {
            if (this.httpError.error.error == 'Invalid Email OTP, please try again'
                || this.httpError.error.error == 'Invalid Google OTP, please try again') {
              this.emailCodeWireTransfer = true;
              this.showVerifyMessage = false;
              this.showVerifyWireTransfer = true;
            }
            else {
              this.hasErrors = true;
            }
          }

      }
    }

    public async withdrawWithPerfectMoneyAsync() {
      try {
          this.submitted = true;
          $("#validatePerfectMoney").attr('disabled', 'disabled');
          const perfectMoneyWithdrawalRequest: WithdrawalPerfectMoney = this.withdrawalWithPerfectMoney.value;
          const withdrawal = await this.withdrawalsService.createWitdrawalWithPerfectMoneyAsync(perfectMoneyWithdrawalRequest);
          $("#validatePerfectMoney").removeAttr('disabled');
          this.submitted = false;
          this.emailCode = false;
          this.showVerifyMessage = false;
          alert('Withdrawal has been saved successfully.');
        } catch (error) {
          $("#validatePerfectMoney").removeAttr('disabled');
          this.submitted = false;
          this.httpError = error;
          console.log('ERROR IS:');
          console.log(this.httpError.error.error);
          if (this.httpError.error.error == 'Please verify the OTP Code') {
            this.emailCode = true;
            this.showVerifyMessage = true;
            this.showVerify = false;
          }
          else {
              if (this.httpError.error.error == 'Invalid Email OTP, please try again'
                  || this.httpError.error.error == 'Invalid Google OTP, please try again') {
                this.emailCode = true;
                this.showVerifyMessage = false;
                this.showVerify = true;
              }
              else {
                this.hasErrors = true;
              }
            }

        }
    }

    public async withdrawWithCryptoAsync() {
      try {
          this.submitted = true;
          $("#validateCrypto").attr('disabled', 'disabled');
          const cryptoWithdrawalRequest: WithdrawalCrypto = this.withdrawalWithCrypto.value;
          const withdrawal = await this.withdrawalsService.createWitdrawalWithCryptoAsync(cryptoWithdrawalRequest);
          $("#validateCrypto").removeAttr('disabled');
          this.submitted = false;
          this.emailCodeCrypto = false;
          this.showVerifyMessage = false;
          alert('Withdrawal has been saved successfully.');
        } catch (error) {
          $("#validateCrypto").removeAttr('disabled');
          this.submitted = false;
          this.httpError = error;
          if (this.httpError.error.error == 'Please verify the OTP Code') {
            this.emailCodeCrypto = true;
            this.showVerifyMessage = true;
            this.showVerifyCrypto = false;
          }
          else {
              if (this.httpError.error.error == 'Invalid Email OTP, please try again'
                  || this.httpError.error.error == 'Invalid Google OTP, please try again') {
                this.emailCodeCrypto = true;
                this.showVerifyMessage = false;
                this.showVerifyCrypto = true;
              }
              else {
                this.hasErrors = true;
              }
            }

        }
    }

    public async withdrawWithDAIAsync() {
      try {
          this.submitted = true;
          $("#validateDAI").attr('disabled', 'disabled');
          const daiWithdrawalRequest: WithdrawalDAI = this.withdrawalWithDAI.value;
          const withdrawal = await this.withdrawalsService.createWitdrawalWithDAIAsync(daiWithdrawalRequest);
          $("#validateDAI").removeAttr('disabled');
          this.submitted = false;
          this.emailCodeDAI = false;
          this.showVerifyMessage = false;
          alert('Withdrawal has been saved successfully.');
        } catch (error) {
          $("#validateDAI").removeAttr('disabled');
          this.submitted = false;
          this.httpError = error;
          if (this.httpError.error.error == 'Please verify the OTP Code') {
            this.emailCodeDAI = true;
            this.showVerifyMessage = true;
            this.showVerifyDAI = false;
          }
          else {
              if (this.httpError.error.error == 'Invalid Email OTP, please try again'
                  || this.httpError.error.error == 'Invalid Google OTP, please try again') {
                this.emailCodeDAI = true;
                this.showVerifyMessage = false;
                this.showVerifyDAI = true;
              }
              else {
                this.hasErrors = true;
              }
            }

        }
    }

  public createWithdrawalForm() {
    this.withdrawalForm = this.formBuilder.group({
      amount: ['', [Validators.required]],
      name_on_card: ['', [Validators.required]],
      last_4_digits: ['', [Validators.required]],
    });
  }

  public createPerfectMoneyWithdrawalForm() {
    this.withdrawalWithPerfectMoney = this.formBuilder.group({
      wallet: ['', [Validators.required]],
      first_name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      perfect_money_account: ['', [Validators.required]],
      amount: ['', [Validators.required]],
      code: ['', null],
    });
  }

  public createCryptoWithdrawalForm() {
    this.withdrawalWithCrypto = this.formBuilder.group({
      wallet: ['', [Validators.required]],
      first_name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      wallet_address: ['', [Validators.required]],
      amount: ['', [Validators.required]],
      code: ['', null],
    });
  }

  public createDAIWithdrawalForm() {
    this.withdrawalWithDAI = this.formBuilder.group({
      wallet: ['', [Validators.required]],
      first_name: ['', [Validators.required]],
      last_name: ['', [Validators.required]],
      wallet_address: ['', [Validators.required]],
      amount: ['', [Validators.required]],
      code: ['', null],
    });
  }

  public createWithdrawalWithWireTransferForm() {
    this.withdrawalWithWireTransferForm = this.formBuilder.group({
      amount: ['', [Validators.required]],
      wallet: ['', [Validators.required]],
      bank_name: ['', [Validators.required]],
      bank_address: ['', [Validators.required]],
      bank_country: ['', [Validators.required]],
      iban: ['', [Validators.required]],
      swift: ['', [Validators.required]],
      code: ['', null],
    });
  }

  public checkBalance(amount: number, formName: string) {
    let walletName = "";
    if(formName === "PerfectMoney") {
      walletName = this.withdrawalWithPerfectMoney.controls['wallet'].value;
    } else if(formName == "Crypto") {
      walletName = this.withdrawalWithCrypto.controls['wallet'].value;
    } else if(formName == "DAI") {
      walletName = this.withdrawalWithDAI.controls['wallet'].value;
    }
    else if(formName == "WireTransfer") {
      walletName = this.withdrawalWithWireTransferForm.controls['wallet'].value;
    }

    if(walletName == "") {
      alert("Please select a wallet first");
    }

    if(walletName == 'Binary' && this.getBinaryWalletBalance() < amount) {
        alert("You do not have enough balance available in your wallet");
    } else if(walletName == "roi" && this.getRoiWalletBalance() < amount) {
      alert("You do not have enough balance available in your wallet");
    }
  }

  public getBinaryWalletBalance() {
    return Number(this.client.profile.binary_wallet) || 0;
  }

  public getRoiWalletBalance() {
    return Number(this.client.profile.roi_wallet) || 0;
  }

  public walletChanged(event: any, formName: string) {
    const maxAmount = (event.target.value == 'ROI')? this.getRoiWalletBalance(): this.getBinaryWalletBalance();
    if(formName === "PerfectMoney") {
      this.withdrawalWithPerfectMoney.controls['amount'].setValidators([Validators.max(maxAmount)]);
    } else if(formName == "Crypto") {
      this.withdrawalWithCrypto.controls['amount'].setValidators([Validators.max(maxAmount)]);
    } else if(formName == "DAI") {
      this.withdrawalWithDAI.controls['amount'].setValidators([Validators.max(maxAmount)]);
    } else if(formName == "WireTransfer") {
      this.withdrawalWithWireTransferForm.controls['amount'].setValidators([Validators.max(maxAmount)]);
    }
  }

  public get roiWalletProgressPercentage() {
    return (this.getRoiWalletBalance() / this.WALLET_MAX_AMOUNT) * 100;
  }

  public get binaryWalletProgressPercentage() {
    return (this.getBinaryWalletBalance() / this.WALLET_MAX_AMOUNT) * 100;
  }

  public reinvestFromWallet(wallet: string) {
    this.reinvestmentWallet = wallet;
    if(this.reinvestmentWallet == 'roi') {
      this.reinvestAmount = this.getRoiWalletBalance();
    } else if(this.reinvestmentWallet == 'binary'){
      this.reinvestAmount = this.getBinaryWalletBalance();
    }

    $("#reinvest-modal").modal('show');
  }

  public async confirmReinvestmentFromWallet() {
    try {
      this.loading = true;
      const payload = {wallet: this.reinvestmentWallet, amount: this.reinvestAmount};
      const response = await this.withdrawalsService.reinvestFromWalletAsync(payload);
      this.client = await this.profileService.getProfileAsync(true);
      this.loading = false;
      $("#reinvest-modal").modal('hide');
    } catch(error) {
      $("#reinvest-modal").modal('hide');
      this.loading = false;
      alert("Unable to complete your request, please try again.");
    }
  }

}
