import { Component, OnInit } from '@angular/core';
import { ClientEntity } from '../profile/profile.model';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { ProfileService } from '../profile/profile.service';
import { PackagesService } from './packages.service';
import { Package, PackageResponse } from './package.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
declare const jQuery: any;

@Component({
  selector: 'app-packages',
  templateUrl: './packages.component.html',
  styleUrls: ['./packages.component.css']
})
export class PackagesComponent extends AbstractBaseComponent implements OnInit {
    public activeProfile: ClientEntity;
    public packages: Package[];
    public selectedPackage: Package;
    public packageForm: FormGroup;
    constructor(private pakagesService: PackagesService,
        private formBuilder: FormBuilder,
        protected profileService: ProfileService) {
        super(profileService);
        this.dataTableName = 'dt-packages';
        this.pageTitle = 'Manage Packages';
        this.pageIcon = 'fal fa-users';
    }

    async ngOnInit() {
        this.listPackages();
    }

    private async listPackages() {
        this.loadingStart();
        const response: PackageResponse = await this.pakagesService.fetchPackagesAsync();
        this.packages = response.packages;
        this.setPackage(this.packages[0].id);
        this.addTableOption('responsive', true);
        this.addTableOption('pageLength', 50);
        this.addDataTableButtons();
        this.addTableOption('bFilter', false);
        this.loadingFinished();
    }

    public async setPackage(selectedPackageId:number) {
        const singlePackageResponse: PackageResponse = await this.pakagesService.fetchSinglePackageAsync(selectedPackageId);
        console.log(singlePackageResponse);
        this.createPackageForm(singlePackageResponse.package);
    }

    private createPackageForm(singlePackage: Package) {
      this.packageForm = this.formBuilder.group({
        id: [singlePackage.id, [Validators.required]],
        name: [singlePackage.name, [Validators.required]],
        min_amount: [singlePackage.min_amount, Validators.required],
        max_amount: [singlePackage.max_amount, Validators.required],
        monthly_income: [singlePackage.monthly_income, Validators.required],
        duration: [singlePackage.duration, Validators.required],
        referral_income: [singlePackage.referral_income, Validators.required],
        binary_income: [singlePackage.binary_income, Validators.required],
        capping_limit_for_weekly_withrawal: [singlePackage.capping_limit_for_weekly_withrawal],
        withdrawal_fee: [singlePackage.withdrawal_fee, Validators.required],
      });
      this.ready= true;
    }

   public async updatePackage() {
      const singlePackageResponse: PackageResponse = await this.pakagesService.updatePackageAsync(this.packageForm.value);
      jQuery('#update-package-modal').modal('hide');
      this.listPackages();
    }
}
