import { Injectable } from '@angular/core';
import { BaseAPIClass } from '@app/core/class/baseAPI.class';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { PackageResponse } from './package.model';
import { Package } from '@app/frontend/pakcages/package.model';

@Injectable({providedIn: 'root'})
export class PackagesService extends BaseAPIClass {
  private readonly URL_FETCH_PACKAGES: string = 'packages';
  private readonly URL_FETCH_PACKAGE: string = 'single_package';
  private readonly URL_UPDATE_PACKAGES: string = 'packages/update';

  constructor(
    protected httpClient: HttpClient,
    protected authenticationService: AuthenticationService) {
    super(httpClient, authenticationService);
  }

  public async fetchPackagesAsync() {
    return await this.postAsync<PackageResponse>(this.URL_FETCH_PACKAGES).toPromise();
  }

  public async fetchSinglePackageAsync(packageId:number) {
    return await this.postAsync<PackageResponse>(this.URL_FETCH_PACKAGE,{packageId}).toPromise();
  }


  public async updatePackageAsync(singlePackage: Package) {
    return await this.postAsync<PackageResponse>(
      this.URL_UPDATE_PACKAGES,
      singlePackage
    ).toPromise();
  }

}
