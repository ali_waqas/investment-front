export interface Package {
    id: number;
    name: string;
    min_amount: string;
    max_amount: string;
    monthly_income: string;
    duration: string;
    referral_income: string;
    binary_income: string;
    capping_limit_for_weekly_withrawal: string;
    withdrawal_fee: string;
    extra_param_1: string;
    extra_param_2: string;
    created_at?: any;
    updated_at?: any;
}

export interface PackageResponse {
    packages: Package[];
    package?: Package;
}

