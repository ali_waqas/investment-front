import { Component, OnInit } from '@angular/core';
import { ClientKYCDocuments } from '../profile/profile.model';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { ProfileService } from '../profile/profile.service';
import { Package } from '@app/frontend/pakcages/package.model';
import { ClientsService } from '../clients/clients.service';

@Component({
  selector: 'app-kyc',
  templateUrl: './kyc.component.html',
  styleUrls: ['./kyc.component.css']
})
export class KYCComponent extends AbstractBaseComponent implements OnInit {
    public KYCDocuments: any;
    public selectedPackage: Package;
    constructor(private clientsService: ClientsService,
        protected profileService: ProfileService) {
        super(profileService);
        this.dataTableName = 'dt-kyc';
        this.pageTitle = 'KYC';
        this.pageIcon = 'fal fa-file-pdf';
    }

    async ngOnInit() {
        this.listKYC();
    }

    private async listKYC() {
      try {
        this.loadingStart();
        const clientKYC: ClientKYCDocuments = await this.clientsService.fetchallKYCDocumentsAsync();
        this.KYCDocuments = clientKYC.documents;
        this.addTableOption('responsive', true);
        this.addTableOption('pageLength', 50);
        this.addDataTableButtons();
        this.addTableOption('bFilter', false);
        this.loadingFinished();
      } catch (error) {
        console.log(error);
      }
    }
}
