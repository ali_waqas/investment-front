import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MediaComponent } from './media.component';


@NgModule({
    declarations: [
        MediaComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    exports: [
        MediaComponent,
    ],
    providers: [],
    entryComponents: [
        MediaComponent,
    ],
  })
export class MediaModule { }
