import { StringMap } from '@angular/compiler/src/compiler_facade_interface';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange } from '@angular/core';
import { DynamicScriptLoaderService } from '@app/shared/services/dynamic-script-loader.service';
import { MediaService } from './media.service';
import { MediaUploadResponse } from './models/media.model';

@Component({
  selector: 'app-upload-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.scss']
})
export class MediaComponent implements OnInit {
    private dropZoneOptions: StringMap[] = [];
    private params: StringMap = {};

    private document_type_: string = null;
    @Input()  public url: string = null;
    @Input()  public multipleUploads = false;
    @Input()  public max_files = 100;
    @Input()  public dropzone_id = '';
    @Output() mediaUploaded = new EventEmitter();

    constructor(
        private dynamicScriptLoaderService: DynamicScriptLoaderService,
        private mediaService: MediaService) {
    }

    get document_type() {
        return this.document_type_;
    }
    @Input()
    set document_type(val: string) {
        this.params['document_type'] = val;
    }

    ngOnInit() {
        this.dynamicScriptLoaderService.load(['dropzone']).then((scripts) => {
            this.initDropZone();
        });
    }

    private initDropZone() {
        if (this.dropzone_id !== '') {
            (<any>$)('#' + this.dropzone_id).dropzone(
                this.getDropZoneOptions()
            );
        } else {
            (<any>$)('.dropzone').dropzone(
                this.getDropZoneOptions()
            );
        }
    }

    protected addDropZoneOption(name: string, value: any) {
        this.dropZoneOptions.push({name, value});
    }

    private getPayload() {
        if (this.document_type != null) {
            this.params['document_type'] = this.document_type;
        }
        return this.params;
    }

    private get uploadUrl() {
        return (this.url != null) ? this.url : this.mediaService.getUploadUrl();
    }

    protected getDropZoneOptions() {
        const this_ = this;
        if (this.dropZoneOptions.length === 0 ) {
            return {
                        url: this.uploadUrl,
                        parallelUploads: 1,
                        maxFiles: this.max_files,
                        uploadMultiple: this.multipleUploads,
                        params: this.getPayload(),
                        headers: this.mediaService.getHeaders(),
                        success: function(file: any, response: MediaUploadResponse) {
                            this_.mediaUploaded.emit({file, response});
                        }
                    };
        } else {
            const dropZoneOptions: StringMap = {};
            this.dropZoneOptions.forEach(option => {
                dropZoneOptions[option.name] = option.value;
            });
            return dropZoneOptions;
        }
    }


}
