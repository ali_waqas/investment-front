import { Injectable } from '@angular/core';
import { BaseAPIClass } from '@app/core/class/baseAPI.class';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '@app/core/authentication/authentication.service';

@Injectable({ providedIn: 'root' })
export class MediaService extends BaseAPIClass {

    private readonly URL_UPLOAD_MEDIA: string = 'https://server.optionslegion.com/api/documents/store';
    // private readonly URL_UPLOAD_MEDIA: string = 'api/documents/store';
    private readonly URL_MEDIA_BY_ENTITY: string = 'media/entity';
    private readonly URL_ALL_MEDIA: string = 'media/list';
    private readonly URL_MEDIA: string = 'media';
    constructor(
    protected httpClient: HttpClient,

    protected authenticationService: AuthenticationService) {
        super(httpClient, authenticationService);
    }

    public getUploadUrl() {
        return this.URL_UPLOAD_MEDIA;
    }

    public getHeaders() {
        return this.getAuthHeadersAsync();
    }
}
