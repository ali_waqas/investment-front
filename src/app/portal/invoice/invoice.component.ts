import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DepositsService } from '../deposits/deposits.service';
import { PerfectMoneyRequest } from '../deposits/deposit.model';
import { ProfileService } from '../profile/profile.service';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent extends AbstractBaseComponent implements OnInit {

  public paymentStatus: boolean;
  public perfectMoneyRequest: PerfectMoneyRequest;
  constructor(private router: Router,
              private depositService: DepositsService,
              protected profileService: ProfileService) {
    super(profileService);
   }

  async ngOnInit() {
    this.pageTitle = 'Perfect Money Deposit';
    this.pageIcon = 'fal fa-dollar-sign';
    const params = this.router.parseUrl(this.router.url);
    if (params.queryParams.session) {
      this.perfectMoneyRequest = await this.depositService.getPerfectMoneyRequestAsync({payment_id: params.queryParams.session});
    }
  }

}
