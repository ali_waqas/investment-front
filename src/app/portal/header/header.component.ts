import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@app/core';
import { Router } from '@angular/router';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { ProfileService } from '../profile/profile.service';
import { BinaryService } from '@app/shared/services/binary.service';
import { BinaryProfile } from '@app/binary/profile/profile.model';
import { AcademyAccountResponse } from '../profile/profile.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent extends AbstractBaseComponent implements OnInit {
  public is_Admin:boolean;
  constructor(
    public router: Router,
    private authenticationService: AuthenticationService,
    private binaryService: BinaryService,
    profileService: ProfileService) {
      super(profileService);
    }

    async ngOnInit() {

    this.client = await this.profileService.getProfileAsync();

    if (this.isAdmin()) {
      this.is_Admin = true;
    }
    else {
      this.is_Admin = false;
    }
  }

  public async logoutAsync() {
    await this.authenticationService.logout().toPromise();
  }

  public async showClientDashboard() {
          this.router.navigateByUrl('portal/client-dashboard');
  }

  public async checkAcademyRecord() {
    try {
      const academyProfile:AcademyAccountResponse = await this.profileService.getAcademyProfileAsync();
      if (academyProfile.is_account_exists) {
          this.redirectToAcademyWebsite();
      } else  {
          this.router.navigateByUrl('portal/academy-signup');
      }
    } catch (error) {
      console.log(error);
    }
  }


   private redirectToAcademyWebsite() : void {
      window.open("https://academy.optionslegion.com/", "_blank");
    }

  public async loginToBinaryPlatform() {
    const client = await this.profileService.getProfileAsync();
    try {
        const authenticationToken = await this.binaryService.getAuthenticationTokenAsync(false, true);
        if ( authenticationToken ) {
            this.router.navigateByUrl('binary/dashboard');
        }
    } catch (ex) {
        // if application fails to get token, that means there is no profile.
        const binaryProfile = <BinaryProfile> await this.binaryService.createAccountAsync({email: client.profile.email, demo: false});
        if (binaryProfile) {
            this.router.navigateByUrl('binary/dashboard');
        }
    }
  }

}
