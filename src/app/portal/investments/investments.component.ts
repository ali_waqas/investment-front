import { Component, OnInit } from '@angular/core';
import { Investment } from '../deposits/deposit.model';
import { DepositsService } from '../deposits/deposits.service';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { ProfileService } from '../profile/profile.service';
import { Router } from '@angular/router';
import { LocalStorageService } from '@app/core';

@Component({
  selector: 'app-investments',
  templateUrl: './investments.component.html',
  styleUrls: ['./investments.component.css']
})
export class InvestmentsComponent extends AbstractBaseComponent implements OnInit {

  public investments: Investment[];
  constructor(profileService: ProfileService,
    private depositsService: DepositsService,
    private localStorage: LocalStorageService,
    private router: Router) {
    super(profileService);
   }

  async ngOnInit() {
    this.pageIcon = 'fal fa-shield-alt';
    this.pageTitle = 'Investments';
    const response: any = await this.depositsService.getInvestmentsAsync();
    this.investments = response.deposits;
  }

  public goToDepositWithPerfectMoney() {
    this.router.navigateByUrl('/portal/add_funds');
  }

  public selectPackage(packageId: number) {
    this.localStorage.setItem('portal-package-id', packageId);
    this.router.navigateByUrl('/portal/add_funds');
  }

}
