import { Component, OnInit } from '@angular/core';
import { ClientEntity, Client, ClientKYCDocuments, Document } from '../profile/profile.model';
import { Alert } from '@app/shared/models/alert.model';
import { ApplicationEvent } from '@app/shared/services/alert-modal.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { ProfileService } from '../profile/profile.service';
import { ManualDeposit } from '../deposits/deposit.model';
import { ClientsService } from '../clients/clients.service';

@Component({
  selector: 'app-pending-registrations',
  templateUrl: './pending-registrations.component.html',
  styleUrls: ['./pending-registrations.component.css']
})
export class PendingRegistrationsComponent extends AbstractBaseComponent implements OnInit {
    public clients: ClientEntity[] = [];
    public profileForm: FormGroup;
    public profileReady = false;
    public depositFormReady = false;
    public depositForm: FormGroup;
    public activeProfile: ClientEntity;
    public KYCDocuments: Document[] = [];
    constructor(private clientsService: ClientsService,
        private applicationEvent: ApplicationEvent,
        protected profileService: ProfileService) {
        super(profileService);
        this.dataTableName = 'dt-clients';
        this.pageTitle = 'Pending Registrations';
        this.pageIcon = 'fal fa-users';
    }

    async ngOnInit() {
        this.listClients();
    }

    private async listClients() {
        this.loadingStart();
        this.clients = await this.clientsService.fetchPendingRegistrationsAsync();
        this.setPages(this.clients.length);
        this.addTableOption('responsive', true);
        this.addTableOption('pageLength', 50);
        this.addDataTableButtons();
        this.addTableOption('bFilter', false);
        // this.initDataTable();
        this.loadingFinished();
    }

    public async markAsFeePaid(client: ClientEntity) {
        try {
            client.is_signup_fee_paid = 'Yes';
            client.password_confirmation = client.password;
            const response: any = await this.clientsService.completePendingRegistration(client);
            const alert: Alert = {
                title: 'Account Activated',
                sub_title: 'Profile changes saved',
                body: 'Client profile has been updated successfuly.',
                ok_text: 'OK',
                close_text: 'CLOSE'};
            this.applicationEvent.fireAlert(alert);
        } catch (error) {
            console.log(error);
        }
    }



    // CAN BE CALLED IF WANT TO RETRY ACADEMY SIGNUP


    // public async callFactorySignUpAPI(client: ClientEntity){
    //   try {
    //     const registrationResponse = await this.profileService.registerFactoryAPIAsync(client);
    //     this.hasErrors = false;
    //     const alert: Alert = {
    //       title: 'Account Registered Successfully',
    //       sub_title: 'Profile created successfully',
    //       body: 'Client Academy profile has been created successfuly.',
    //       ok_text: 'OK',
    //       close_text: 'CLOSE'};
    //   this.applicationEvent.fireAlert(alert);
    //   } catch (error) {
    //     this.httpError = error;
    //     this.hasErrors = true;
    //   }
    // }



}
