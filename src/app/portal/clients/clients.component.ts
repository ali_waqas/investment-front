import { Component, OnInit } from '@angular/core';
import { ClientsService } from './clients.service';
import { ClientEntity, Client, ClientKYCDocuments, Document, TypeOfAuthentication, ClientResponse, ClientsFilter, Profile } from '../profile/profile.model';
import { Alert } from '@app/shared/models/alert.model';
import { ApplicationEvent } from '@app/shared/services/alert-modal.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { ProfileService } from '../profile/profile.service';
import { ManualDeposit } from '../deposits/deposit.model';
import { Analysis,DashboardItem, DashboardResponse } from '@app/dashboard/dashboard/dashboard.model';
import { DashboardService } from '@app/dashboard/dashboard/dashboard.service';

declare const $: any;
@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.css']
})
export class ClientsComponent extends AbstractBaseComponent implements OnInit {
    public clients: ClientEntity[] = [];
    public clientResponse: ClientResponse;
    private clientsFilter: ClientsFilter  = {};
    public client: Client;
    public profileForm: FormGroup;
    public profileReady = false;
    public depositFormReady = false;
    public alreadyDisabled = false;
    public alreadyGoogleDisabled = false;
    public depositForm: FormGroup;
    public clientFilterForm: FormGroup;
    public activeProfile: ClientEntity;
    public KYCDocuments: Document[] = [];
    public analysis: Analysis;
    public analysisReady = false;
    public dashboardItems: DashboardItem[] = [];
    constructor(private clientsService: ClientsService,
        private applicationEvent: ApplicationEvent,
        protected profileService: ProfileService,
        private dashboardService: DashboardService,
        private formBuilder: FormBuilder) {
        super(profileService);
        this.dataTableName = 'dt-clients';
        this.pageTitle = 'Manage Clients';
        this.pageIcon = 'fal fa-users';
    }

    protected async fetchClientProfile() {
      if (this.profileService.authenticated()) {
          this.client = await this.profileService.getProfileAsync();
      }
    }

    protected async fetchClientDashboardData() {
        this.client = await this.profileService.getProfileAsync();
    }

    async ngOnInit() {
        this.listClients();
        await this.fetchClientProfile();
        await this.fetchClientDashboardData();
        this.prepareDashboardItems();
        this.initForm();
    }

    private async listClients() {
        this.loadingStart();
        this.clientResponse = await this.clientsService.fetchClientsAsync(this.clientsFilter);
        this.setPages(this.clientResponse.clients.last_page);
        this.addTableOption('responsive', true);
        this.addTableOption('pageLength', 50);
        this.addDataTableButtons();
        this.addTableOption('bFilter', false);
        // this.initDataTable();
        this.loadingFinished();
    }

    private async prepareDashboardItems() {
      const dashboardResponse: DashboardResponse = await this.dashboardService.getAnalysisData();
      this.analysis = dashboardResponse.analysis;
      this.analysisReady = true;

      const totalClients = Number(this.analysis.total_clients).toString();
      const totalDeposits = Number(this.analysis.total_deposits).toString();
      const totalWithdrawals = Number(this.analysis.total_withdrawals).toString();
      this.dashboardItems = [
        {
          name: 'Total Clients',
          value: totalClients,
          background_class: 'bg-success-400',
          icon: 'fa-users',
        },
        {
          name: 'Total Deposits',
          value: totalDeposits,
          background_class: 'bg-danger-500',
          icon: 'fal fa-credit-card',
        },
        {
          name: 'Total Withdrawals',
          value: totalWithdrawals,
          background_class: 'bg-primary-300',
          icon: 'fal fa-credit-card',
        }
      ];
    }

    public async markAsFeePaid(client: ClientEntity) {
        try {
            client.is_signup_fee_paid = 'Yes';
            const response: any = await this.clientsService.saveClientProfileAsync(client);
            const alert: Alert = {
                title: 'Account Activated',
                sub_title: 'Profile changes saved',
                body: 'Client profile has been updated successfuly.',
                ok_text: 'OK',
                close_text: 'CLOSE'};
            this.applicationEvent.fireAlert(alert);
        } catch (error) {
            console.log(error);
        }
    }

    public async updateProfile() {
        const profile: ClientEntity = this.profileForm.value;
        try {
          const updatedProfile = await this.clientsService.saveClientProfileAsync(profile);
          this.listClients();
          const alert: Alert = {title: 'Profile changes saved',
          sub_title: 'Request completed',
          body: 'Client profile has been updated successfuly.',
          ok_text: 'OK',
          close_text: 'CLOSE'};
          this.applicationEvent.fireAlert(alert);
          (<any>$)('#update-client-modal').modal('hide');
        } catch (error) {
          this.httpError = error;
          this.hasErrors = true;
        }
    }

    public async resetPassword(client: ClientEntity) {
        try {
            client.password = this.makeid(6);
            const response: any = await this.clientsService.saveClientProfileAsync(client);
            const alert: Alert = {
                title: 'Password Reset Successfuly',
                sub_title: 'Profile changes saved',
                body: 'Client password has been changed, new Password is: <b>' + client.password + '</b>',
                ok_text: 'OK',
                close_text: 'CLOSE'
            };
            this.applicationEvent.fireAlert(alert);
        } catch (error) {
            console.log(error);
        }
    }

    private makeid(length: number) {
        let result           = '';
        const characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for ( let i = 0; i < length; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    public openUpdateProfile(client: ClientEntity) {
        this.activeProfile = client;
        this.createProfileForm(client);
        (<any>$)('#update-client-modal').modal();
        this.profileReady = true;
    }

    public openDisable2FA(client: ClientEntity) {
      this.activeProfile = client;
      console.log('Disable Button Clicked');
      console.log(this.clients);
      console.log('Single client is:');
      console.log(client);
      (<any>$)('#disable-2fa-modal').modal();
      this.profileReady = true;
      if (this.client.profile.type_of_authentication.toString() == TypeOfAuthentication.google_authenticator.toString()) {

      }
  }

    public openDepositForm(client: ClientEntity) {
        this.activeProfile = client;
        this.createDepositForm(client);
        (<any>$)('#client-deposit-modal').modal();
        this.depositFormReady = true;
    }

    public deleteClient(client: ClientEntity) {
        console.log(client);
    }

    private createProfileForm(client: ClientEntity) {
        this.profileForm = this.formBuilder.group({
          id: [client.id, [Validators.required]],
          first_name: [client.first_name, [Validators.required]],
          last_name: [client.last_name, [Validators.required]],
          email: [client.email, [Validators.required]],
          mobile: [client.mobile, []],
          phone: [client.phone],
          city: [client.city],
          address: [client.address],
          country: [client.country],
          balance: [client.balance, [Validators.required]],
          roi_wallet: [client.roi_wallet,[]],
          binary_wallet: [client.binary_wallet,[]],
          award_amount: [client.award_amount,[]],
          reward_amount: [client.reward_amount,[]],
          is_signup_fee_paid: [client.is_signup_fee_paid, [Validators.required]],
          daily_roi: [client.daily_roi, []],
          total_withdrawals: [client.total_withdrawals, []],
          total_deposits: [client.total_deposits, []],
          last_deposit_amount: [client.last_deposit_amount, []],
          last_withdrawal_amount: [client.last_withdrawal_amount, []],
          referral_income: [client.referral_income, []],
          profit_from_last_balanced_node: [client.profit_from_last_balanced_node, []],
          daily_roi_last_income: [client.daily_roi_last_income, []],
        });
    }

    public createDepositForm(client: ClientEntity) {
        this.depositForm = this.formBuilder.group({
            client_id: [client.id, [Validators.required]],
            first_name: [client.first_name, [Validators.required]],
            last_name: [client.last_name, [Validators.required]],
            email: [client.email, [Validators.required]],
            amount: ['', [Validators.required]],
            method_id: ['', [Validators.required]],
            details: ['', [Validators.required]],
            type: ['INVESTMENT', [Validators.required]]
        });
    }

    public async saveDeposit() {
        try {
            const manualDeposit: ManualDeposit = this.depositForm.value;
            const response: any = await this.clientsService.createManualDeposit(manualDeposit);
            const alert: Alert = {
                title: 'Deposit Saved',
                sub_title: 'New deposit has been created',
                body: 'Client account has been deposited with $' + manualDeposit.amount,
                ok_text: 'OK',
                close_text: 'CLOSE'
            };
            this.applicationEvent.fireAlert(alert);
            $("#client-deposit-modal").modal('hide');
        } catch (error) {
            console.log(error);
        }
    }

    public async viewKYCDocuments(client: ClientEntity) {
        try {
            this.activeProfile = client;
            const clientKYC: ClientKYCDocuments = await this.clientsService.fetchClientKYCDocumentsAsync(client.id);
            this.KYCDocuments = clientKYC.documents;
            (<any>$)('#client-kyc-modal').modal();
        } catch (error) {
            console.log(error);
        }
    }

    public async changeKYCStatus(client: ClientEntity, status: string) {
        try {
            const kycApprovalResponse: any = await this.clientsService.approveKYCAsync(client.id, status);
            const alert: Alert = {
                title: 'KYC Updated',
                sub_title: 'Request Completed',
                body: kycApprovalResponse.message,
                ok_text: 'OK',
                close_text: 'CLOSE'};
            this.applicationEvent.fireAlert(alert);
        } catch (error) {
            console.log(error);
        }
    }

    public async disableEmail2FA(client: ClientEntity) {
      try {
          const disableResponse: any = await this.clientsService.disableEmail2FAAsync(client.id);
          console.log(disableResponse.message);
          if (disableResponse.message == 'Email 2fa is already disabled') {
                this.alreadyDisabled = true;
            this.alreadyGoogleDisabled = false;

          }
          else{
               const alert: Alert = {
            title: 'Two Factor Authentication Disabled',
            sub_title: 'Email 2FA',
            body: 'Email Two Factor Authentication has been Disabled',
            ok_text: 'OK',
            close_text: 'CLOSE'
            };
          this.applicationEvent.fireAlert(alert);
            (<any>$)("#disable-2fa-modal").modal('hide');
          }

      } catch (error) {
          console.log(error);
      }
  }

  public async disableGoogle2FA(client: ClientEntity) {
    try {
        const disableResponse: any = await this.clientsService.disableGoogle2FAAsync(client.id);
        if (disableResponse.message == 'Google 2fa is already disabled') {
          this.alreadyGoogleDisabled = true;
          this.alreadyDisabled = false;
        } else {
          const alert: Alert = {
            title: 'Two Factor Authentication Disabled',
            sub_title: 'Google 2FA',
            body: 'Google Two Factor Authentication has been Disabled',
            ok_text: 'OK',
            close_text: 'CLOSE'
          };
          this.applicationEvent.fireAlert(alert);
          (<any>$)("#disable-2fa-modal").modal('hide');
        }

    } catch (error) {
        console.log(error);
    }
 }


  private initForm() {
    this.clientFilterForm = this.formBuilder.group({
      client_id: ['', []],
      name: ['', []],
      email: ['', []],
    });
  }

  public applyFilers() {
    this.clientsFilter = this.clientFilterForm.value;
    this.listClients();
  }

  public clearFilters() {
    this.initFilters();
    this.listClients();
  }

  public initFilters() {
    this.clientsFilter = { page: 1, per_page: 100 };
  }

  public async gotoPage(page: number) {
    this.clientsFilter.page = page;
    this.updatePagerLinks(page);
    this.listClients();
  }

  public async allocateRoi(client: Profile) {
    try {
      const response: any = await this.clientsService.allocateRoiAsync(client.id);
      const alert: Alert = {
          title: 'Dailt ROI Allocated',
          sub_title: "",
          body: response.daily_roi_income,
          ok_text: 'OK',
          close_text: 'CLOSE'};
      this.applicationEvent.fireAlert(alert);
    } catch (error) {
        console.log(error);
    }
  }

  public async addAward(client: Profile, badge: string) {
    try {
      const response: any = await this.clientsService.addAwardAsync(client.id,badge);
      const alert: Alert = {
          title: 'Award Added Successfully',
          sub_title: "",
          body: response.message,
          ok_text: 'OK',
          close_text: 'CLOSE'};
      this.applicationEvent.fireAlert(alert);
    } catch (error) {
        console.log(error);
    }
  }
  public async addbothBadges(client: Profile, badge: string) {
    try {
      const response: any = await this.clientsService.addAwardAsync(client.id,badge);
      const alert: Alert = {
          title: 'Badges Added Successfully',
          sub_title: "",
          body: response.message,
          ok_text: 'OK',
          close_text: 'CLOSE'};
      this.applicationEvent.fireAlert(alert);
    } catch (error) {
        console.log(error);
    }
  }
  public changeValue(number: Number){
    return parseFloat(number.toString());
  }
}
