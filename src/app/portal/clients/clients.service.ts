import { Injectable } from '@angular/core';
import { BaseAPIClass } from '@app/core/class/baseAPI.class';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { Client, ClientEntity, ClientKYCDocuments, ClientResponse, ClientsFilter, ClientSummary, ClientSummaryResponse, SendEmail, ShortProfile } from '../profile/profile.model';
import { ManualDeposit } from '../deposits/deposit.model';

@Injectable({providedIn: 'root'})
export class ClientsService extends BaseAPIClass {
  private readonly URL_FETCH_CLIENTS: string = 'clients';
  private readonly URL_FETCH_CLIENTS_PENDING_REGISTRATIONS: string = 'clients/pending/registrations';
  private readonly URL_FETCH_CLIENTS_PENDING_REGISTRATIONS_COMPLETE: string = 'clients/pending/registrations/complete';
  private readonly URL_CLIENT_MANUAL_DEPOSIT: string = 'client/deposit/manual';
  private readonly URL_CLIENT_KYC_DOCUMENTS: string = 'client/documents';
  private readonly URL_UPDATE_CLIENT: string = 'client/profile/update';
  private readonly URL_UPDATE_CLIENT_KYC: string = 'client/change-kyc';
  private readonly URL_DISABLE_CLIENT_KYC: string = 'client/disable_email_2fa';
  private readonly URL_DISABLE_GOOGLE_CLIENT_KYC: string = 'client/disable_google_2fa';
  private readonly URL_CLIENT_SUMMARY: string = 'client/{id}/summary';
  private readonly URL_UPDATE_CLIENT_PROFILE: string = 'client/update';
  private readonly URL_SEND_CLIENT_EMAIL: string = 'client/send_email';
  private readonly URL_ALLOCATE_ROI: string = 'client/{clientId}/allocate_roi';
  private readonly URL_ADD_AWARD: string = 'clients/add_reward_badge';
  private readonly URL_FETCH_ALL_KYC: string = 'list_kyc';
  constructor(
    protected httpClient: HttpClient,
    protected authenticationService: AuthenticationService) {
    super(httpClient, authenticationService);
  }

  public async fetchClientsAsync(filters: ClientsFilter) {
    return await this.postAsync<ClientResponse>(this.URL_FETCH_CLIENTS, filters, {}).toPromise();
  }

  public async fetchPendingRegistrationsAsync() {
    return await this.postAsync<ClientEntity[]>(this.URL_FETCH_CLIENTS_PENDING_REGISTRATIONS).toPromise();
  }

  public async fetchClientSummary(clientId: number) {
    const url = this.URL_CLIENT_SUMMARY.replace('{id}', clientId.toString());
    return await this.getAsync<ClientSummaryResponse>(url, {}, {}).toPromise();
  }

  public async updateProfileAsync(profile: ShortProfile) {
    return await this.postAsync<Client>(
      this.URL_UPDATE_CLIENT_PROFILE,
      profile
    ).toPromise();
  }

  public async sendEmail(sendEmail: SendEmail) {
    return await this.postAsync(this.URL_SEND_CLIENT_EMAIL, sendEmail).toPromise();
  }

  public async saveClientProfileAsync(client: ClientEntity) {
    return await this.postAsync(this.URL_UPDATE_CLIENT, client).toPromise();
  }

  public async completePendingRegistration(client: ClientEntity) {
    return await this.postAsync(this.URL_FETCH_CLIENTS_PENDING_REGISTRATIONS_COMPLETE, client).toPromise();
  }

  public async createManualDeposit(manualDeposit: ManualDeposit) {
    return await this.postAsync(this.URL_CLIENT_MANUAL_DEPOSIT, manualDeposit).toPromise();
  }

    public async fetchClientKYCDocumentsAsync(client_id: number) {
        return await this.postAsync<ClientKYCDocuments>(this.URL_CLIENT_KYC_DOCUMENTS + '/' + client_id).toPromise();
    }

    public async fetchallKYCDocumentsAsync() {
      return await this.getAsync<ClientKYCDocuments>(this.URL_FETCH_ALL_KYC).toPromise();
  }

    public async approveKYCAsync(client_id: number, status: string) {
        return await this.postAsync(this.URL_UPDATE_CLIENT_KYC , {client_id, status}).toPromise();
    }

    public async disableEmail2FAAsync(client_id: number) {
      return await this.postAsync(this.URL_DISABLE_CLIENT_KYC , {client_id}).toPromise();
    }

    public async disableGoogle2FAAsync(client_id: number) {
      return await this.postAsync(this.URL_DISABLE_GOOGLE_CLIENT_KYC , {client_id}).toPromise();
    }

    public async allocateRoiAsync(clientId: number) {
      return await this.postAsync(this.URL_ALLOCATE_ROI.replace('{clientId}', clientId.toString()) , {clientId}).toPromise();
    }

    public async addAwardAsync(client_id: number, badge:string) {
      return await this.postAsync(this.URL_ADD_AWARD, {client_id, badge}).toPromise();
    }
}
