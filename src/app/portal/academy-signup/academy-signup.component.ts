import { Component, OnInit } from '@angular/core';
import { Alert } from '@app/shared/models/alert.model';
import { ApplicationEvent } from '@app/shared/services/alert-modal.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { ProfileService } from '../profile/profile.service';
import { ClientsService } from '../clients/clients.service';
import { Profile } from '../profile/profile.model';

@Component({
  selector: 'app-academy-signup',
  templateUrl: './academy-signup.component.html',
  styleUrls: ['./academy-signup.component.css']
})
export class AcademySignUpComponent extends AbstractBaseComponent implements OnInit {
    public profileForm: FormGroup;
    public profile: Profile;
    constructor(private applicationEvent: ApplicationEvent,
        protected profileService: ProfileService,
        private formBuilder: FormBuilder
    ) {
        super(profileService);
        this.pageTitle = 'Academy Signup';
        this.pageIcon = 'fal fa-users';
    }

    async ngOnInit() {
      const response = await this.profileService.getProfileAsync();
      this.profile = response.profile;
      this.createAcademyProfileForm();
    }

    public async callFactorySignUpAPI(){
      try {
        const registrationResponse = await this.profileService.registerFactoryAPIAsync(this.profileForm.value);
        this.hasErrors = false;
        const alert: Alert = {
          title: 'Academy Account Created Successfully',
          sub_title: 'Profile created successfully',
          body: 'Client Academy profile has been created successfuly.',
          ok_text: 'OK',
          close_text: 'CLOSE'};
          this.applicationEvent.fireAlert(alert);
          this.redirectToAcademyWebsite();
      } catch (error) {
        const alert: Alert = {
          title: 'Failed to Create Academy Profile',
          sub_title: 'Profile Creation Failed',
          body: 'Academy Profile Already Exists',
          ok_text: 'OK',
          close_text: 'CLOSE'};
          this.applicationEvent.fireAlert(alert);
      }
    }

    private redirectToAcademyWebsite() : void {
      window.open("https://academy.optionslegion.com/", "_blank");
    }

    private createAcademyProfileForm() {
      this.profileForm = this.formBuilder.group({
        first_name: [this.profile.first_name, []],
        last_name: [this.profile.first_name, []],
        email: [this.profile.email, []],
        password: ['',[Validators.required]],
      });
      this.ready = true;
    }



}
