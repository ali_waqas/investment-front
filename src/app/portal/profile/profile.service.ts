import { Injectable } from '@angular/core';
import { BaseAPIClass } from '@app/core/class/baseAPI.class';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import {
  Client,
  ShortProfile,
  Registration,
  UploadDocument,
  Profile,
  TwoFAQRCodeResponse,
  TwoFAEmailCodeResponse,
  AcademyAccountResponse
} from './profile.model';
import { LocalStorageService } from '@app/core';
import { AuthenticationEvent } from '@app/core/authentication/authentication.model';
import { TreeParentNode } from '../network/tree-parent-node.model';
import { MessageCount } from '../sidebar/sidebar.component';

@Injectable({ providedIn: 'root' })
export class ProfileService extends BaseAPIClass {
  private readonly URL_PROFILE: string = 'profile';
  private readonly URL_CLIENT_PROFILE: string = 'open/profile';
  private readonly URL_VALIDATE_2FA: string = '2fa/validate';
  private readonly URL_REGISTER: string = 'client/register';
  private readonly URL_FACTORY_API_REGISTER: string = 'client/create_factory_signup';
  private readonly URL_INCOME_REFERRAL: string = 'client/income/referrals';
  private readonly URL_INCOME_BINARY: string = 'client/income/binary';
  private readonly URL_UPDATE_PROFILE: string = 'profile/update';
  public readonly STORAGE_KEY_PROFILE: string = 'ClientProfile';
  private readonly URL_FORGOT_PASSWORD: string = 'client/forgot-password';
  private readonly URL_RESET_PASSWORD: string = 'client/reset-password';
  private readonly URL_CHANGE_PASSWORD: string = 'client/password';
  private readonly URL_NETWORK: string = 'client/network';
  private readonly URL_GENEAOLGY_TREE: string = 'client/network-tree';
  private readonly URL_GENEAOLGY_TREE_NODE: string = 'client/network-tree/node';
  private readonly URL_UPLOAD_DOCUMENT: string = 'documents/store';
  public readonly URL_TEMP_REG = 'open/temp_reg/';
  public readonly URL_CHANGE_2FA = 'change2fa';
  public readonly URL_GENERATE_2FA_QR_CODE = 'generate_2fa_qrcode';
  public readonly URL_GENERATE_2FA_EMAIL_CODE = 'generate_2fa_email_code';
  public readonly URL_ENABLE_GOOGLE_2FA = 'enable_google_2fa';
  public readonly URL_ENABLE_EMAIL_2FA = 'enable_email_2fa';
  public readonly URL_DISABLE_EMAIL_2FA = 'disable_email_2fa';
  public readonly URL_DISABLE_GOOGLE_2FA = 'disable_google_2fa';
  public readonly URL_SHORT_CLIENT_PROFILE = 'client/short_profile';
  private readonly URL_FETCH_UNREAD_MESSAGES_DATA: string = 'unread_messages_count';
  public readonly URL_CHECK_ACADEMY_ACCOUNT = 'client/check_academy_account';
  public readonly URL_SUBMIT_KYC = 'documents/submit_kyc';


  constructor(
    protected httpClient: HttpClient,
    protected authenticationService: AuthenticationService,
    private localStorageService: LocalStorageService
  ) {
    super(httpClient, authenticationService);
    this.authenticationSubscription = this.authenticationService.onAuthenticationChange.subscribe(
      this.handleAuthenticationChanged.bind(this)
    );
  }

  public authenticated() {
    return this.authenticationService.isAuthenticated();
  }

  public async getProfileAsync(refresh: boolean = false) {
    let client: Client = null;
    if (!refresh) {
      const savedProfile = this.localStorageService.getItem(
        this.STORAGE_KEY_PROFILE
      );
      if (savedProfile) {
        client = JSON.parse(savedProfile);
      }
    } else {
      client = await this.postAsync<Client>(this.URL_PROFILE).toPromise();
      this.saveProfileLocally(client);
    }
    // if (client.profile.is_signup_fee_paid === 'No') {
    //    this.authenticationService.onAuthenticationChange.emit({status: 'subscription-not-paid', payload: {}});
    // }
    return client;
  }

  public async getClientProfileAsync() {
      let client: Client = await this.postAsync<Client>(this.URL_SHORT_CLIENT_PROFILE).toPromise();
      this.saveProfileLocally(client);
      return client;
  }

  public async getAcademyProfileAsync() {
    try {
      let client: AcademyAccountResponse = await this.postAsync<AcademyAccountResponse>(this.URL_CHECK_ACADEMY_ACCOUNT).toPromise();
      return client;
    } catch (error) {
      console.log(error);

    }
}

  public async fetchMyProfileAsync(profile: Profile) {
    return await this.postAsync<Client>(
      this.URL_CLIENT_PROFILE,
      profile
    ).toPromise();
  }

  public async validate2FAAsync(profile: Profile) {
    return await this.postAsync(this.URL_VALIDATE_2FA, profile).toPromise();
  }

  public async registerAsync(registration: Registration) {
    const registeration = await this.postAsync<Client>(
      this.URL_REGISTER,
      registration
    ).toPromise();
    return registeration;
  }

  public async registerFactoryAPIAsync(registration: any) {
    const registeration = await this.postAsync<Client>(
      this.URL_FACTORY_API_REGISTER,
      registration
    ).toPromise();
    return registeration;
  }

  public async updateProfileAsync(profile: ShortProfile) {
    const updatedProfile = await this.postAsync<Client>(
      this.URL_UPDATE_PROFILE,
      profile
    ).toPromise();
    this.saveProfileLocally(updatedProfile);
    return profile;
  }

  private saveProfileLocally(profile?: Client) {
    if (profile) {
      this.localStorageService.setItem(
        this.STORAGE_KEY_PROFILE,
        JSON.stringify(profile)
      );
    } else {
      this.localStorageService.clearItem(this.STORAGE_KEY_PROFILE);
    }
  }

  private async handleAuthenticationChanged(event: AuthenticationEvent) {
    if (event.status === 'logged-in') {
      this.saveProfileLocally();
    } else if (event.status === 'logged-out') {
      this.saveProfileLocally();
    }
  }

  public async getReferralIncome() {
    return await this.postAsync(this.URL_INCOME_REFERRAL).toPromise();
  }

  public async getBinaryIncome() {
    return await this.postAsync(this.URL_INCOME_BINARY).toPromise();
  }

  public async generatePasswordResetEmail(email: string) {
    return await this.postAsync(
      this.URL_FORGOT_PASSWORD,
      { email },
      [],
      true
    ).toPromise();
  }

  public async resetPassword(data: any) {
    return await this.postAsync(
      this.URL_RESET_PASSWORD,
      data,
      [],
      true
    ).toPromise();
  }

  public async changePassword(data: any) {
    return await this.postAsync(this.URL_CHANGE_PASSWORD, data, []).toPromise();
  }

  public async fetchNetwork() {
    return await this.postAsync(this.URL_NETWORK).toPromise();
  }

  public async fetchGenealogyTree() {
    return await this.postAsync(this.URL_GENEAOLGY_TREE).toPromise();
  }

  public async fetchGenealogyTreeNode(nodeId: number) {
    return await this.postAsync<TreeParentNode>(this.URL_GENEAOLGY_TREE_NODE+"/"+nodeId).toPromise();
  }

  public async uploadDocument(document: UploadDocument) {
    return await this.postAsync(this.URL_UPLOAD_DOCUMENT, document).toPromise();
  }

  public getPackageId() {
    const clientJson = this.localStorageService.getItem(
      this.STORAGE_KEY_PROFILE
    );
    const client: Client = JSON.parse(clientJson);
    return client.profile.package_id;
  }

  public getTotalDeposits() {
    const clientJson = this.localStorageService.getItem(
      this.STORAGE_KEY_PROFILE
    );
    const client: Client = JSON.parse(clientJson);
    return client.profile.total_deposits;
  }

  public fetchTempraryRegistrationAsync(id: number) {
    return this.getAsync<any>(this.URL_TEMP_REG + id).toPromise();
  }

  public async enableGoogleAuthenticator2FA(data: any) {
    return await this.postAsync(this.URL_ENABLE_GOOGLE_2FA, data).toPromise();
  }

  public async enableEmail2FA(data: any) {
    return await this.postAsync(this.URL_ENABLE_EMAIL_2FA, data).toPromise();
  }

  public async disableEmail2FA(data: any) {
    return await this.postAsync(this.URL_DISABLE_EMAIL_2FA, data).toPromise();
  }

  public async disableGoogle2FA(data: any) {
    return await this.postAsync(this.URL_DISABLE_GOOGLE_2FA, data).toPromise();
  }

  public async generate2FAQRCode() {
    return await this.postAsync<TwoFAQRCodeResponse>(
      this.URL_GENERATE_2FA_QR_CODE
    ).toPromise();
  }

  public async generate2FAEmailCode() {
    return await this.postAsync<TwoFAEmailCodeResponse>(
      this.URL_GENERATE_2FA_EMAIL_CODE
    ).toPromise();
  }

  public async postMarkAsRead() {
    return await this.postAsync<MessageCount>(this.URL_FETCH_UNREAD_MESSAGES_DATA, {}, {}, false).toPromise();
  }

  public async submitKYCDocuments(documents: any[]) {
    return await this.postAsync<MessageCount>(this.URL_SUBMIT_KYC, {documents}, {}, false).toPromise();
  }
}
