import { Component, OnInit } from '@angular/core';
import { ProfileService } from './profile.service';
import { Client, ClientKYCDocuments, Document, ShortProfile, TwoFAEmailCodeResponse,
  TwoFAQRCodeResponse,
  TypeOfAuthentication,
  UploadDocument
} from './profile.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { ApplicationEvent } from '@app/shared/services/alert-modal.service';
import { Alert } from '@app/shared/models/alert.model';
import { MediaUploadResponse } from '../media/models/media.model';
import { ClientsService } from '../clients/clients.service';

declare const jQuery: any;

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent extends AbstractBaseComponent implements OnInit {
  public hideDropZone = true;
  public KYCDocuments: Document[] = [];
  public profileForm: FormGroup;
  public changePasswordForm: FormGroup;
  public uploadForm: FormGroup;
  public qrCodeForm: FormGroup;
  public emailForm: FormGroup;
  public selectedDocumentType: string;
  public qrCode: TwoFAQRCodeResponse;
  public Code: TwoFAEmailCodeResponse;
  public generatedCode: string;
  public client: Client;
  fileToUpload: File = null;
  public kyc_verified = false;
  public showAlert = false;
  public scanQRCode = false;
  public emailCode = false;
  public showEmailDisable = false;
  public showQrCodeDisable = false;
  public twoFAAlreadyEnabled = false;
  public submitted:boolean;
  public documentsUploaded: boolean = false;
  public showUploadDocuments: boolean = true;
  public uploadedDocuments: any[] = [];
  public submittingKYC = false;
  public submittedKyc = false;

  constructor(
    protected profileService: ProfileService,
    private formBuilder: FormBuilder,
    private applicationEvent: ApplicationEvent,
    private clientsService: ClientsService) {
    super(profileService);

  }

  async ngOnInit() {
    this.pageTitle = 'Profile';
    this.pageIcon = 'fal fa-chart-area';
    this.hasErrors = false;
    this.initBinding();
    await this.getProfile();
    await this.fetchKYCDocuments();
    this.initUploadDocumentForm();
    this.generateQrCodeForm();
    this.generateEmailForm();
    this.ready = true;
  }

  public onTypeSelected(type: string) {
    if (type !== '') {
      this.hideDropZone = false;
      this.selectedDocumentType = type;
    }
  }

  private async getProfile() {
    this.client = await this.profileService.getProfileAsync(true);
    this.createProfileForm();
    this.createPassswordResetForm();
    if(this.client.profile.type_of_authentication.toString()  !== TypeOfAuthentication.none.toString()) {
        this.enable2FA(this.client.profile.type_of_authentication.toString());
    }
  }

  public async enable2FA(type: string) {
    this.client = await this.profileService.getProfileAsync(true);
    if(this.client.profile.type_of_authentication.toString() !== TypeOfAuthentication.none.toString()) {
        this.twoFAAlreadyEnabled = true;
        if(this.client.profile.type_of_authentication.toString() == TypeOfAuthentication.email.toString()) {
          if(type == TypeOfAuthentication.google_authenticator.toString()) {
            this.qrCode = await this.profileService.generate2FAQRCode();
            this.scanQRCode = true;
            this.emailCode = false;
            this.showQrCodeDisable = false;
          }else{
            this.scanQRCode = false;
            this.showEmailDisable = true;
            this.emailCode = true;
          }
        } else if(this.client.profile.type_of_authentication.toString() == TypeOfAuthentication.google_authenticator.toString()) {
          if(type == TypeOfAuthentication.email.toString()) {
            this.submitted = true;
            this.Code = await this.profileService.generate2FAEmailCode();
            this.submitted = false;
            this.generatedCode = this.Code.code;
            this.emailCode = true;
            this.scanQRCode = false;
            this.showEmailDisable = false;
          }else{
            this.emailCode = false;
            this.scanQRCode = true;
            this.showQrCodeDisable = true;
          }
        }
    } else {
        this.twoFAAlreadyEnabled = false;
        if(type == TypeOfAuthentication.email.toString()) {
            if(this.client.profile.type_of_authentication.toString() == TypeOfAuthentication.email.toString()) {
              this.showEmailDisable = true;
              this.emailCode = true;
            }
            this.Code = await this.profileService.generate2FAEmailCode();
            this.generatedCode = this.Code.code;
            this.emailCode = true;
            this.scanQRCode = false;
            this.showEmailDisable = false;
        } else if(type == TypeOfAuthentication.google_authenticator.toString()) {
            this.qrCode = await this.profileService.generate2FAQRCode();
            this.scanQRCode = true;
            this.emailCode = false;
            this.showQrCodeDisable = false;
        }
    }
  }

  public async disableEmail2FA() {
    this.submitted = true;
    const response = await this.profileService.disableEmail2FA({ code: null });
    this.submitted = false;
    const alert: Alert = {
      title: 'Two Factor Authentication Disabled',
      sub_title: 'Email 2FA',
      body: 'Two Factor Authentication has been Disabled',
      ok_text: 'OK',
      close_text: 'CLOSE'
    };
    this.applicationEvent.fireAlert(alert);
    this.emailCode = false;
    this.showEmailDisable = false;
    this.client = await this.profileService.getProfileAsync(true);
    if (this.client.profile.type_of_authentication.toString() == TypeOfAuthentication.google_authenticator.toString()) {
      this.scanQRCode = true;
      this.showQrCodeDisable= true;
    }
    else{
      this.submitted = false;
      this.scanQRCode = false;
      this.showQrCodeDisable= false;
    }
  }



  public async disableGoogle2FA() {
    this.submitted= true;
    const response = await this.profileService.disableGoogle2FA({ code: null });
    this.submitted= false;
    const alert: Alert = {
      title: 'Google Two Factor Authentication Disabled',
      sub_title: 'Google 2FA',
      body: 'Google Two Factor Authentication has been Disabled',
      ok_text: 'OK',
      close_text: 'CLOSE'
    };
    this.applicationEvent.fireAlert(alert);
    this.emailCode = false;
    this.showEmailDisable = false;
    this.scanQRCode = false;
    this.showQrCodeDisable= false;
  }

  public async updateProfile() {
    if (this.client.profile.kyc_verified) {
      this.showAlert = true;
    } else {
      const shortProfile: ShortProfile = this.profileForm.value;
      try {
        const updatedProfile = await this.profileService.updateProfileAsync(
          shortProfile
        );
        const alert: Alert = {
          title: 'Profile changes saved',
          sub_title: 'Request completed',
          body: 'Your profile has been updated successfuly.',
          ok_text: 'OK',
          close_text: 'CLOSE'
        };
        this.applicationEvent.fireAlert(alert);
      } catch (error) {
        this.httpError = error;
        this.hasErrors = true;
      }
    }
  }

  public async changePassword() {
    const passwords: any = this.changePasswordForm.value;
    try {
      const updatedProfile = await this.profileService.changePassword(
        passwords
      );
      jQuery('#password_change_model').modal('hide');
      const alert: Alert = {
        title: 'Password Update',
        sub_title: 'Request completed',
        body: 'Your password has been updated successfuly.',
        ok_text: 'OK',
        close_text: 'CLOSE'
      };
      this.applicationEvent.fireAlert(alert);
    } catch (error) {
      this.httpError = error;
      this.hasErrors = true;
    }
  }

  private createProfileForm() {
    this.profileForm = this.formBuilder.group({
      first_name: [this.client.profile.first_name, [Validators.required]],
      last_name: [this.client.profile.last_name, [Validators.required]],
      email: [this.client.profile.email, [Validators.required]],
      mobile: [this.client.profile.mobile, [Validators.required]],
      phone: [this.client.profile.phone],
      city: [this.client.profile.city],
      address: [this.client.profile.address],
      country: [this.client.profile.country]
    });
  }
  private createPassswordResetForm() {
    this.changePasswordForm = this.formBuilder.group({
      password: ['', [Validators.required]],
      old_password: ['', [Validators.required]],
      password_confirmation: ['', [Validators.required]]
    });
  }

  private initUploadDocumentForm() {
    this.uploadForm = this.formBuilder.group({
      document_file: ['', []],
      document_type: ['', []],
      client_id: [this.client.profile.id, []]
    });
  }

  private generateQrCodeForm() {
    this.qrCodeForm = this.formBuilder.group({
      qr_code: ['', [Validators.required]]
    });
  }

  private generateEmailForm() {
    this.emailForm = this.formBuilder.group({
      code: ['', [Validators.required]]
    });
  }

  public async enableGoogleAuthenticator2FA() {
    try {
      this.submitted = true;
      const code = this.qrCodeForm.value;
      const response = await this.profileService.enableGoogleAuthenticator2FA({
        otp: code.qr_code,
        secret: this.qrCode.key
      });
      this.submitted = false;
      const alert: Alert = {
        title: 'Two Factor Authentication Enabled',
        sub_title: 'Google Authenticator',
        body: 'Your account now is protected with two factor authentication',
        ok_text: 'OK',
        close_text: 'CLOSE'
      };
      this.applicationEvent.fireAlert(alert);
      this.showQrCodeDisable = true;
    } catch (error) {
      this.hasErrors = true;
      this.submitted = false;
      this.qrCodeForm.setValue({qr_code: ''});
      this.httpError = error.error.message;
    }
  }

  public async enableEmail2FA() {
    this.hasErrors = false;
    this.submitted = true;
    this.emailForm.invalid;
    try {
      console.log('Email Enable Validate clicked');
      const code = this.emailForm.value;
      const response = await this.profileService.enableEmail2FA({
        code: code.code , generatedCode: this.generatedCode
      });
    this.submitted = false;
    this.emailForm.valid;
      const alert: Alert = {
        title: 'Two Factor Authentication Enabled',
        sub_title: 'Email Authentication',
        body: 'Your account now is protected with two factor authentication',
        ok_text: 'OK',
        close_text: 'CLOSE'
      };
      this.applicationEvent.fireAlert(alert);
      this.showEmailDisable = true;
    } catch (error) {
      this.submitted = false;
      this.hasErrors = true;
      this.emailForm.setValue({code: ''});
      this.httpError = error.error.error;
    }
  }

  public async uploadFile() {
    const uploadDocument: UploadDocument = this.uploadForm.value;
    try {
      const updatedDocument = await this.profileService.uploadDocument(uploadDocument);
      const alert: Alert = {
        title: 'File saved',
        sub_title: 'Request completed',
        body: 'Your file has been uploded successfuly.',
        ok_text: 'OK',
        close_text: 'CLOSE'
      };
      this.applicationEvent.fireAlert(alert);
    } catch (error) {
      this.httpError = error;
      this.hasErrors = true;
    }
  }

  private initBinding() {
    (<any>$)('.toggle-password').click(function() {
      $(this).toggleClass('fa-eye fa-eye-slash');
      const input = (<any>$)((<any>$)(this).attr('toggle'));
      if (input.attr('type') === 'password') {
        input.attr('type', 'text');
      } else {
        input.attr('type', 'password');
      }
    });
  }

  public mediaHasBeenUploaded(event: { file: any; response: MediaUploadResponse}) {
    this.documentsUploaded = true;
    this.showUploadDocuments = false;
    this.uploadedDocuments.push(event.response.document);
    this.KYCDocuments.push(event.response.document);
  }

  public uploadMoreDocuments() {
    this.documentsUploaded = false;
    this.showUploadDocuments = true;
  }

  public async submitKyc() {
    try {
      const response = await this.profileService.submitKYCDocuments(this.uploadedDocuments);
      this.submittedKyc = true;
      const alert: Alert = {
        title: 'Documents pending for review',
        sub_title: 'KYC',
        body: 'Thank you for submitting your documents, Your KYC process is under review now, we will update you as soon as the process is completed.',
        ok_text: 'OK',
        close_text: 'CLOSE'
      };
      this.applicationEvent.fireAlert(alert);
    } catch(error) {
      alert(error);
    }
  }

  public getDocumentType() {
    return this.uploadForm.controls['document_type'].value;
  }

  public async fetchKYCDocuments() {
    try {
      const clientKYC: ClientKYCDocuments = await this.clientsService.fetchClientKYCDocumentsAsync(
        this.client.profile.id
      );
      this.KYCDocuments = clientKYC.documents;
    } catch (error) {
      console.log(error);
    }
  }

  public closeModal() {
    (<any>$('#announcement')).modal('hide');
  }

  public openDocumentUploadModal() {
    this.uploadForm.controls['document_type'].setValue(null);
    this.hideDropZone = true;
    jQuery("#upload_file").modal('show');
  }
}
