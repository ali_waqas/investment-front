import { Deposit, DepositData } from '../deposits/deposit.model';
import {
  Withdrawal,
  WithdrawalRequestsData
} from '../withdrawals/withdrawal.model';
import { Referral } from '../referrals/referral.model';
import { DateTime } from 'luxon';

export interface Overview {
  client_id: number;
  left_referrals: number;
  right_referrals: number;
  total_deposits: number;
  total_withdrawals: number;
  referral_income: number;
  balance: number;
  total_users: number;
  ib_wallet: number;
  roi_wallet: number;
  etrade_wallet: number;
}

export interface ShortProfile {
  id: number;
  first_name: string;
  middle_name: string;
  last_name: string;
  user_name: string;
  email: string;
  phone: string;
  address: string;
  city: string;
  country: string;
  unkow: string;
}

export interface SendEmail {
  to: string;
  cc: string;
  bcc: string;
  email_body: string;
}

export interface Profile {
  id: number;
  type: string;
  first_name: string;
  middle_name: string;
  last_name: string;
  user_name: string;
  password: string;
  confirm_password: string;
  email: string;
  phone: string;
  mobile: string;
  address: string;
  city: string;
  zip: string;
  secret_2fa: string;
  type_of_authentication: TypeOfAuthentication;
  badge?: Badge;
  referrals_activation: boolean;
  referrals_issued: string;
  referrals_joined: string;
  referrals_expired: string;
  left_joined_referrals: string;
  right_joined_referrals: string;
  country: string;
  package_id: number;
  balance: number;
  roi_wallet: number;
  binary_wallet: number;
  is_signup_fee_paid: string;
  total_deposits: number;
  total_withdrawals: number;
  last_deposit_amount: number;
  last_withdrawal_amount: number;
  network_id: string;
  referral_id: number;
  belongd_to: number;
  last_deposit_on: string;
  last_withdrawal_on: string;
  referrals: Referral[];
  deposits: Deposit[];
  withdrawals: Withdrawal[];
  network: Peer[];
  overview: Overview;
  roi_rate: number;
  daily_roi: number;
  is_daily_roi_enabled: boolean;
  daily_roi_credited_on: string;
  daily_roi_last_income: number;
  kyc_verified: number;
  kyc_submitted: number;
  subscription_started_at: Date;
  award_amount: number;
  reward_amount: number;
  incomes: {
    left_balance: number;
    right_balance: number;
    right_balanced_income: number;
    left_balanced_income: number;
    id_nick: string;
    downline: number;
    deposits: number;
  };
}

export enum TypeOfAuthentication {
  email = 'email',
  sms = 'sms',
  google_authenticator = 'Google Authenticator',
  none = 'none'
}

export enum Badge{
  awarded = 'Awarded',
  rewarded = 'Rewarded',
  all = 'All'
}

export interface TwoFAQRCodeResponse {
  key: string;
  qr_code: string;
}

export interface TwoFAEmailCodeResponse {
  message: string;
  code?: string;
}

export interface ClientEntity {
  id: number;
  type: string;
  first_name: string;
  middle_name: string;
  last_name: string;
  user_name: string;
  password: string;
  confirm_password: string;
  password_confirmation: string;
  email: string;
  phone: string;
  mobile: string;
  address: string;
  city: string;
  zip: string;
  referrals_activation: boolean;
  referrals_issued: string;
  referrals_joined: string;
  referrals_expired: string;
  left_joined_referrals: string;
  right_joined_referrals: string;
  country: string;
  balance: number;
  roi_wallet: number;
  binary_wallet: number;
  award_amount: number;
  reward_amount: number;
  is_signup_fee_paid: string;
  total_deposits: number;
  total_withdrawals: number;
  last_deposit_amount: number;
  last_withdrawal_amount: number;
  network_id: string;
  referral_id: number;
  belongd_to: number;
  last_deposit_on: string;
  last_withdrawal_on: string;
  referrals: Referral[];
  deposits: Deposit[];
  withdrawals: Withdrawal[];
  network: Peer[];
  overview: Overview;
  roi_rate: number;
  daily_roi: number;
  kyc_verified?: number;
  is_daily_roi_enabled: boolean;
  daily_roi_credited_on: string;
  daily_roi_last_income: number;
  referral_income: number;
  profit_from_last_balanced_node: number;
  created_at: Date;
  left_balance: number;
  right_balance: number;
}

export interface Client {
  profile: Profile;
  is_signup_failed?: boolean;
}

export interface AcademyAccountResponse {
  account: Profile;
  is_account_exists?: boolean;
}

export interface ClientResponse {
  clients?: ClientPager;
  message?: string;
}

export interface ClientsFilter {
  type?: string;
  method_id?: string;
  client_id?: number;
  amount?: number;
  bonus?: number;
  total?: number;
  details?: string;
  page?: number;
  per_page?: number;
  sort?: string;
  sort_by?: string;
}

export interface ClientSummaryResponse {
  summary: ClientSummary;
}

export interface ClientSummary {
  profile: Profile;
  referrals: ReferralPager;
  deposits: DepositPager;
  withdrawals: WithdrawalRequestsPager;
}

export interface ReferralPager {
  current_page: number;
  data: Referral[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  next_page_url: string;
  path: string;
  per_page: number;
  prev_page_url?: any;
  to: number;
  total: number;
}

export interface DepositPager {
  current_page: number;
  data: DepositData[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  next_page_url: string;
  path: string;
  per_page: number;
  prev_page_url?: any;
  to: number;
  total: number;
}

export interface WithdrawalRequestsPager {
  current_page: number;
  data: WithdrawalRequestsData[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  next_page_url: string;
  path: string;
  per_page: number;
  prev_page_url?: any;
  to: number;
  total: number;
}

export interface ClientPager {
  current_page: number;
  data: ClientEntity[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  next_page_url: string;
  path: string;
  per_page: number;
  prev_page_url?: any;
  to: number;
  total: number;
}

export interface Peer {
  id: number;
  direction: string;
  client_id: number;
  first_name: string;
  last_name: string;
  email: string;
  mobile: string;
  balance: number;
  total_deposits: number;
  total_withdrawals: number;
  network: Peer[];
  created_at: string;
}

export interface Registration {
  package_id: number;
  first_name: string;
  last_name: string;
  password: string;
  password_confirmation: string;
  email: string;
  referral: string;
}

export interface ReferralIncome {
  client_id: number;
  first_name: string;
  last_name: string;
  depositor_id: number;
  amount_deposited: number;
  commission_percentage: number;
  income: number;
  direction: string;
  created_at: string;
  updated_at: string;
}

export interface BinaryIncome {
  client_id: number;
  right_balance: number;
  left_balance: number;
  balanced_amount: number;
  percentage: number;
  income: number;
  date: string;
  created_at: string;
  updated_at: string;
}

export interface TestObject {
  first: 1;
  second: 2;
}

export interface UploadDocument {
  id: number;
  client_id: number;
  document_type: string;
  document_file: string;
}

export interface ClientKYCDocuments {
  documents: Document[];
}

export interface Document {
  client_id: number;
  client?: Profile;
  document_file: string;
  document_type: string;
  created_at: DateTime;
  updated_at: DateTime;
}
