import { Injectable } from '@angular/core';
import { BaseAPIClass } from '@app/core/class/baseAPI.class';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { Referral } from './referral.model';

@Injectable({providedIn: 'root'})
export class ReferralsService extends BaseAPIClass {
  private readonly URL_REFERRAL: string = 'referral';
  private readonly URL_GET_REFERRAL: string = 'referral';

  constructor(
    protected httpClient: HttpClient,
    protected authenticationService: AuthenticationService) {
    super(httpClient, authenticationService);
  }

  public async createReferralAsync(direction: string, referral_type: string) {
    const response: any = await this.postAsync(this.URL_REFERRAL, { 'direction': direction, 'referral_type': referral_type }).toPromise();
    return response.referral;
  }

  public async getReferralsAsync() {
    const response: any = await this.postAsync(this.URL_GET_REFERRAL).toPromise();
    return response.referrals;
  }
}
