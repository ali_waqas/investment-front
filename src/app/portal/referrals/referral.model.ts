export interface Referral {
  id: number;
  client_id: number;
  referral: string;
  direction: string;
  referral_type: string;
  status: string;
  created_at: Date;
}
