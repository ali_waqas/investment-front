import { AfterViewInit, APP_BOOTSTRAP_LISTENER, Component, Input, OnInit } from '@angular/core';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { ProfileService } from '../profile/profile.service';

// import * as $ from 'jquery';
import 'jquery-knob';
declare var $: any;

@Component({
  selector: 'app-awards',
  templateUrl: './awards.component.html',
  styleUrls: ['./awards.component.scss']
})
export class AwardsComponent extends AbstractBaseComponent implements OnInit, AfterViewInit {

    public subscriptionDate: Date = null;
    public withdrawDate_1: Date;
    public withdrawDate_2: Date;
    public withdrawDate_3: Date;

    public daysToAdd_1 = 60;
    public daysToAdd_2 = 180;
    public daysToAdd_3 = 365;
    public showModal = true;


    public imagePath = [
        {
            path: '/assets/images/10.jpg'
        },
        {
            path: '/assets/images/20.jpg'
        },
        {
            path: '/assets/images/30.jpg'
        },
        {
            path: '/assets/images/40.jpg'
        },
        {
            path: '/assets/images/50.jpg'
        }
    ];


    constructor(protected profileService: ProfileService) {
        super(profileService);
    }

    async ngOnInit() {
        this.client = await this.profileService.getProfileAsync();
        this.pageTitle = 'Rewards';
        this.pageIcon = 'fal fa-trophy-alt';
    }
    private getTimeLeft(no: number) {
        switch(no) {
            case 1:
                return this.withdrawDate_1.getTime() - new Date().getTime();
                case 2:
                    return this.withdrawDate_2.getTime() - new Date().getTime();
                    case 3:
                        return this.withdrawDate_3.getTime() - new Date().getTime();
                    }
                }
                
    ngAfterViewInit() {
        $('#modal').modal('show');
        $('.mobile-nav-on').removeClass();
    }

    hideModal() {
        $('#modal').modal('hide');
    }

    private setCounterData(elementNo: number, milliseconds: number) {
        var days: any = Math.floor(milliseconds / (3600 * 24));
        var hours: any = Math.floor((milliseconds % (60 * 60 * 24)) / 3600);
        var minutes: any = Math.floor((milliseconds % (60 * 60)) / 60);
        var seconds: any = Math.floor(milliseconds % 60);


        $('.reward_'+elementNo+'_daysDial').val(days).trigger('change');
        $('.reward_'+elementNo+'_hoursDial').val(hours).trigger('change');
        $('.reward_'+elementNo+'_minutesDial').val(minutes).trigger('change');
        $('.reward_'+elementNo+'_secondsDial').val(seconds).trigger('change');
    }


    private initJquery() {
        if (window.matchMedia('(max-width: 408px)').matches) {
        (<any>$)('.dial').knob({
            width: 30,
            height: 30,
            readOnly: true
        });
        } else {
        (<any>$)('.dial').knob({
            width: 40,
            height: 40,
            readOnly: true
        });
        }
    }

    private addDays(oldDate: string, days: number) {
        const startDate = new Date(oldDate);
        return new Date(startDate.setDate(startDate.getDate() + days));
    }
}