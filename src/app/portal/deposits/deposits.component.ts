import { Component, OnInit } from '@angular/core';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { ProfileService } from '../profile/profile.service';
import { FormBuilder, FormGroup, Validators, ValidationErrors } from '@angular/forms';
import { DepositsService } from './deposits.service';
import { Deposit, WireTransfer, PerfectMoneyRequest, Product, CryptoDepositRequest } from './deposit.model';
import { SettingsService } from '@app/shared/services/settings-service';
import { LocalStorageService } from '@app/core';
import { PackagesService } from '@app/frontend/pakcages/packages.service';
import { Package } from '@app/frontend/pakcages/package.model';
import { Alert } from '@app/shared/models/alert.model';
import { ApplicationEvent } from '@app/shared/services/alert-modal.service';

@Component({
  selector: 'app-deposit',
  templateUrl: './deposits.component.html',
  styleUrls: ['./deposits.component.css']
})
export class DepositsComponent extends AbstractBaseComponent implements OnInit {
  public depositForm: FormGroup;
  public cryptoDepositForm: FormGroup;
  public perfectMoneyForm: FormGroup;
  public wireTransferForm: FormGroup;
  public perfectMoneyRequest: PerfectMoneyRequest;
  public package: Package;
  public requestInProgress = false;
  constructor(
    protected profileService: ProfileService,
    protected depositService: DepositsService,
    private formBuilder: FormBuilder,
    private storageService: LocalStorageService,
    private packagesService: PackagesService,
    private applicationEvent: ApplicationEvent,
    private settingsService: SettingsService) {
    super(profileService);
  }

  async ngOnInit() {
    this.pageTitle = 'Deposit';
    this.pageIcon = 'fal fa-dollar-sign';
    this.client = await this.profileService.getProfileAsync();
    const selectedPackageId: number = await Number(this.storageService.getItem('portal-package-id') || 1);
    this.package = this.packagesService.getPackageById(selectedPackageId);
    this.createDepositForm();
    this.createWireTransferForm();
    this.createPerfectMoneyForm();
    this.createCryptoDepositForm();
    this.ready = true;
  }

  public async depositNowAsync() {
    try {
      const depositRequest: Deposit = this.depositForm.value;
      depositRequest.method_id = '1';
      const deposit = await this.depositService.createDepositAsync(depositRequest);
      this.client = await this.profileService.getProfileAsync(true);
      alert('Deposit has been saved successfully.');
    } catch (error) {
      this.httpError = error;
      this.hasErrors = true;
    }
  }

  public async createPerfectMoneyRequest() {
    try {
      const failedUrl = await this.settingsService.get('perfect_money_failed_api_url');
      const successUrl = await this.settingsService.get('perfect_money_success_api_url');
      const perfectMoneyRequest: PerfectMoneyRequest = this.perfectMoneyForm.value;
      perfectMoneyRequest.payment_units = 'USD';
      perfectMoneyRequest.is_subscription_fee = 'No';
      perfectMoneyRequest.product = Product.INVESTMENT_PLATFORM;
      const request: any = await this.depositService.createPerfectMoneyRequest(perfectMoneyRequest);
      this.perfectMoneyRequest = request.perfect_money_request;
      (<any>$('#form_payment_id')).val(this.perfectMoneyRequest.payment_id);
      (<any>$('#form_payment_amount')).val(this.perfectMoneyRequest.payment_amount);
      (<any>$('#payment_url')).val(successUrl.value);
      (<any>$('#no_payment_url')).val(failedUrl.value);
      (<any>$('#perfectmoney_step_1')).submit();
    } catch (error) {
      this.httpError = error;
      this.hasErrors = true;
    }
  }

  public async createCryptoDepositRequest() {
    try {
        this.requestInProgress = true;
        const cryptoDepositRequest: CryptoDepositRequest = this.cryptoDepositForm.value;
        cryptoDepositRequest.email = await (await this.profileService.getProfileAsync()).profile.email;
        const request: any = await this.depositService.createCryptoDepositRequestForLoggedInUser(cryptoDepositRequest);
        this.cryptoDepositForm.reset();
        const alert: Alert = {
            title: 'Deposit Request Created',
            sub_title: 'Transaction Saved',
            body: 'Your crypto deposit request has been saved. Once we receive your funds, your account will be deposited with this amount',
            ok_text: 'OK',
            close_text: 'CLOSE'
        };
        this.applicationEvent.fireAlert(alert);
        this.requestInProgress = false;
      } catch (error) {
        this.httpError = error;
        this.hasErrors = true;
      }
  }

  public async saveWireDetailsAsync() {
    try {
      const wireTransferRequest: WireTransfer = this.wireTransferForm.value;
      const wireTransfer = await this.depositService.createWireTransferAsync(wireTransferRequest);
      this.client = await this.profileService.getProfileAsync(true);
      alert('Wire Details have been saved successfully. Your account will be credited once the transaction is confirmed.');
    } catch (error) {
      this.httpError = error;
      this.hasErrors = true;
    }
  }

  public createDepositForm() {
    this.depositForm = this.formBuilder.group({
      first_name: [this.client.profile.first_name, [Validators.required]],
      last_name: [this.client.profile.last_name, [Validators.required]],
      email: [this.client.profile.email, [Validators.required]],
      amount: [this.package.min_amount, [Validators.required, Validators.min(this.package.min_amount)]],
      card_number: ['', [Validators.required]],
      expiry: ['', [Validators.required]],
    });
  }

  public createCryptoDepositForm() {
    this.cryptoDepositForm = this.formBuilder.group({
      transaction_id: ['', [Validators.required]],
      transaction_date: ['', [Validators.required]],
      amount: [this.package.min_amount, [Validators.required]],
      status: ['PENDING', [Validators.required]],
    });
  }

  public async createPerfectMoneyForm() {
     this.perfectMoneyForm = this.formBuilder.group({
      first_name: [this.client.profile.first_name, [Validators.required]],
      last_name: [this.client.profile.last_name, [Validators.required]],
      email: [this.client.profile.email, [Validators.required]],
      payment_amount: [this.package.min_amount, [Validators.required, Validators.min(this.package.min_amount)]]
    });
  }

  public createWireTransferForm() {
    this.wireTransferForm = this.formBuilder.group({
      bank: ['', [Validators.required]],
      country: ['', [Validators.required]],
      city: ['', [Validators.required]],
      wire_date: ['', [Validators.required]],
      amount: [this.package.min_amount, [Validators.required, Validators.min(this.package.min_amount)]]
    });
  }

  getFormValidationErrors() {
    Object.keys(this.depositForm.controls).forEach(key => {

    const controlErrors: ValidationErrors = this.depositForm.get(key).errors;
    if (controlErrors != null) {
          Object.keys(controlErrors).forEach(keyError => {
            console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
          });
        }
      });
    }
}
