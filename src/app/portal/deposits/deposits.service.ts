import { Injectable } from '@angular/core';
import { BaseAPIClass } from '@app/core/class/baseAPI.class';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { Deposit, WireTransfer, PerfectMoneyRequest, CryptoDepositRequest, DepositsFilter, DepositsResponse, DAIDepositRequest } from './deposit.model';


@Injectable({providedIn: 'root'})
export class DepositsService extends BaseAPIClass {
  private readonly URL_CREATE_DEPOSIT: string = 'client/deposit';
  private readonly URL_CREATE_WIRETRANSFER: string = 'client/wiretransfer';
  private readonly URL_CREATE_OPEN_CRYPTO_DEPOSIT_REQUEST: string = 'open/client/crypto_deposit';
  private readonly URL_CREATE_OPEN_DAI_DEPOSIT_REQUEST: string = 'open/client/dai_deposit';
  private readonly URL_CREATE_CRYPTO_DEPOSIT_REQUEST: string = 'client/crypto_deposit';
  private readonly URL_INVESTMENTS: string = 'client/investments';
  private readonly URL_PERFECT_MONEY_REQUEST: string = 'client/deposit/perfect_money/request';
  private readonly URL_OPEN_PERFECT_MONEY_REQUEST: string = 'open/client/perfect_money_deposit';
  private readonly URL_PERFECT_MONEY_REQUEST_FETCH: string = 'client/deposit/perfect_money/fetch';
  private readonly URL_FETCH_DEPOSITS: string = 'deposits';

  constructor(
    protected httpClient: HttpClient,
    protected authenticationService: AuthenticationService) {
    super(httpClient, authenticationService);
  }

  public async createDepositAsync(depositRequest: Deposit) {
    return await this.postAsync<Deposit>(this.URL_CREATE_DEPOSIT, depositRequest).toPromise();
  }

  public async createWireTransferAsync(wireTransferRequest: WireTransfer) {
    return await this.postAsync<WireTransfer>(this.URL_CREATE_WIRETRANSFER, wireTransferRequest).toPromise();
  }

  public async getInvestmentsAsync() {
    return await this.postAsync(this.URL_INVESTMENTS).toPromise();
  }

  public async createPerfectMoneyRequest(perfectMoneyRequest: PerfectMoneyRequest) {
    const registeration = await this.postAsync<PerfectMoneyRequest>(this.URL_PERFECT_MONEY_REQUEST, perfectMoneyRequest).toPromise();
    return registeration;
  }

  public async createOpenPerfectMoneyRequest(perfectMoneyRequest: PerfectMoneyRequest) {
    const registeration = await this.postAsync<PerfectMoneyRequest>(this.URL_OPEN_PERFECT_MONEY_REQUEST, perfectMoneyRequest).toPromise();
    return registeration;
  }

  public async createCryptoDepositRequest(cryptoDepositRequest: CryptoDepositRequest) {
    return await this.postAsync<PerfectMoneyRequest>(this.URL_CREATE_OPEN_CRYPTO_DEPOSIT_REQUEST, cryptoDepositRequest).toPromise();
  }

  public async createDAIDepositRequest(daiDepositRequest: DAIDepositRequest) {
    return await this.postAsync<PerfectMoneyRequest>(this.URL_CREATE_OPEN_DAI_DEPOSIT_REQUEST, daiDepositRequest).toPromise();
  }

  public async createCryptoDepositRequestForLoggedInUser(cryptoDepositRequest: CryptoDepositRequest) {
    return await this.postAsync<PerfectMoneyRequest>(this.URL_CREATE_CRYPTO_DEPOSIT_REQUEST, cryptoDepositRequest).toPromise();
  }


  public async getPerfectMoneyRequestAsync(perfectMoneyRequest: any) {
    return await this.postAsync<PerfectMoneyRequest>(this.URL_PERFECT_MONEY_REQUEST_FETCH, perfectMoneyRequest).toPromise();
  }

  public async fetchDepositsAsync(filters: DepositsFilter) {
    return await this.getAsync<DepositsResponse>(this.URL_FETCH_DEPOSITS, filters, {}).toPromise();
  }
}
