import { Profile } from "../profile/profile.model";

export interface ManualDeposit {
    first_name: string;
    last_name: string;
    email: string;
    client_id: number;
    method: string;
    amount: number;
}

export interface Deposit {
  first_name: string;
  last_name: string;
  email: string;
  client_id: number;
  method_id: string;
  amount: number;
  bonus: number;
  total: number;
  created_at: string;
  updated_at: string;
}

export interface DepositsResponse {
  deposits?: DepositsPager;
  message?: string;
}

export interface DepositData {
  client_id: number;
  client: Profile;
  method_id: string;
  details: string;
  amount: number;
  bonus: number;
  total: number;
  type: string;
  created_at: string;
  updated_at: string;
}

export interface DepositsPager {
  current_page: number;
  data: DepositData[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  next_page_url: string;
  path: string;
  per_page: number;
  prev_page_url?: any;
  to: number;
  total: number;
}

export interface DepositsFilter {
  type?: string;
  method_id?: string;
  client_id?: number;
  amount?: number;
  bonus?: number;
  total?: number;
  details?: string;
  page?: number;
  per_page?: number;
  sort?: string;
  sort_by?: string;
}


export interface WireTransfer {
  id: number;
  client_id: number;
  bank: string;
  city: string;
  country: string;
  wire_date: string;
  amount: string;
  created_at: string;
  updated_at: string;
}

export interface CryptoDepositRequest {
    id?: number;
    client_id: number;
    amount: number;
    transaction_id: string;
    transaction_date: Date;
    status: string;
    email?: string;
    created_at: string;
    updated_at: string;
  }

  export interface DAIDepositRequest {
    id?: number;
    client_id: number;
    amount: number;
    transaction_id: string;
    transaction_date: Date;
    status: string;
    email?: string;
    created_at: string;
    updated_at: string;
  }


export interface Investment {
  id: number;
  client_id: number;
  method_id: string;
  amount: number;
  bonus: number;
  total: number;
  date: string;
  created_at: string;
  updated_at: string;
}

export interface PerfectMoneyRequest {
  id?: number;
  token?: string;
  product: Product;
  client_id: number;
  payee_account: string;
  payee_name: string;
  payment_id: string;
  payment_amount: number;
  payment_units: string;
  status_url?: string;
  payment_url?: string;
  is_subscription_fee: string;
  payment_url_method: string;
  nopayment_url?: string;
  nopayment_url_method?: string;
  suggested_memo?: string;
  created_at?: string;
  updated_at?: string;
}


export enum Product {
    INVESTMENT_PLATFORM,
    BINARY_TRADE,
    SUBSCRIPTION_FEE_1
}
