import { Injectable } from '@angular/core';
import { BaseAPIClass } from '@app/core/class/baseAPI.class';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { ClientDailyRoi, Transaction } from './transaction.model';

@Injectable({providedIn: 'root'})
export class TransactionsService extends BaseAPIClass {
  private readonly URL_DAILY_ROI: string = 'client/income/daily_roi';
  private readonly URL_TRANSACTIONS: string = 'client/transactions';

  constructor(
    protected httpClient: HttpClient,
    protected authenticationService: AuthenticationService) {
    super(httpClient, authenticationService);
  }

  public async getDailyRoi() {
    return await this.postAsync<ClientDailyRoi>(this.URL_DAILY_ROI).toPromise();
  }

  public async fetchTransactions() {
    return await this.postAsync<Transaction>(this.URL_TRANSACTIONS).toPromise();
  }
}
