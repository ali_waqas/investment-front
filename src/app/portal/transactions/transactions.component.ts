import { Component, OnInit } from '@angular/core';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { ProfileService } from '../profile/profile.service';
import { TransactionsService } from './transactions.service';
import { ClientDailyRoi, Transaction } from './transaction.model';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.css']
})
export class TransactionsComponent extends AbstractBaseComponent implements OnInit {
  public transactions: Transaction[] = [];
  constructor(profileService: ProfileService,
              private transactionsService: TransactionsService) {
    super(profileService);
  }

  async ngOnInit() {
    this.pageTitle = 'Transactions';
    this.pageIcon = 'fal fa-exchange-alt';
    this.loadingStart();
    const response: any = await this.transactionsService.fetchTransactions();
    this.transactions = response.transactions;
    this.loadingFinished();
  }

  public typeClass(transaction: Transaction) {
    switch (transaction.type) {
        case 'Investment':
            return 'badge badge-success';
        break;
        case 'Subscription Fee':
            return 'badge badge-danger';
        break;
        case 'Daily Income':
            return 'badge badge-info';
        break;
        case 'Referral':
            return 'badge badge-warning';
        break;
    }
  }
}
