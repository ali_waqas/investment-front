export interface ClientDailyRoi {
  id: number;
  client_id: number;
  roi_rate: number;
  days: number;
  income: number;
  created_at: string;
  updated_at: string;
}

export interface Transaction {
  id: number;
  client_id: number;
  amount: number;
  type: string;
  date: string;
  created_at: string;
  updated_at: string;
}
