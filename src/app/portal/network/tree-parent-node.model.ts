export interface Node2 {
  id: string;
  id_nick: string;
  deposits: string;
  downline: number;
  left_balance: number;
  right_balance: number;
  left_balanced_income: string;
  right_balanced_income: string;
}

export interface Node3 {
  id: string;
  id_nick: string;
  deposits: string;
  downline: number;
  left_balance: number;
  right_balance: number;
  left_balanced_income: string;
  right_balanced_income: string;
}

export interface Text {
  name: string;
  deposits: string;
  downline: string;
  left_balance: string;
  right_balance: string;
}

export interface Left {
  node: Node3;
  text: Text;
  HTMLclass: string;
  downline: number;
}

export interface Node4 {
  id: string;
  id_nick: string;
  deposits: string;
  downline: number;
  left_balance: number;
  right_balance: number;
  left_balanced_income: string;
  right_balanced_income: string;
}

export interface Text2 {
  name: string;
  deposits: string;
  downline: string;
  left_balance: string;
  right_balance: string;
}

export interface Right {
  node: Node4;
  text: Text2;
  HTMLclass: string;
  downline: number;
}

export interface Node {
  node: Node2;
  left: Left;
  right: Right;
}

export interface TreeParentNode {
  node: Node;
}
