import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../profile/profile.service';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { Peer, Profile } from '../profile/profile.model';
import { CurrencyPipe } from '@angular/common';
import { DynamicScriptLoaderService } from '@app/shared/services/dynamic-script-loader.service';
import { TreeParentNode } from './tree-parent-node.model';
declare const Treant: any;

@Component({
  selector: 'app-network',
  templateUrl: './network.component.html',
  styleUrls: ['./network.component.scss']
})
export class NetworkComponent extends AbstractBaseComponent implements OnInit {
    public network: Peer[];
    public loading = false;
    private tree: any;
    public profile: Profile;
    private chart_config: any;
    constructor(profileService: ProfileService,
        private currency: CurrencyPipe,
        private dynamicScriptLoaderService: DynamicScriptLoaderService) {
      super(profileService);

    }

    async ngOnInit() {
      const scriptsToLoad: string[] = [
        'treant',
        'raphael'
        ];
        this.dynamicScriptLoaderService.load(scriptsToLoad).then(async(scripts) => {
        this.loading = true;
        const client = await this.profileService.getProfileAsync();
        const response: TreeParentNode = await this.profileService.fetchGenealogyTreeNode(client.profile.id);
        this.tree = [response.node.left, response.node.right];
        this.pageIcon = 'fal fa-sitemap';
        this.pageTitle = 'Genealogy';
        this.chart_config = {
            chart: {
                container: '#network-tree'
            },
            stackChildren: false,
            nodeStructure: {
                text: {
                    name: `OPL-${response.node.node.id}`,
                    // balance: `Cap: $${client.profile.balance}`,
                    total: `Total: $${response.node.node.left_balance + response.node.node.right_balance + Number(response.node.node.right_balanced_income) + Number(response.node.node.left_balanced_income)}`,
                    left: `Left: $${response.node.node.left_balance}`,
                    right: `Right: $${response.node.node.right_balance}`,
                },
                HTMLclass: 'parent-node',
                children:  this.tree
            }
        };
        this.paintTree();
        this.loading = false;
      });
  }

  private paintTree() {
    new Treant(this.chart_config);
    this.runBindings();
  }


  private runBindings() {
    const this_ = this;
    $(".node").on("click", async function() {
      const nodeName = $(this).find('.node-name').html();
      const nodeId = Number(nodeName.split("OPL-")[1]);
      const response: any = await this_.profileService.fetchGenealogyTreeNode(nodeId);
      if(response != null) {
        if(("node" in response) && ("left" in response.node) && response.node.left != "") {
          this_.addNodeToTree(this_.tree, response.node.left, nodeId);
        }
        if(("node" in response) && ("right" in response.node) && response.node.right != "") {
          this_.addNodeToTree(this_.tree, response.node.right, nodeId);
        }
        this_.paintTree();
      }
    });
  }

  private addNodeToTree(parentNode: any, node: any, nodeToCatch: number) {
    let shouldExit = false;
    while(!shouldExit) {
      if(parentNode == null) {
        shouldExit = true;
      } else {
          if(parentNode[0] != "" && parentNode[0] != undefined && ("node" in parentNode[0]) && parentNode[0].node.id == nodeToCatch) {
            if(parentNode[0]["children"] == null) {
              parentNode[0]["children"] = [];
              parentNode[0]["children"][0] = node;
            } else {
              if(parentNode[0]["children"][0] != null && parentNode[0]["children"][0].node.id != node.node.id) {
                parentNode[0]["children"][1] = node;
              }
            }
            shouldExit = true;
          } else if(parentNode[1] != "" && parentNode[1] != undefined && ("node" in parentNode[1]) && parentNode[1].node.id == nodeToCatch) {
            if(parentNode[1]["children"] == null) {
              parentNode[1]["children"] = [];
              parentNode[1]["children"][0] = node;
            } else {
              if(parentNode[1]["children"][0] != null && parentNode[1]["children"][0].node.id != node.node.id) {
                parentNode[1]["children"][1] = node;
              }
            }
            shouldExit = true;
          } else {
            if(parentNode[0] !="" && parentNode[0] != "undefined" && parentNode[0] != null && ("children" in parentNode[0]) && parentNode[0].children !=null) {
              this.addNodeToTree(parentNode[0].children, node, nodeToCatch);
            }
            if(parentNode[1] !="" && parentNode[1] != "undefined" && parentNode[1] != null && ("children" in parentNode[1]) && parentNode[1].children != null) {
              this.addNodeToTree(parentNode[1].children, node, nodeToCatch);
            }
            shouldExit = true;
          }
      }
    }
  }
}

