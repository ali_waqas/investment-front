import { Profile } from "../profile/profile.model";

export interface Commission {
    id: number;
    client_id: string;
    client: Profile;
    commission_amount: string;
    amount_withdrew: string;
    percentage: string;
    note: string;
    created_at?: any;
    updated_at?: any;
}

export interface CommissionResponse {
    commissions: Commission[];
}

