import { Component, OnInit } from '@angular/core';
import { ClientEntity } from '../profile/profile.model';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { ProfileService } from '../profile/profile.service';
import { Commission, CommissionResponse } from './commission.model';
import { CommissionsService } from './commissions.service';

@Component({
  selector: 'app-commissions',
  templateUrl: './commissions.component.html',
  styleUrls: ['./commissions.component.css']
})
export class CommissionsComponent extends AbstractBaseComponent implements OnInit {
    public commissions: Commission[];
    constructor(private commissionService: CommissionsService,
        protected profileService: ProfileService) {
        super(profileService);
        this.dataTableName = 'dt-commissions';
        this.pageTitle = 'Commissions';
        this.pageIcon = 'fal fa-users';
    }

    async ngOnInit() {
        this.listCommissions();
    }

    private async listCommissions() {
        this.loadingStart();
        const response: CommissionResponse = await this.commissionService.fetchCommissionsAsync();
        this.commissions = response.commissions;
        this.addTableOption('responsive', true);
        this.addTableOption('pageLength', 50);
        this.addDataTableButtons();
        this.addTableOption('bFilter', false);
        this.loadingFinished();
    }
}
