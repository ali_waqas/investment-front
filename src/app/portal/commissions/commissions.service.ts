import { Injectable } from '@angular/core';
import { BaseAPIClass } from '@app/core/class/baseAPI.class';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { CommissionResponse } from './commission.model';

@Injectable({providedIn: 'root'})
export class CommissionsService extends BaseAPIClass {
  private readonly URL_FETCH_COMMISSION: string = 'client/commissions';

  constructor(
    protected httpClient: HttpClient,
    protected authenticationService: AuthenticationService) {
    super(httpClient, authenticationService);
  }

  public async fetchCommissionsAsync() {
    return await this.postAsync<CommissionResponse>(this.URL_FETCH_COMMISSION).toPromise();
  }
}
