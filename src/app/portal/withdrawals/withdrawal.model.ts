import { Profile } from "../profile/profile.model";

export interface Withdrawal {
    id: number;
    first_name: string;
    last_name: string;
    email: string;
    client_id: number;
    method_id: number;
    method: string;
    status: string;
    amount: number;
    bonus: number;
    total: number;
    date: string;
    wallet_address?: string;
    wallet?: string;
    created_at: string;
    updated_at: string;
}

export interface WithdrawalResponse {
  withdrawals?: WithdrawalPager;
  message?: string;
}

export interface WithdrawalPager {
  current_page: number;
  data: Withdrawal[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  next_page_url: string;
  path: string;
  per_page: number;
  prev_page_url?: any;
  to: number;
  total: number;
}


export interface WithdrawalsFilters {
  id?: number;
  first_name?: string;
  last_name?: string;
  email?: string;
  client_id?: number;
  method_id?: number;
  method?: string;
  status?: string;
  amount?: number;
  bonus?: number;
  total?: number;
  date?: string;
  wallet_address?: string;
  created_at?: string;
  updated_at?: string;
  page?: number;
  per_page?: number;
  sort?: string;
  sort_by?: string;
}

export interface WithdrawalRequestsResponse {
  withdrawal_requests?: WithdrawalRequestsPager;
  message?: string;
}

export interface WithdrawalRequestsData {
  client_id: number;
  client: Profile;
  method: string;
  fee: number;
  amount: number;
  status: string;
  request_token: string;
  created_at: string;
  updated_at: string;
}

export interface WithdrawalRequestsPager {
  current_page: number;
  data: WithdrawalRequestsData[];
  first_page_url: string;
  from: number;
  last_page: number;
  last_page_url: string;
  next_page_url: string;
  path: string;
  per_page: number;
  prev_page_url?: any;
  to: number;
  total: number;
}

export interface WithdrawalRequestsFilter {
  method?: string;
  client_id?: number;
  amount?: number;
  fee?: number;
  status?: string;
  page?: number;
  per_page?: number;
  sort?: string;
  sort_by?: string;
}



export interface WithdrawalPerfectMoney {
    id?: number;
    first_name: string;
    last_name: string;
    client_id?: number;
    perfect_money_account: string;
    amount: number;
    created_at: string;
    updated_at: string;
}

export interface WithdrawalCrypto {
    id?: number;
    first_name: string;
    last_name: string;
    client_id?: number;
    wallet_address: string;
    amount: number;
    created_at: string;
    updated_at: string;
}

export interface WithdrawalDAI {
  id?: number;
  first_name: string;
  last_name: string;
  client_id?: number;
  wallet_address: string;
  amount: number;
  created_at: string;
}

export interface WithdrawalWireTransfer {
  name: string;
  country: string;
  bank_name: string;
  bank_address: number;
  bank_country: number;
  iban: number;
  swift: number;
  amount: number;
}
