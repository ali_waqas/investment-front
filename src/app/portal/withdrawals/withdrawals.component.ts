import { Component, OnInit } from '@angular/core';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { ProfileService } from '../profile/profile.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Withdrawal, WithdrawalResponse, WithdrawalsFilters, WithdrawalWireTransfer } from './withdrawal.model';
import { WithdrawalsService } from './withdrawals.service';
import { ApplicationEvent } from '@app/shared/services/alert-modal.service';
import { Alert } from '@app/shared/models/alert.model';


@Component({
  selector: 'app-withdrawals',
  templateUrl: './withdrawals.component.html',
  styleUrls: ['./withdrawals.component.css']
})
export class WithdrawalsComponent extends AbstractBaseComponent implements OnInit {
  public withdrawalForm: FormGroup;
  public withdrawalWithWireTransferForm: FormGroup;
  public wireTransferForm: FormGroup;
  public withdrawals: Withdrawal[] = [];
  public withdrawalResponse: WithdrawalResponse;
  public withdrawalsFilter: WithdrawalsFilters  = {};
  public withdrawalFilterForm: FormGroup;
  constructor(
    protected profileService: ProfileService,
    protected withdrawalsService: WithdrawalsService,
    protected applicationEvent: ApplicationEvent,
    private formBuilder: FormBuilder) {
    super(profileService);
    }

    async ngOnInit() {
        this.pageTitle = 'Withdrawals';
        this.pageIcon = 'fal fa-credit-card-front';
        this.fetchWithdrawals();
        this.client = await this.profileService.getProfileAsync();
        this.initForm();
    }

    private async fetchWithdrawals() {
        this.loadingStart();
        const response: WithdrawalResponse = await this.withdrawalsService.fetchWithdrawals(this.withdrawalsFilter);
        this.withdrawals = response.withdrawals.data;
        this.setPages(response.withdrawals.last_page);
        this.addTableOption('responsive', true);
        this.addTableOption('pageLength', 50);
        this.addDataTableButtons();
        this.addTableOption('bFilter', false);
        this.loadingFinished();
    }

    public typeClass(withdrawal: Withdrawal) {
        switch (withdrawal.status) {
            case 'PENDING':
                return 'badge badge-warning';
            case 'FAILED':
            case 'CANCELLED':
                return 'badge badge-danger';
            case 'COMPLETED':
                return 'badge badge-success';
        }
    }

    public getWalletClass(withdrawal: Withdrawal) {
      return (withdrawal.wallet === "Binary")? 'badge badge-primary': 'badge badge-success';
    }

    public async gotoPage(page: number) {
      this.withdrawalsFilter.page = page;
      this.updatePagerLinks(page);
      this.fetchWithdrawals();
    }

    public gotoPageByDropdown(event: any) {
      this.gotoPage(event.target.value);
    }

    public applyFilers() {
      this.withdrawalsFilter = this.withdrawalFilterForm.value;
      this.fetchWithdrawals();
    }

    public clearFilters() {
      this.initFilters();
      this.fetchWithdrawals();

    }

    private initForm() {
      this.withdrawalFilterForm = this.formBuilder.group({
        client_id: ['', []]
      });
      this.ready = true;
    }

    public initFilters() {
      this.withdrawalsFilter = { page: 1, per_page: 100 };
    }

    public async processTransactionAsync(withdrawal: Withdrawal, status: string) {
        try {
            const response: any = await this.withdrawalsService.processWithdrawal(withdrawal.id, status);
            const alert: Alert = {title: 'Operation Successful',
            sub_title: 'Withdrawal Request',
            body: response.message,
            ok_text: 'OK',
            close_text: 'CLOSE'};
            this.applicationEvent.fireAlert(alert);
            this.fetchWithdrawals();
        } catch (response) {
            const alert: Alert = {title: 'Operation Failed',
            sub_title: 'Withdrawal Request',
            body: response.error.error,
            ok_text: 'OK',
            close_text: 'CLOSE'};
            this.applicationEvent.fireAlert(alert);
        }
    }

    public canBeCancelled(withdrawal: Withdrawal) {
        return withdrawal.status === 'PENDING';
    }

    public canBeCompleted(withdrawal: Withdrawal) {
        return withdrawal.status === 'PENDING';
    }
}
