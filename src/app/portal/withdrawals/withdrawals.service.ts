import { Injectable } from '@angular/core';
import { BaseAPIClass } from '@app/core/class/baseAPI.class';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { Withdrawal, WithdrawalWireTransfer, WithdrawalPerfectMoney, WithdrawalCrypto, WithdrawalRequestsFilter, WithdrawalRequestsResponse, WithdrawalResponse, WithdrawalsFilters, WithdrawalDAI } from './withdrawal.model';

@Injectable({providedIn: 'root'})
export class WithdrawalsService extends BaseAPIClass {
  private readonly URL_CREATE_WITHDRAWAL: string = 'client/withdrawal/credit_card';
  private readonly URL_CREATE_WITHDRAWAL_WIRETRANSFER: string = 'client/withdrawal/wire_transfer';
  private readonly URL_CREATE_WITHDRAWAL_PERFECTMONEY: string = 'client/withdrawal/perfect_money';
  private readonly URL_CREATE_WITHDRAWAL_CRYPTO: string = 'client/withdrawal/crypto';
  private readonly URL_CREATE_WITHDRAWAL_DAI: string = 'client/withdrawal/dai';
  private readonly URL_WITHDRAWALS: string = 'client/withdrawals';
  private readonly URL_WITHDRAWAL_PROCESS: string = 'client/withdrawal/process';
  private readonly URL_FETCH_WITHDRAWAL_REQUESTS: string = 'withdrawal_requests';
  private readonly URL_REINVEST_FROM_WALLET: string = 'client/reinvest';

  constructor(
    protected httpClient: HttpClient,
    protected authenticationService: AuthenticationService) {
    super(httpClient, authenticationService);
  }

  public async createWithdrawalAsync(withdrawalRequest: Withdrawal) {
    return await this.postAsync<Withdrawal>(this.URL_CREATE_WITHDRAWAL, withdrawalRequest).toPromise();
  }

  public async createWitdrawalWithWireTransferAsync(withdrawalRequest: WithdrawalWireTransfer) {
    return await this.postAsync<WithdrawalWireTransfer>(this.URL_CREATE_WITHDRAWAL_WIRETRANSFER, withdrawalRequest).toPromise();
  }

  public async createWitdrawalWithPerfectMoneyAsync(withdrawalRequest: WithdrawalPerfectMoney) {
    return await this.postAsync<WithdrawalPerfectMoney>(this.URL_CREATE_WITHDRAWAL_PERFECTMONEY, withdrawalRequest).toPromise();
  }

  public async createWitdrawalWithCryptoAsync(withdrawalRequest: WithdrawalCrypto) {
    return await this.postAsync<WithdrawalCrypto>(this.URL_CREATE_WITHDRAWAL_CRYPTO, withdrawalRequest).toPromise();
  }

  public async createWitdrawalWithDAIAsync(withdrawalRequest: WithdrawalDAI) {
    return await this.postAsync<WithdrawalDAI>(this.URL_CREATE_WITHDRAWAL_DAI, withdrawalRequest).toPromise();
  }

  public async fetchWithdrawals(filters: WithdrawalsFilters) {
    return await this.postAsync<WithdrawalResponse>(this.URL_WITHDRAWALS,filters,{}).toPromise();
  }

  public async processWithdrawal(id: number, status: string) {
    return await this.postAsync<Withdrawal[]>(this.URL_WITHDRAWAL_PROCESS, {id, status}).toPromise();
  }

  public async fetchWithdrawalRequestsAsync(filters: WithdrawalRequestsFilter) {
    return await this.getAsync<WithdrawalRequestsResponse>(this.URL_FETCH_WITHDRAWAL_REQUESTS, filters, {}).toPromise();
  }

  public async reinvestFromWalletAsync(payload: any) {
    return await this.postAsync(this.URL_REINVEST_FROM_WALLET, payload, {}).toPromise();
  }

}
