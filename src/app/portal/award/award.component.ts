import { AfterViewInit, Component, OnInit } from '@angular/core';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { DynamicScriptLoaderService } from '@app/shared/services/dynamic-script-loader.service';
import { ProfileService } from '../profile/profile.service';

// import * as $ from 'jquery';
import 'jquery-knob';
declare var $: any;

@Component({
  selector: 'app-award',
  templateUrl: './award.component.html',
  styleUrls: ['./award.component.css']
})
export class AwardComponent extends AbstractBaseComponent implements OnInit {
    public counterDaysLeft = 0;
    public daysToAdd = 365;
    public subscriptionDate: Date = null;
    public withdrawDate: Date;
   
    

    public imagePath = [
        {
            path: '/assets/images/200.jpg'
        },
        {
            path: '/assets/images/500.jpg'
        },
        {
            path: '/assets/images/1000.jpg'
        },
    ];

    constructor(protected profileService: ProfileService) {
        super(profileService);
    }

    ngAfterViewInit() {
        $('#modal').modal('show');
    }

    hideModal() {
        $('#modal').modal('hide');
    }

    ngOnInit() {
        
        this.pageTitle = 'Awards';
        this.pageIcon = 'fal fa-trophy-alt';
        $('.mobile-nav-on').removeClass();
    }

    private getTimeLeft() {
        return this.withdrawDate.getTime() - new Date().getTime();
    }
    

    private setCounterData(milliseconds: number) {
        var days: any = Math.floor(milliseconds / (3600 * 24));
        var hours: any = Math.floor((milliseconds % (60 * 60 * 24)) / 3600);
        var minutes: any = Math.floor((milliseconds % (60 * 60)) / 60);
        var seconds: any = Math.floor(milliseconds % 60);
        $('.award_daysDial').val(days).trigger('change');
        $('.award_hoursDial').val(hours).trigger('change');
        $('.award_minutesDial').val(minutes).trigger('change');
        $('.award_secondsDial').val(seconds).trigger('change');
    }

    private addDays(oldDate: string, days: number) {
        const startDate = new Date(oldDate);
        return new Date(startDate.setDate(startDate.getDate() + days));
    }


}
