import { Routes } from '@angular/router';

import { FullComponent } from '@app/shared';
import { PortalModule } from './portal/portal.module';
import { AuthenticationGuard } from './core';
import { BinaryModule } from './binary/binary.module';
import { IntroComponent } from './frontend/intro/intro.component';
import { AboutComponent } from './frontend/about/about.component';
import { WebinarsComponent } from './frontend/webinars/webinars.component';
import { NewsComponent } from './frontend/news/news.component';
import { FoundationComponent } from './frontend/foundation/foundation.component';
import { ContactComponent } from './frontend/contact/contact.component';
import { FrontEndComponent } from './frontend/frontend.component';
import { NotFoundComponent } from './authentication/404';
import { LoginComponent } from './authentication/login';
import { SignupComponent } from './authentication/signup';
import { SubsriptionPaymentComponent } from './frontend/subsription-payment/subsription-payment.component';
import { BinaryLoginComponent } from './binary/login/login.component';
import { ForgotPasswordComponent } from './authentication/forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './authentication/reset-password/reset-password.component';
import { PakcagesComponent } from './frontend/pakcages/packages.component';
import { TwoFAComponent } from './frontend/two-fa/two-fa.component';
import { FrontendFaqsComponent } from './frontend/faqs/frontend-faqs.component';

export const AppRoutes: Routes = [
  {
    path: '',
    component: FrontEndComponent,
    children: [
      {
        path: '404',
        component: NotFoundComponent
      },
      {
        path: 'home',
        component: IntroComponent
      },
      {
        path: 'about',
        component: AboutComponent
      },
      {
        path: 'news',
        component: NewsComponent
      },
      {
        path: 'faq',
        component: FrontendFaqsComponent
      },
      {
        path: 'foundation',
        component: FoundationComponent
      },
      {
        path: 'webinars',
        component: WebinarsComponent
      },
      {
        path: 'contact',
        component: ContactComponent
      },
      {
        path: 'forgot-password',
        component: ForgotPasswordComponent
      },
      {
        path: 'reset-password/:token',
        component: ResetPasswordComponent
      },
      {
        path: 'packages',
        component: PakcagesComponent
      },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'register',
        component: SignupComponent
      },
      {
        path: 'validate/2fa',
        component: TwoFAComponent
      }
    ]
  },
  {
    path: 'client/packages',
    component: PakcagesComponent
  },

  {
    path: 'client/binary/login',
    component: BinaryLoginComponent
  },
  {
    path: 'client/logout',
    component: LoginComponent
  },
  {
    path: 'client/register',
    component: SignupComponent
  },
  {
    path: 'client/payment',
    component: SubsriptionPaymentComponent
  },
  {
    path: 'portal',
    loadChildren: () => PortalModule,
    component: FullComponent,
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'binary',
    loadChildren: () => BinaryModule,
    component: FullComponent,
    canActivate: [AuthenticationGuard]
  }
];
