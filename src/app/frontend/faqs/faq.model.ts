export interface FAQ {
    question: string;
    answer: string;
}


export const faqs: FAQ[] = [
  {
      question: 'How much is the minimum amount for investment in Options Legion?',
      answer: 'Our investment platform starts from 50$ up to any higher amount withoutlimitation.'
  },
  {
      question: 'How much is the registration fee?',
      answer: 'The registration fee is 10$ for annual membership.'
  },
  {
      question: 'When will my profits be released?',
      answer: 'All profits will be calculated and released at 11:59PM (GMT+2) per working day.'
  },
  {
      question: 'How long is my investment contract time duration?',
      answer: 'Investment contract time is 24 months which can be extended with same conditions'
  },
  {
      question: 'What is the interest rate of the investment?',
      answer: 'Interest rate of the investment is 4-8% per month based on the different packages, which is calculated daily during 22 working days.'
  },
  {
      question: 'What is the direct referral commission?',
      answer: '4-7% of each investment via direct referral goes to referent. <br>Please note that it depends on the package you choose.<br>(All members can have unlimited direct referrals)'
  },
  {
      question: 'How is binary plan profit calculation?',
      answer: 'When the average balance of right and left legs reaches up to 1000$ for each leg, referent will receive 10%.'
  },
    {
        question: 'How can I deposit money into my investments account?',
        answer: 'You can charge your account through Perfect Money, Tether USDT and Credit Card Payment.'
    },
    {
        question: 'How can I withdraw my available profit?',
        answer: 'Withdrawal is available only on Monday.<br> Withdrawal cap depend on your package.'
    },
    {
      question: 'How much is the minimum amount for withdrawal?',
      answer: 'You can withdraw any amount up to 10$ which is available in your wallet. After deducting 4% commission.'
  },
    {
      question: 'When can I cancel my contract?',
      answer: 'Cancelling your contract within a certain period is dependant on the type of package you possess. “Bronze”, “Silver” and “Gold” packages are cancellable 12 months after initiation whereas “Diamond” packages cannot be cancelled throughout the two-year duration of the contract. Please note that 10% of your capital will be deducted from your account upon cancellation. <br> <br>Notice: The contracts commences as soon as you deposit capital into your account. As soon as you plan on cancelling your contract (during the eligibility period), you must send three documents to prove your identity. These documents are: your passport, electricity bill, and ID.'
    }
];
