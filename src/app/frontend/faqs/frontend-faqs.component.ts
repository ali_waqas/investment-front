import { Component, OnInit } from '@angular/core';
import { FAQ } from '@app/portal/faqs/faq.model';
import { faqs } from './faq.model';

@Component({
  selector: 'app-frontend-faqs',
  templateUrl: './frontend-faqs.component.html',
  styleUrls: ['./frontend-faqs.component.css']
})
export class FrontendFaqsComponent implements OnInit {
  public pageTitle = 'FAQ';
  public pageIcon = 'ni ni-question';
  public faqs: FAQ[] = [];
  constructor() { }
  ngOnInit() {
    this.faqs = faqs;
  }
}
