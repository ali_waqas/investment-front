import { NumberUnitLength } from "luxon";

export interface Package {
    id: number;
    name: string;
    min_amount: number;
    max_amount: number;
    monthly_income: number;
    duration: number;
    referral_income: number;
    binary_income: Number;
    capping_limit_for_weekly_withdrawal: number;
    withdrawal_fee: number;
    extra_param_1: string;
    extra_param_2: string;
}