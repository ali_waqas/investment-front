import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from '@app/core';

@Component({
  selector: 'app-packages',
  templateUrl: './packages.component.html',
  styleUrls: ['./packages.component.css']
})
export class PakcagesComponent implements OnInit {

    constructor(private router: Router, private localStorage: LocalStorageService) { }

    ngOnInit() {
    }

    public selectPackage(packageId: number) {
        this.localStorage.setItem('package-id', packageId);
        this.router.navigateByUrl('client/register');
    }
}
