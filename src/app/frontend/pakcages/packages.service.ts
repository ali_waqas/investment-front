import { Injectable } from '@angular/core';
import { BaseAPIClass } from '@app/core';
import { Package } from './package.model';

@Injectable({providedIn: 'root'})
export class PackagesService extends BaseAPIClass {
    private packages: Package[] = [
        {
            id: 1,
            name: 'Bronze',
            min_amount: 50,
            max_amount: 199,
            monthly_income: 4,
            duration: 24,
            referral_income: 4,
            binary_income: 10,
            capping_limit_for_weekly_withdrawal: 1000,
            withdrawal_fee: 4,
            extra_param_1: '0',
            extra_param_2: '0'
        },
        {
            id: 2,
            name: 'Silver',
            min_amount: 200,
            max_amount: 499,
            monthly_income: 5,
            duration: 24,
            referral_income: 5,
            binary_income: 10,
            capping_limit_for_weekly_withdrawal: 3000,
            withdrawal_fee: 4,
            extra_param_1: '0',
            extra_param_2: '0'
        },
        {
            id: 3,
            name: 'Gold',
            min_amount: 500,
            max_amount: 999,
            monthly_income: 6,
            duration: 24,
            referral_income: 6,
            binary_income: 10,
            capping_limit_for_weekly_withdrawal: 5000,
            withdrawal_fee: 4,
            extra_param_1: '0',
            extra_param_2: '0'
        },
        {
            id: 4,
            name: 'Diamond',
            min_amount: 1000,
            max_amount: 99999,
            monthly_income: 7,
            duration: 24,
            referral_income: 7,
            binary_income: 10,
            capping_limit_for_weekly_withdrawal: 7000,
            withdrawal_fee: 4,
            extra_param_1: '0',
            extra_param_2: '0'
        }
    ];

    public getPackages() {
        return this.packages;
    }

    public getPackageById(packageId: number) {
        console.log(packageId);
        return this.packages.find((package_: Package) => package_.id === packageId);
    }
}