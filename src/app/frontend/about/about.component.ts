import { Component, OnInit } from '@angular/core';
import { NavbarItem } from '../../portal/sidebar/sidebar.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  constructor(private router: Router) {}
  public NavbarItems: NavbarItem[] = [
    {
        id: 0,
        title: 'About',
        link: '/portal/dashboard',
        icon: 'fal fa-warehouse'
      },
      {
        id: 2,
        title: 'News',
        link: '/news',
        icon: 'fal fa-newspaper'
      },
      {
        id: 3,
        title: 'Foundation',
        link: '/foundation',
        icon: 'fal fa-bullseye'
      },
      {
        id: 3,
        title: 'Webinars',
        link: '/webinars',
        icon: 'ni ni-camcorder'
      },
      {
        id: 3,
        title: 'Contact',
        link: '/contact',
        icon: 'nav-link-text'
      },
  ];

  ngOnInit() {
      (<any>$)('body').addClass('desktop chrome webkit pace-done nav-mobile-push nav-function-top blur');
  }

  public gotoPage(navbarItem: NavbarItem) {
    this.router.navigateByUrl(navbarItem.link);
  }
}
