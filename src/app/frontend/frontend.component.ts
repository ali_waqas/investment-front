import { Component, OnInit } from '@angular/core';
import { NavbarItem } from '@app/portal/sidebar/sidebar.component';
import { Router } from '@angular/router';
import { ApplicationEvent } from '@app/shared/services/alert-modal.service';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { ProfileService } from '@app/portal/profile/profile.service';
import { Alert } from '@app/shared/models/alert.model';

@Component({
  selector: 'app-frontend',
  templateUrl: './frontend.component.html',
  styleUrls: ['./frontend.component.scss']
})
export class FrontEndComponent extends AbstractBaseComponent implements OnInit {
    constructor(
        private router: Router,
        protected profileService: ProfileService,
        private applicationEvent: ApplicationEvent) {
        super(profileService);
        this.subscribe(this.applicationEvent.onAlert.subscribe(this.handleModalAlert.bind(this)));
    }

    public NavbarItems: NavbarItem[] = [
        {
          id: 0,
          title: 'Home',
          link: '/home',
          icon: 'fal fa-warehouse'
        },
        {
            id: 0,
            title: 'About',
            link: '/about',
            icon: 'ni ni-graduation'
        },
        {
          id: 2,
          title: 'News',
          link: '/news',
          icon: 'fal fa-newspaper'
        },
        {
          id: 3,
          title: 'Foundation',
          link: '/foundation',
          icon: 'fal fa-bullseye'
        },
        {
          id: 3,
          title: 'Webinars',
          link: '/webinars',
          icon: 'ni ni-camcorder'
        },
        {
          id: 3,
          title: 'Contact',
          link: '/contact',
          icon: 'fal fa-mobile'
        },
    ];
    ngOnInit() {

        if (this.router.url === '/') {
            this.router.navigateByUrl('/home');
        }

        (<any>$)('body').addClass('desktop chrome webkit pace-done nav-mobile-push nav-function-top blur');
    }
    public gotoPage(navbarItem: NavbarItem) {
      this.router.navigateByUrl(navbarItem.link);
    }

    public handleModalAlert(alert: Alert) {
        this.alert = alert;
        (<any>$('#alert-modal-md')).modal();
    }

}
