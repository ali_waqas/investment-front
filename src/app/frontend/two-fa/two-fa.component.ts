import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '@app/core';
import { AbstractBaseComponent } from '@app/core/class/abstract.base.omponent';
import { Profile } from '@app/portal/profile/profile.model';
import { ProfileService } from '@app/portal/profile/profile.service';

declare const $: any;

@Component({
  selector: 'app-two-fa',
  templateUrl: './two-fa.component.html',
  styleUrls: ['./two-fa.component.css']
})
export class TwoFAComponent extends AbstractBaseComponent implements OnInit {
  validateForm: FormGroup;
  isLoading = false;
    @Input() email: string;
    @Input() password: string;

  constructor(
    public router: Router,
    private formBuilder: FormBuilder,
    profileService: ProfileService,
    private authenticationService: AuthenticationService) {
    super(profileService);
  }

  ngOnInit() {
    this.createForm();
  }

  async validate() {
    this.hasErrors = false;
    this.isLoading = true;
    try {
      this.validateForm.controls.email.setValue(this.email);
      this.validateForm.controls.password.setValue(this.password);
      const validatedForm: Profile = this.validateForm.value;
      const validate2FA: any = await this.profileService.validate2FAAsync(validatedForm);
      this.authenticationService.setCredentials({user: null , token: validate2FA.token, session: null });
      const client = await this.profileService.getProfileAsync(true);
      $("#2fa-modal-md").modal('hide');
      this.router.navigate(['/portal/dashboard'], { replaceUrl: true});
    } catch (error) {
      this.hasErrors = true;
      this.httpError = error.error.error;
      this.isLoading = false;
    }
  }

  private createForm() {
    this.validateForm = this.formBuilder.group({
      email: ['', []],
      password: ['', []],
      token: ['', Validators.required]
    });
  }
}
