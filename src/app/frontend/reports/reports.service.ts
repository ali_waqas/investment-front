import { Injectable } from '@angular/core';
import { BaseAPIClass } from '@app/core/class/baseAPI.class';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { AnalysisReport } from '@app/shared/models/report.model';

@Injectable({ providedIn: 'root' })
export class ReportsService extends BaseAPIClass {

    private readonly URL_FETCH_DEPOSITS_BY_DAYS: string = 'dashboard/deposits_by_days';

    constructor(
        protected httpClient: HttpClient,
        protected authenticationService: AuthenticationService) {
        super(httpClient, authenticationService);
    }

    public async fetchDepositsByDays() {
        return await this.getAsync<AnalysisReport>(this.URL_FETCH_DEPOSITS_BY_DAYS, {}, {}).toPromise();
    }


    private getToken() {
        return this.authenticationService.getAccessToken();
    }

    private serialize(obj: any) {
        var str = [];
        for (var p in obj)
          if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
          }
        return str.join("&");
    }
}
