import { HttpClient } from '@angular/common/http';
import { Injectable, EventEmitter } from '@angular/core';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { LocalStorageService } from '@app/core/local-storage.service';
import { AuthenticationEvent } from './authentication.model';
import { SettingsService } from '@app/shared/services/settings-service';

const credentialsKey = 'credentials';

@Injectable({providedIn: 'root'})
export class AuthenticationService {
  private _credentials: Authentication.Credentials | null;
  public onAuthenticationChange = new EventEmitter<AuthenticationEvent>();
  public onBinaryPlatformAuthenticationChange = new EventEmitter<AuthenticationEvent>();
  public readonly URL_LOGIN = 'client/authenticate';
  public readonly URL_LOGOUT = 'client/logout';

  constructor(
    private httpClient: HttpClient,
    private localStorageService: LocalStorageService) {
    const savedCredentials = this.localStorageService.getItem(credentialsKey);
    if (savedCredentials) {
      this._credentials = JSON.parse(savedCredentials);
    }
  }

  login( payload: Authentication.LoginPayload ): Observable<Authentication.Credentials> {
    return this.httpClient.post(this.URL_LOGIN, payload).pipe(
      map((body: Authentication.Credentials) => {
        this.setCredentials(body);
        return body;
      })
    );
  }

  signup(
    payload: Authentication.SignupPayload
  ): Observable<Authentication.User> {
    return this.httpClient.post('/signup', payload).pipe(
      map((body: Authentication.User) => {
        return body;
      })
    );
  }

  /**
   * Logs out the user and clear credentials.
   * @return {Observable<boolean>} True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    return this.httpClient
      .post(this.URL_LOGOUT, { sessionId: this.credentials.session })
      .pipe(
        map(() => {
          const authenticationEvent: AuthenticationEvent = { status: 'logged-out', payload: {} };
          this.onAuthenticationChange.emit(authenticationEvent);
          this.onBinaryPlatformAuthenticationChange.emit(authenticationEvent);
          this.setCredentials();
          return true;
        })
      );
  }

  /**
   * Checks is the user is authenticated.
   * @return {boolean} True if the user is authenticated.
   */
  isAuthenticated(): boolean {
    return !!this.credentials;
  }

  /**
   * Gets the user credentials.
   * @return {Credentials} The user credentials or null if the user is not authenticated.
   */
  get credentials(): Authentication.Credentials | null {
    return this._credentials;
  }

  /**
   * Get the auth token.
   * @return {string} The auth token is null if user is not authenticated.
   */
  public getAccessToken(): string | null {
    return this.credentials ? this.credentials.token : null;
  }
  /**
   * Sets the user credentials.
   * @param {Credentials=} Authentication.Credentials The user credentials.
   */
  public setCredentials(credentials?: Authentication.Credentials) {
    this._credentials = credentials || null;
    if (credentials) {
      const authenticationEvent: AuthenticationEvent = { status: 'logged-in', payload: this._credentials};
      this.onAuthenticationChange.emit(authenticationEvent);
      this.localStorageService.setItem(credentialsKey, JSON.stringify(credentials));
    } else {
      this.localStorageService.clearItem(credentialsKey);
    }
  }
}
