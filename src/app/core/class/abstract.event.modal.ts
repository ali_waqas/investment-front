export interface AbstractComponentEvent {
  type: string;
  payload: string;
}
