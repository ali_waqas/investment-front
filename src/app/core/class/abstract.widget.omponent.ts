declare var $: any;
import { Subscription } from 'rxjs';

export abstract class AbstractWidgetComponent {
    public pageTitle = 'Abstract';
    public pageIcon = 'fal fa-chart-area';
    public hasErrors = false;
    public ready = false;
    public loading = false;
    protected subscriptions: Subscription[] = [];
    protected initializedDataTables: string[] = [];
    private tableOptions: StringMap[] = [];
    public dataTableName = 'datatable';
    protected dataTableInitialized = false;
    protected massIds: number[] = [];
    protected pages: number[] = [];
    protected entityName = '';
    constructor() {

    }


    ngOnDestroy(): void {
      this.subscriptions.forEach(subscription => {
          console.log('Destroying subscription');
      });
  }

    protected subscribe(subscription: Subscription) {
      if (!this.subscriptions.includes(subscription)) {
          this.subscriptions.push(subscription);
      }
  }

  protected unsubscribe() {
    this.subscriptions.forEach(subscription => {
        subscription.unsubscribe();
    });
  }


    protected initDataTable(tableName: string = '') {
        const tableToBeInit = (tableName === '') ? this.dataTableName : tableName;
        this.addTableOption('bFilter', false); // Hides the search
        this.addTableOption('bInfo', false); // Hides the Footer  (Showing 1 to 5 of 5 entries)
        this.addTableOption('responsive', true);
        this.addTableOption('bPaginate', false); // Hides the pager
        const tableOptions = this.getTableOptions();
        const this_ = this;
        setTimeout(function() {
            const table: any = $('#' + tableToBeInit);
            if (this_.initializedDataTables.indexOf(tableToBeInit) >= 0) {
                table.DataTable();
            } else {
                this_.initializedDataTables.push(tableToBeInit);
                table.DataTable(tableOptions);
            }
        }, 500);
    }

    protected addTableOption(name: string, value: any) {
        this.tableOptions.push({name, value});
    }

    protected clearTableOptions() {
        this.tableOptions = [];
    }

    protected getTableOptions() {
        if (this.tableOptions.length === 0 ) {
            return {
                    scrollY: '500px',
                    scrollCollapse: true
                    };
        } else {
            const tableOptions: StringMap = {};
            this.tableOptions.forEach(option => {
                tableOptions[option.name] = option.value;
            });
            return tableOptions;
        }
    }

    protected setPages(pages: number) {
        this.pages = [];
        for (let i = 1; i <= pages; i++) {
            this.pages.push(i);
        }
    }


    protected updatePagerLinks(page: number) {
      $('.page-item').removeClass('active');
      $('.page-item:eq('+(page-1)+')').addClass('active');
  }

    protected addDataTableButtons() {
        this.addTableOption('dom', '<\'row mb-3\'<\'col-sm-12 col-md-6 d-flex align-items-center justify-content-start\'f>' +
        '<\'col-sm-12 col-md-6 d-flex align-items-center justify-content-end\'B>>' +
        '<\'row\'<\'col-sm-12\'tr>>' +
        '<\'row\'<\'col-sm-12 col-md-5\'i><\'col-sm-12 col-md-7\'p>>');
        this.addTableOption('buttons', [
            {
                extend: 'colvis',
                text: 'Column Visibility',
                titleAttr: 'Col visibility',
                className: 'btn-outline-default'
            },
            {
                extend: 'csvHtml5',
                text: 'CSV',
                titleAttr: 'Generate CSV',
                className: 'btn-outline-default'
            },
            {
                extend: 'copyHtml5',
                text: 'Copy',
                titleAttr: 'Copy to clipboard',
                className: 'btn-outline-default'
            },
            {
                extend: 'print',
                text: '<i class="fal fa-print"></i>',
                titleAttr: 'Print Table',
                className: 'btn-outline-default'
            }

        ]);
    }

    protected loadingStart() {
        this.loading = true;
    }

    protected loadingFinished() {
        this.loading = false;
    }

    protected removeRow(rowId: string, dataTableName: string) {
        const table: any = $('#' + dataTableName);
        $('#' + rowId).remove();
        table.draw();
    }

    protected check(obj: any, idKey: string = 'id') {
        const id = obj[idKey];
        const idIndex = this.massIds.indexOf(id);
        if (idIndex > -1) {
            this.massIds.splice(idIndex, 1);
        } else {
            this.massIds.push(id);
        }
    }

    public getRowId(row: any) {
        return this.entityName + '-' + row.id;
      }
}
