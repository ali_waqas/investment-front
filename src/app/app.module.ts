import * as $ from 'jquery';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutes } from '@app/app.routing';
import { AppComponent } from '@app/app.component';
import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { SidebarComponent } from './portal/sidebar/sidebar.component';
import { ProfileService } from './portal/profile/profile.service';
import { BinaryModule } from './binary/binary.module';
import { PakcagesComponent } from './frontend/pakcages/packages.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [AppComponent, PakcagesComponent],
  exports: [SidebarComponent],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule,
    RouterModule.forRoot(AppRoutes),
    CoreModule,
    BinaryModule,
    SharedModule
  ],
  bootstrap: [AppComponent],
  providers: [ProfileService]
})
export class AppModule {}
