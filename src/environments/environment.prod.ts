export const environment = {
  env: 'PROD',
  production: true,
  serverBaseUrl: 'https://optionslegion.com',
  serverUrl: 'https://server.optionslegion.com/api/'
};
