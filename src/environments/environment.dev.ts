export const environment = {
  env: 'DEV',
  production: true,
  serverBaseUrl: 'https://optionslegion.com',
  serverUrl: 'https://server.optionslegion.com/api/'
};
