export const environment = {
  env: 'LOCAL',
  production: false,
  serverBaseUrl: 'http://localhost:4200/',
  serverUrl: 'http://localhost:4200/api/'
};
